package com.vyooha.ridelogic.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.View;

public class CompassNeedle extends View {
	  
	 private float direction;
	 private Direction[] dirclass;
	 
	   
	 public CompassNeedle(Context context) {
	  super(context);
	  setDirection();
	  // TODO Auto-generated constructor stub
	 }
	  
	 public CompassNeedle(Context context, AttributeSet attrs) {
	  super(context, attrs);
	  
	  
	  
	  // TODO Auto-generated constructor stub
	 }
	  
	 public CompassNeedle(Context context, AttributeSet attrs, int defStyle) {
	  super(context, attrs, defStyle);
	  setDirection();
	  // TODO Auto-generated constructor stub
	 }
	  
	 @Override
	 protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	  setMeasuredDimension(
	    MeasureSpec.getSize(widthMeasureSpec),
	    MeasureSpec.getSize(heightMeasureSpec));
	 }
	  
	 @Override
	 protected void onDraw(Canvas canvas) {
	    
	  int w = getMeasuredWidth() *3;
	  int h = getMeasuredHeight();
	  int r;
	  if(w > h){
	   r = h/2;
	  }else{
	   r = w/2;
	  }
	    
	  Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	  paint.setStyle(Paint.Style.STROKE);
	  paint.setStrokeWidth(5);
	  paint.setColor(Color.BLUE);
	  Paint paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
	  paint1.setStyle(Paint.Style.STROKE);
	  paint1.setStrokeWidth(5);
	  paint1.setTextAlign(Align.CENTER);
	  paint1.setColor(Color.BLUE);
	  
	  Paint paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
	  paint2.setStyle(Paint.Style.STROKE);
	  paint2.setStrokeWidth(5);
	  paint2.setTextAlign(Align.CENTER);
	  paint2.setColor(Color.RED);
	    
//	  canvas.drawCircle(w/2, h/2, r, paint);
//	    
	  int xdivfactor=w/16;
	  int ydivfactor=h/8;
	  canvas.drawLine(0, h/2, w, h/2, paint);
	  int f=1,alter=h/2+ydivfactor;
	  for(int i=1;i<16;i++)
	  {if(f==0)
	  	{
		  alter=h/2+ydivfactor;
		  f=1;
	  	  	}
	  else
	  {
		  alter=h/2+2*ydivfactor;
		  f=0;
	  }
		  canvas.drawLine(i*xdivfactor, h/2, i*xdivfactor,alter , paint);
		  if((i-1)%4==0)
		  canvas.drawText("North", i*xdivfactor, (h/2)-ydivfactor, paint1);
		  
	  }
	
	//  paint.setColor(Color.RED);
//	  canvas.drawLine(
//	    w/2,
//	    h/2,
//	    (float)(w/2 + r * Math.sin(-direction)),
//	    (float)(h/2 - r * Math.cos(-direction)),
//	    paint);
	  canvas.drawLine(w/2, h/2, w/2, h, paint2);
	  
	 }
	   
	 public void update(float dir){
	  direction = dir;
	  invalidate();
	 }
	  public void setDirection()
	  {
		  String[] arrnames={"North","NE","East","SE","South","SW","West","NW"};
		  for(int i=0;i<8;i++){
			  dirclass[i].dir_name=arrnames[i];
			  if(i==0){
				 dirclass[i].prev=7;
			  }
			  else
			  {
					 dirclass[i].prev=i-1;
			  }
			  if(i==7){
					 dirclass[i].next=0;
			  }
			  else
			  {
				  dirclass[i].next=i+1;  
			  }
			  
		  }
	  }
	  public int radianToDiv(float rad)
	  {
		  int result=(int)((rad*32)/3.14);
		  return result;
	  }
	}