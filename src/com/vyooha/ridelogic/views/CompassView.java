package com.vyooha.ridelogic.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.provider.Telephony.Sms.Conversations;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;


import com.vyooha.ridelogic.activities.CompassActivity;
import com.vyooha.ridelogic.activities.R;

 public class CompassView
  extends Button
{
  private Bitmap mBackground = ((BitmapDrawable)getResources().getDrawable(R.drawable.compass_view)).getBitmap();
  private  Bitmap mBackground1 = ((BitmapDrawable)getResources().getDrawable(R.drawable.speedohalf)).getBitmap();
  
  private float mBearing = 0.0F;
  private Paint mPaint;
  private Paint mPaintPointer;
  private float mScreenRatio = CompassActivity.mScreenRatio;
  
  public CompassView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CompassView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  
  
  public CompassView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setHeight(this.mBackground.getHeight());
    this.mPaint = new Paint();
    this.mPaint.setARGB(255, 50, 50, 50);
    

    this.mPaintPointer = new Paint();
    this.mPaintPointer.setColor(Color.RED);
    this.mPaintPointer.setStrokeWidth(2.0F * this.mScreenRatio);
  }
  
  float normBearing(float paramFloat)
  {
	  return paramFloat;
//	  if (paramFloat >= 0.0F) {}
//	    for (;;)
//	    {
//	      if (paramFloat <= 360.0F)
//	      {
//	    	  Log.e("=============angle retrieved ==========",""+paramFloat);
//	        return paramFloat;
//	      }
//	      
////	        paramFloat += 360.0F;
////	        break;
//	      
//	      paramFloat -= 360.0F;
//	    }
//    
 
  }
  
public  void onBearingChanged(float paramFloat)
  {
    this.mBearing = normBearing(paramFloat);
    invalidate();
  }
  
@Override
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    float center=getWidth()/2f;
    
    float dif=center-(148.0f*mScreenRatio);
    paramCanvas.drawBitmap(this.mBackground, (float)-(1.78D * this.mBearing * this.mScreenRatio)+dif, 0.0F, this.mPaint);
    paramCanvas.drawBitmap(this.mBackground1,-(40.0F*this.mScreenRatio),0.0F, mPaint);
    paramCanvas.drawLine((148.0F * this.mScreenRatio)+dif,  42.0F * this.mScreenRatio, (148.0F * this.mScreenRatio)+dif, 80.0F * this.mScreenRatio, this.mPaintPointer);
  }
}


/* Location:           C:\Users\Nikheesh\Desktop\speedexample\New folder\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.codesector.speedview.free.CompassView
 * JD-Core Version:    0.7.0.1
 */