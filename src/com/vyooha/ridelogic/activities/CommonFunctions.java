package com.vyooha.ridelogic.activities;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

public class CommonFunctions {
	
	
	  public static String getAddress(double latitude, double longitude,Context context) {
	        StringBuilder result = new StringBuilder();
	        try {
	            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
	            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
	            if (addresses.size() > 0) {
	                Address address = addresses.get(0);
	                result.append(address.getLocality()).append(",");
	                result.append(address.getCountryName());
	            }
	        } catch (IOException e) {
	            Log.e("tag", e.getMessage());
	        }

	        return result.toString();
	    }
	    
	
	
	
	public static String getTimeStamp(Date d){
		
		String timestampStr="";
	
		
		
		Calendar thatDay = Calendar.getInstance();
		thatDay.setTime(d);
		 

		  Calendar today = Calendar.getInstance();

		  long diff = today.getTimeInMillis() - thatDay.getTimeInMillis();
		  long days = diff / (24 * 60 * 60 * 1000);
		  int nodays=(int) days;
			String stranpm;
		  if(thatDay.AM_PM==Calendar.PM){
			  Log.e("",""+thatDay.AM_PM);
			  stranpm="PM";
		  }
		  else
			  stranpm="AM";
		  
		  switch (nodays) {
		case 0:
			
			     timestampStr=""+thatDay.HOUR+" : "+thatDay.MINUTE+" "+stranpm;
			break;
		case 1:
			timestampStr="Yesterday";
			break;
		default:
			timestampStr=days+" days ago";
			
			break;
		}
		  
		  return timestampStr;
		  
	}
	
	

}
