package com.vyooha.ridelogic.activities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DBName";

    private static final int DATABASE_VERSION = 2;

    // Database creation sql statement
    private static final String DATABASE_CREATE_TRIP = "create table UserTrips(user text,vehicle text,obid text,tripname text,tripdist integer,triponoff text,isActive text,flag text);";
    private static final String DATABASE_CREATE_FUEL_FILLINGS="create table FuelFillings(user text,vehicle text,litre real,cost real,amount real,flag text);";
    private static final String DATABASE_CREATE_USERDISTBYDAY="create table UserDistByDay(user text,vehicle text,date_id text,dist_covered real,flag text);";


    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
    	
    //	Log.d("database", "db created");
    	database.execSQL(DATABASE_CREATE_TRIP);
    	database.execSQL(DATABASE_CREATE_FUEL_FILLINGS);
    	database.execSQL(DATABASE_CREATE_USERDISTBYDAY);
    	
    	
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVersion){
        Log.d(MyDatabaseHelper.class.getName(),"Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS UserTrips");
        database.execSQL("DROP TABLE IF EXISTS FuelFillings");
        database.execSQL("DROP TABLE IF EXISTS UserDistByDay");
        
        onCreate(database);
    }
}