package com.vyooha.ridelogic.activities;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.vyooha.ridelogic.activities.PlanATrip1.SelectDateFragment;

public class PlanATrip4  extends Fragment {
		Button starttrip,finish;
	String Name;
	
	 @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	       View contacts=inflater.inflate(R.layout.plan_a_trip, container, false); 
	       return contacts;
	    }  
	       
	       
	    @Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
	        
	        this.getView().setFocusableInTouchMode(true);

	        this.getView().setOnKeyListener( new OnKeyListener()
	        {
	            @Override
	            public boolean onKey( View v, int keyCode, KeyEvent event )
	            {
	                if( keyCode == KeyEvent.KEYCODE_BACK )
	                {
	                    return true;
	                }
	                return false;
	            }
	        } );
	        
	        
	        starttrip=(Button)getActivity().findViewById(R.id.plantrip_starttrip_btn); 
	        finish=(Button)getActivity().findViewById(R.id.plantrip_finish_btn); 
		       
	        //showDate(year, month+1, day);
	        
	        starttrip.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					String tripid=getArguments().getString("plantripid");
//			
//					HashMap<String,Object> input_param=new HashMap<String, Object>();
//					input_param.put("pTrip_objid",tripid);
//					ParseCloud.callFunctionInBackground("startPTrip", input_param, new FunctionCallback<Map<String,Object>>() {
//						
//
//						@Override
//						public void done(Map<String,Object> result, ParseException e) {
//							// TODO Auto-generated method stub
//							 if (e == null) {
//							      // result is "Hello world!"
//								 Toast.makeText(getActivity().getApplicationContext(), result.get("message").toString(), Toast.LENGTH_LONG).show();
//									
//							   
//							 }
//						}
//						});
//					
			
				}
			});
	        finish.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					 Intent i=new Intent(getActivity().getApplicationContext(), MainMenuActivity.class);
						startActivity(i);
				}
			});
	    
	    }
	    
	  

}
