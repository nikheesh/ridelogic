package com.vyooha.ridelogic.activities;

import java.util.ArrayList;
import java.util.HashMap;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.internal.ed;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.Parse;
import com.parse.ParseUser;

import android.app.ActionBar;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.IBinder;
import android.util.Log;
import io.fabric.sdk.android.Fabric;

public class ParseApplication extends Application {
	public static MediaPlayer mp;
	public SongsManager songManager;
	public ParseUser user;
	 public Utilities utils;
	 public int currentSongIndex;
	 int songindex;
	 public boolean musiconcreate;
	 
	    public boolean isShuffle = false;
	    public boolean isRepeat = false;	 
	 public ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	 
	public static boolean mMapIsTouched=true;
	
	public SharedPreferences pref ; 
	public   Editor editor;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		
		Fabric.with(this, new Crashlytics());
		pref= getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE); 
		editor=  pref.edit();
		
		mp=new MediaPlayer();
		songManager = new SongsManager();
		utils = new Utilities();
		musiconcreate=false;
		if(!pref.contains("index")){
		currentSongIndex=0;
		editor.putInt("index", currentSongIndex);
		editor.commit();
		}
		else
		{
			currentSongIndex=pref.getInt("index", 0);
		}
		
		
		songsList = songManager.getPlayList(this);

		// Add your initialization code here
Parse.enableLocalDatastore(this);
		Parse.initialize(this, "5R5JqlnvaDrfCr2Z8dPBkycwLdZpHVW4dmSZ1MwE", "iTplJvKEUguN9goDvF1GEHluGstY702EnDkqHZwi");
user=ParseUser.getCurrentUser();

DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
.cacheOnDisc(true).cacheInMemory(true)
.imageScaleType(ImageScaleType.EXACTLY)
.displayer(new FadeInBitmapDisplayer(300)).build();

ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
getApplicationContext())
.defaultDisplayImageOptions(defaultOptions)
.memoryCache(new WeakMemoryCache())
.discCacheSize(100 * 1024 * 1024).build();

ImageLoader.getInstance().init(config);

	Intent i=new Intent(getApplicationContext(),LocationService.class);
	bindService(i, mServerConn, Context.BIND_AUTO_CREATE);
	startService(i);
	}
	
	protected ServiceConnection mServerConn = new ServiceConnection() {
	    @Override
	    public void onServiceConnected(ComponentName name, IBinder binder) {
	        Log.d("", "onServiceConnected");
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName name) {
	        Log.d("", "onServiceDisconnected");
	    }
	
	};
	
}
