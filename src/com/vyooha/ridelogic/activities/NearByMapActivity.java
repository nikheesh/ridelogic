package com.vyooha.ridelogic.activities;

import java.util.ArrayList;
import java.util.List;  

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;



//import com.google.maps.android.clustering.ClusterManager;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class NearByMapActivity extends FragmentActivity implements OnCameraChangeListener,LocationListener, ClusterManager.OnClusterClickListener<Person>, ClusterManager.OnClusterInfoWindowClickListener<Person>, ClusterManager.OnClusterItemClickListener<Person>, ClusterManager.OnClusterItemInfoWindowClickListener<Person> {
	
	LocationManager locmgr;
	Criteria criteria;
	String provider;
	Location loc_current;
	//ClusterManager<MyItem> mClusterManager;
	public Intent widIntent ;
	private LocationListener myLocationListener;
	final int min=0,max=20;
	SupportMapFragment map;
	SeekBar radius;
	int radius_circle;
	LatLng latlngnow;
	 private ClusterManager<Person> mClusterManager;
	
	Button search_nearby;
	//ArrayList<ParseUser> users;
	

ImageView back;

private class PersonRenderer extends DefaultClusterRenderer<Person> {
    private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
    private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
    private final ImageView mImageView;
    private final ImageView mClusterImageView;
    private final int mDimension;

    public PersonRenderer() {
        super(getApplicationContext(), map.getMap(), mClusterManager);

        View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
        mClusterIconGenerator.setContentView(multiProfile);
        mClusterImageView = (ImageView) multiProfile.findViewById(R.id.imagecluster);

        mImageView = new ImageView(getApplicationContext());
        mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
        mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
        int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
        mImageView.setPadding(padding, padding, padding, padding);
        mIconGenerator.setContentView(mImageView);
    }

    @Override
    protected void onBeforeClusterItemRendered(Person person, MarkerOptions markerOptions) {
        // Draw a single person.
        // Set the info window to show their name.
        mImageView.setImageResource(person.profilePhoto);
        Bitmap icon = mIconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.name);
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
        // Draw multiple people.
        // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
        List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
        int width = mDimension;
        int height = mDimension;

        for (Person p : cluster.getItems()) {
            // Draw 4 at most.
            if (profilePhotos.size() == 4) break;
            Drawable drawable = getResources().getDrawable(p.profilePhoto);
            drawable.setBounds(0, 0, width, height);
            profilePhotos.add(drawable);
        }
        MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
        multiDrawable.setBounds(0, 0, width, height);

        mClusterImageView.setImageDrawable(multiDrawable);
        Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        // Always render clusters.
        return cluster.getSize() > 1;
    }
}

@Override
public boolean onClusterClick(Cluster<Person> cluster) {
    // Show a toast with some info when the cluster is clicked.
    String firstName = cluster.getItems().iterator().next().name;
    Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();
    return true;
}

@Override
public void onClusterInfoWindowClick(Cluster<Person> cluster) {
    // Does nothing, but you could go to a list of the users.
}

@Override
public boolean onClusterItemClick(Person item) {
    // Does nothing, but you could go into the user's profile page, for example.
    return false;
}

@Override
public void onClusterItemInfoWindowClick(Person item) {
    // Does nothing, but you could go into the user's profile page, for example.
}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_near_by_map);
		radius=(SeekBar)findViewById(R.id.seekBarCircleRadius);
		radius_circle=1000;
		
		//LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
				 
		
	//	 widIntent=new Intent(getApplicationContext(),SpeedWidgetService.class);
		//	startService(widIntent);
			
			back=(ImageView)findViewById(R.id.left_arrow);
			
			back.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(), CompassActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
					finish();
				}
			});
			
			search_nearby=(Button)findViewById(R.id.search_nearby_button);
			search_nearby.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(),NearByUsersListActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				}
			});
			
			
	//		retrieveUsers();
			
			
			
			
			
		locmgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.NO_REQUIREMENT);
		provider = locmgr.getBestProvider(criteria, false);
				
		map = (SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.llf_map);
		map.getMap().setMyLocationEnabled(true); 
		map.getMap().getUiSettings().setZoomControlsEnabled(false);
		
		 ParseQuery<ParseObject> query = ParseQuery.getQuery("NearByContact");

		 query.fromLocalDatastore();
		 query.findInBackground(new FindCallback<ParseObject>() {
			

			@Override
			public void done(List<ParseObject> obj, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					TextView txt=(TextView)findViewById(R.id.txt_subtitle);
					if(obj.size()>0){
						int total=getIntent().getIntExtra("total_contant", 0);
						txt.setText("SHOWING ("+obj.size()+"/"+total+") OF CONTACTS");
						
						ArrayList<String> searchuser = new ArrayList<String>();
						for(int i=0;i<obj.size();i++){
							searchuser.add(obj.get(i).getString("phone"));
						}
						if(searchuser.size()>0){
							addMarkers(searchuser);
						}
					}
				
				else{
					
					txt.setText("SHOWING NEAR BY USERS");
			
		
		// To Enable the current location "blue dot"
		loc_current = locmgr.getLastKnownLocation(provider);
		ParseUser currentuser=ParseUser.getCurrentUser();
		ParseGeoPoint g=currentuser.getParseGeoPoint("current_location");
		final double cLat=g.getLatitude();
		final double cLong=g.getLongitude();
		
		
		if(loc_current!=null){
			latlngnow=new LatLng(loc_current.getLatitude(),loc_current.getLongitude());
		}
		else
		{
			
		
		latlngnow=new LatLng(cLat, cLong);
		}
		 map.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(latlngnow, 12));
		addMarkers(radius_circle,latlngnow);
		int rad=radius_circle/1000;
		radius.setProgress(rad);
		
		
		
		if(loc_current!=null && ParseUser.getCurrentUser().isAuthenticated()){
			
			
			Log.e("current location is ", loc_current.toString());
			ParseUser.getCurrentUser().put("current_location", new ParseGeoPoint(loc_current.getLatitude(),loc_current.getLongitude()));
			ParseUser.getCurrentUser().saveEventually();			
		}	

		radius.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
        {
            @Override
            public void onStopTrackingTouch(SeekBar seek) 
            {
 
            	if(seek.getProgress()>0){
                	radius_circle=1000*seek.getProgress();
                addMarkers(radius_circle, latlngnow);
                	
                	// map.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(10.046010500000000000,76.323204199999960000), rad));
                }
                else{
                	addMarkers(radius_circle, latlngnow);
                	
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) 
            {
//          	  seekvolume.setProgress(audioManager
//                        .getStreamVolume(AudioManager.STREAM_MUSIC));   


            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) 
            {
            	 //          	  se
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
//                        progress, 0);
            }
        });
		
		
				}}}});
		 
		 
			locmgr.requestLocationUpdates(provider, 1, 5, this);
		
	}
	
	
	public void retrieveUsers()
	{
		ParseQuery<ParseUser> query = ParseUser.getQuery();
	
		query.findInBackground(new FindCallback<ParseUser>() {
		

			@Override
			public void done(List<ParseUser> objects, ParseException e) {
				// TODO Auto-generated method stub
				 if (e == null) {
					 Log.e("Retrieved users", Integer.toString(objects.size()));
				      //  users=new ArrayList<ParseUser>(objects); 
				      //  Log.e("Retrieved users", Integer.toString(users.size()));
				    } else {
				        // Something went wrong.
				    }
			}
		});
	}

	public void drawcircle(double rad,LatLng latlng){
		
try{
			
			map.getMap().addCircle(new CircleOptions()
        .center(latlng)
        .radius(rad)
        
        .strokeColor(Color.BLUE)
        .strokeWidth(1)
       
        .fillColor(Color.parseColor("#4d000000")));
		}
		catch(Exception e){}
		
	}
	
	
	public void addMarkers(final double rad,LatLng latlng){
		map.getMap().clear();
		// map.getMap().animateCamera(CameraUpdateFactory.zoomTo(12));
		  map.getMap().moveCamera(CameraUpdateFactory.newLatLng(latlng));
		//ParseUser currentuser=ParseUser.getCurrentUser();
		ParseGeoPoint g=new ParseGeoPoint(latlng.latitude,latlng.longitude);//currentuser.getParseGeoPoint("current_location");
		//final double cLat=g.getLatitude();
		//final double cLong=g.getLongitude();
		//map.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 14));
		
		drawcircle(rad, latlng);
		
		 mClusterManager = new ClusterManager<Person>(this, map.getMap());
	        mClusterManager.setRenderer(new PersonRenderer());
	       map.getMap().setOnCameraChangeListener(mClusterManager);
	        map.getMap().setOnMarkerClickListener(mClusterManager);
	        map.getMap().setOnInfoWindowClickListener(mClusterManager);
	        mClusterManager.setOnClusterClickListener(this);
	        mClusterManager.setOnClusterInfoWindowClickListener(this);
	        mClusterManager.setOnClusterItemClickListener(this);
	        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

	      
	       
	//	map.getMap().addMarker(new MarkerOptions().position(latlng).title("nikheesh").icon(BitmapDescriptorFactory.fromBitmap(b)));
		
		
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		
		query.whereNear("current_location", g);
		query.whereWithinKilometers("current_location", g,rad/1000);
		query.findInBackground(new FindCallback<ParseUser>() {
		

			@Override
			public void done(List<ParseUser> objects, ParseException e) {
				// TODO Auto-generated method stub
				 if (e == null) {
					 Log.e("Retrieved users", Integer.toString(objects.size()));
					 
					 
					
							Log.e("inside users", "hi");
							for(int i=0;i<objects.size();i++){
								
								ParseGeoPoint gUsers=objects.get(i).getParseGeoPoint("current_location");
								double uLat=gUsers.getLatitude();
								double uLong=gUsers.getLongitude();
								String prof_name=objects.get(i).getString("name");
								
								mClusterManager.addItem(new Person(new LatLng(uLat,uLong), prof_name, R.drawable.user));
								
							
								
								
							}
							
							 mClusterManager.cluster();
				    } else {
				        // Something went wrong.
				    }
			}
		});
		
		
		
		
		
	}
	public void addMarkers(ArrayList<String> searchuser){
		
		
		
		 mClusterManager = new ClusterManager<Person>(this, map.getMap());
	        mClusterManager.setRenderer(new PersonRenderer());
	       map.getMap().setOnCameraChangeListener(mClusterManager);
	        map.getMap().setOnMarkerClickListener(mClusterManager);
	        map.getMap().setOnInfoWindowClickListener(mClusterManager);
	        mClusterManager.setOnClusterClickListener(this);
	        mClusterManager.setOnClusterInfoWindowClickListener(this);
	        mClusterManager.setOnClusterItemClickListener(this);
	        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

		
		
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereContainedIn("phone", searchuser);
		query.whereEqualTo("isLocationPublic", true);
	
		query.findInBackground(new FindCallback<ParseUser>() {
		

			@Override
			public void done(List<ParseUser> objects, ParseException e) {
				// TODO Auto-generated method stub
				 if (e == null) {
					 Log.e("Retrieved users", Integer.toString(objects.size()));
					 
					 
					
							Log.e("inside users", "hi");
							for(int i=0;i<objects.size();i++){
								
								ParseGeoPoint gUsers=objects.get(i).getParseGeoPoint("current_location");
								double uLat=gUsers.getLatitude();
								double uLong=gUsers.getLongitude();
								String prof_name=objects.get(i).getString("name");
								
								mClusterManager.addItem(new Person(new LatLng(uLat,uLong), prof_name, R.drawable.user));
								
							
								
								
							}
							
//							 mClusterManager.cluster();
//							 
//							 LatLngBounds.Builder builder = new LatLngBounds.Builder();
//							 for (Marker marker : mClusterManager.getClusterMarkerCollection().getMarkers()) {
//							     builder.include(marker.getPosition());
//							 }
//							 LatLngBounds bounds = builder.build();
//							 int padding = 0; // offset from edges of the map in pixels
//							 CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//							map.getMap().animateCamera(cu);
							 
							 
							 
				    } else {
				        // Something went wrong.
				    }
			}
		});
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.near_by_map, menu);
		return true;
	}

	@Override
	public void onCameraChange(CameraPosition arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	 @Override
	  public void onDestroy() {
	    super.onDestroy();
//	   if(widIntent!=null){
//		   stopService(widIntent);
//	   }
	  }
	 
	 
	 @Override
	    public void onLocationChanged(Location location) {
	 
	      //  TextView tvLocation = (TextView) findViewById(R.id.tv_location);
	 
		 if(location!=null){
	        // Getting latitude of the current location
	        double latitude = location.getLatitude();
	 
	        // Getting longitude of the current location
	        double longitude = location.getLongitude();
	 
	        // Creating a LatLng object for the current location
	        latlngnow = new LatLng(latitude, longitude);
	 
	        // Showing the current location in Google Map
	     
	 
	        // Zoom in the Google Map
	       
	 addMarkers(radius_circle, latlngnow);
		 }
	        // Setting latitude and longitude in the TextView tv_location
	      //  tvLocation.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );
	 
	    }
	 
	    @Override
	    public void onProviderDisabled(String provider) {
	        // TODO Auto-generated method stub
	    }
	 
	    @Override
	    public void onProviderEnabled(String provider) {
	        // TODO Auto-generated method stub
	    }
	 
	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras) {
	        // TODO Auto-generated method stub
	    }


		
	 
	 
	 
	 

	 
}


