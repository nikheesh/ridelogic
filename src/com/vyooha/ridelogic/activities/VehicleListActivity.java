package com.vyooha.ridelogic.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class VehicleListActivity extends Activity{
	
	ArrayAdapter<String> vehicleAdapter;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     vehicleAdapter = new ArrayAdapter<String>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.vehicle_item, null);

	                     String cont = this.getItem(position);
	                      try{
	             
	               

	                  final TextView name = (TextView)convertView.findViewById(R.id.vehicle_name);
	                  name.setText(cont);
	                  name.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(),ProfileDetails.class);
		  					i.putExtra("model", name.getText().toString());
		  					i.putExtra("choose", true);
		  				startActivity(i);
//		  				stopLoading();
						}
					});

              
	                }
	                      catch (Exception e){}
	                      
	            
	                return convertView;
	            }
	        };


	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	setContentView(R.layout.vehicle_list);
ListView list=(ListView)findViewById(R.id.vehicle_listview);
list.setAdapter(vehicleAdapter);
vehicleAdapter.add("TSX Sport Wagon");
vehicleAdapter.add("A3 e-tron");
vehicleAdapter.add("3 Series Gran Turismo");
vehicleAdapter.add("ActiveHybrid 5");
}
}
