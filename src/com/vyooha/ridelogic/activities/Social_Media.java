package com.vyooha.ridelogic.activities;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.vyooha.ridelogic.views.RoundedImageView;

public class Social_Media extends Activity{
	 ArrayAdapter<Social_Class> socialAdapter;
	 ArrayAdapter<FriendReq_Class> friendAdapter;
	 ArrayAdapter<Notification_Class> notiAdapter;
	 
//   private JSONObject json;
//   private JSONArray jsonarray;
   ListView list,friendlist,notificationlist;
   ParseUser user;
   
   Social_Class socialclass;
   FriendReq_Class friendclass,freq;
   Notification_Class noticlass,notification;
   
   
   RoundedImageView prof_pic;
   ImageView upload_post;
   ImageButton search_edit;
   
   TextView home_txt,friend_txt,notification_txt;
   LinearLayout friendreq_layout;
   
   Social_Class sc;
   
   int nextpageno,resultno,nextpageno1,resultno1;
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     user=ParseUser.getCurrentUser();
	 
	     final DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
	     				.cacheOnDisc(true).resetViewBeforeLoading(true)
	     				//.showImageForEmptyUri(fallback)
	     				//.showImageOnFail(fallback)
	     			//	.showImageOnLoading(fallback)
	     				.build();
	      
	     socialAdapter = new ArrayAdapter<Social_Class>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.socialmedia_item, null);
	                
	                if (position >= getCount() - 4&&resultno==20)
	                {
	                	loadHomeFromParse(nextpageno);
	                }
	              
	                
	                sc=new Social_Class();
	                sc=this.getItem(position);
	                
	                RoundedImageView friend_img=(RoundedImageView)convertView.findViewById(R.id.friend_img);
	                TextView prof_name=(TextView)convertView.findViewById(R.id.prof_name2);
	                TextView post_text=(TextView)convertView.findViewById(R.id.post_text);
	                TextView time_stamp=(TextView)convertView.findViewById(R.id.time_stamp);
	                TextView like_txt=(TextView)convertView.findViewById(R.id.like_txt);
	                TextView comment_txt=(TextView)convertView.findViewById(R.id.comment_txt);
	                LinearLayout cmt_lk_time_layout=(LinearLayout)convertView.findViewById(R.id.coment_like_time_layout);
//ViewPager  viewpager=(ViewPager)findViewById(R.id.image_pager_social);
	                LinearLayout itemlayout=(LinearLayout)convertView.findViewById(R.id.socialmedialinear);
	                ImageView feedimage=(ImageView)convertView.findViewById(R.id.image_social);
	                RelativeLayout feedimage_layout=(RelativeLayout)convertView.findViewById(R.id.image_social_layout);
	                ImageLoader imageLoader = ImageLoader.getInstance();
	  
	                if(sc.name!=null){
	                	Log.e("sc .name", sc.name);
	                	
	                	prof_name.setText(sc.name);
	                	prof_name.setVisibility(View.VISIBLE);
	                	
	                }
	                else{
	                	prof_name.setVisibility(View.GONE);
	                }
	                if(sc.post_text!=null){
	                	post_text.setText(sc.post_text);
	                	post_text.setVisibility(View.VISIBLE);
	                }
	                else
	                {
	                	post_text.setVisibility(View.GONE);
	                }
	                if(sc.no_likes!=0){
	                	like_txt.setText(""+sc.no_likes);
	                	like_txt.setVisibility(View.VISIBLE);
	                	
	                }
	                else{
	                	like_txt.setVisibility(View.GONE);
	                }
	                if(sc.no_comments!=0){
	                	comment_txt.setText(""+sc.no_comments);
	                	comment_txt.setVisibility(View.VISIBLE);
	                	
	                }
	                else{
	                	prof_name.setVisibility(View.GONE);
	                }
	                if(sc.time_stamp!=null){
	                	time_stamp.setText(sc.time_stamp);
	                	time_stamp.setVisibility(View.VISIBLE);
	                	
	                }
	                else{
	                	time_stamp.setVisibility(View.GONE);
	                }
	                
	                if(sc.prof_pic!=null){
	                	
	                	friend_img.setVisibility(View.VISIBLE);
	                	imageLoader.displayImage(sc.prof_pic,friend_img);
	                }
	                else{
	                	friend_img.setVisibility(View.GONE);
	                }
	                
	                if(sc.imageurl!=null){
	                	feedimage_layout.setVisibility(View.VISIBLE);
	                	imageLoader.displayImage(sc.imageurl,feedimage,options);
	                	
	                }
	                else {
						feedimage_layout.setVisibility(View.GONE);
					}
	                
	                feedimage_layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(), SocialSingleNewsFeed.class);
							i.putExtra("postid", sc.id);
							i.putExtra("ownerid", sc.ownerid);
							startActivity(i);
						}
					});
	                
	                cmt_lk_time_layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(), SocialSingleNewsFeed.class);
							i.putExtra("postid", sc.id);
							i.putExtra("ownerid", sc.ownerid);
							startActivity(i);
						}
					});
	                
	                prof_name.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(), SocialProfileView.class);
							
							i.putExtra("userid", sc.ownerid);
							startActivity(i);
						}
					});
	                friend_img.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(), SocialProfileView.class);
							
							i.putExtra("userid", sc.ownerid);
							startActivity(i);
						}
					});
	                post_text.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(), SocialSingleNewsFeed.class);
							i.putExtra("postid", sc.id);
							i.putExtra("ownerid", sc.ownerid);
							startActivity(i);
						}
					});
	                
	                return convertView;
	            }
	        };
	     
	        friendAdapter = new ArrayAdapter<FriendReq_Class>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.friendreq_item, null);
	                
	               freq=new FriendReq_Class();
	                freq=this.getItem(position);
	                
	                RoundedImageView friend_img=(RoundedImageView)convertView.findViewById(R.id.friendreq_img);
	                TextView prof_name=(TextView)convertView.findViewById(R.id.friendreq_name);
	                
	                Button acceptfriend=(Button)convertView.findViewById(R.id.acceptfriendreq_btn);
	                
	                Button declinefriend=(Button)convertView.findViewById(R.id.declinefriendreq_btn);
	                ImageLoader imageLoader = ImageLoader.getInstance();
	          	  
	                prof_name.setText(freq.name);
	                imageLoader.displayImage(freq.profpic, friend_img);
	                
	                acceptfriend.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 HashMap<String, Object> inputparm=new HashMap<String, Object>();
								inputparm.put("target_userId", freq.id);

								ParseCloud.callFunctionInBackground("acceptFriendReq", inputparm, new FunctionCallback<String>() {
						

										@Override
										public void done(String result, ParseException e) {
											// TODO Auto-generated method stub
											if (e == null) {
											friendAdapter.remove(freq);
												
											}}});
						}
					});
	                declinefriend.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 HashMap<String, Object> inputparm=new HashMap<String, Object>();
								inputparm.put("target_userId", freq.id);

								ParseCloud.callFunctionInBackground("rejectFriendReq", inputparm, new FunctionCallback<String>() {
						

										@Override
										public void done(String result, ParseException e) {
											// TODO Auto-generated method stub
											if (e == null) {
											friendAdapter.remove(freq);
												
											}}});
						}
					});
	                
			             
	                return convertView;
	            }};
	            
	            notiAdapter = new ArrayAdapter<Notification_Class>(this, 0) {
		            @Override
		            public View getView(int position, View convertView, ViewGroup parent) {
		                if (convertView == null)
		                    convertView = getLayoutInflater().inflate(R.layout.notification_item, null);
		                
		               notification=new Notification_Class();
		                notification=this.getItem(position);
		                
		                RoundedImageView friend_img=(RoundedImageView)convertView.findViewById(R.id.notification_img);
		                TextView prof_name=(TextView)convertView.findViewById(R.id.notification_name);
		                ImageLoader imageLoader = ImageLoader.getInstance();
		          	  prof_name.setText(notification.notify);
		          	  imageLoader.displayImage(notification.profpic,friend_img);
		                
		                RelativeLayout itemlayout=(RelativeLayout)convertView.findViewById(R.id.itemlayout);
		                itemlayout.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if(notification.action.equals("accept_req")){
									Intent i=new Intent(getApplicationContext(), SocialProfileView.class);
									
									i.putExtra("userid", notification.postid);
									startActivity(i);
								}
								
								else{
									Intent i=new Intent(getApplicationContext(), SocialSingleNewsFeed.class);
									i.putExtra("postid", notification.postid);
									i.putExtra("ownerid", user.getObjectId());
									startActivity(i);
								}
								
								
							}
						});
		                
		                
		                return convertView;
		            }};
	            
	        
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.socialmedia_listview);
	        
	        list=(ListView)findViewById(R.id.social_listview);
	        list.setAdapter(socialAdapter);
	        
	        friendlist=(ListView)findViewById(R.id.friendreq_listview);
	        friendlist.setAdapter(friendAdapter);
	        
	        notificationlist=(ListView)findViewById(R.id.notification_listview);
	        notificationlist.setAdapter(notiAdapter);
	      
	     // TextView home_txt,friend_txt,notification_txt;
	        home_txt=(TextView)findViewById(R.id.home_txt);
	        notification_txt=(TextView)findViewById(R.id.notification_txt);
	        friend_txt=(TextView)findViewById(R.id.friend_txt);
	       friendreq_layout=(LinearLayout)findViewById(R.id.friendrequest_layout);
	      
	        nextpageno=0;
	        resultno=0;
	        resultno1=nextpageno1=0;
	        prof_pic =(RoundedImageView)findViewById(R.id.social_prof_img);
	        ParseFile pf=user.getParseFile("profile_pic");
	        byte[] bitmapdata;
	        Bitmap bitmap = null;
			try {
				bitmapdata = pf.getData();
				 bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
			}
			catch (Exception e){
				Log.e("", e.toString());
			}
			if(bitmap!=null)
			{
				prof_pic.setImageBitmap(bitmap);
			}
	        prof_pic.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 PopupMenu popup = new PopupMenu(getApplicationContext(), prof_pic);
		                //Inflating the Popup using xml file
		                popup.getMenuInflater()
		                    .inflate(R.menu.profilemenu, popup.getMenu());
		                
		                //registering popup with OnMenuItemClickListener
		                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		                    public boolean onMenuItemClick(MenuItem item) {
		                    	int id=item.getItemId();
		                     switch (id) {
							case R.id.one:
								Intent i=new Intent(getApplicationContext(), SocialProfileView.class);
								i.putExtra("userid", user.getObjectId());
								startActivity(i);
								break;
							case R.id.three:
								Intent i1=new Intent(getApplicationContext(), MainMenuActivity.class);
								
								startActivity(i1);
								break;
							case R.id.four:
								ParseUser.logOut();				
								  startActivity(new Intent(getApplicationContext(), FirstpageActivity.class));
								break;
							default:
								break;
							}
		                    	   
		                       
		                    	
		                    	
		                        return true;
		                    }
		                });

		                popup.show(); //showing popup menu
				}
			});
	        upload_post=(ImageView)findViewById(R.id.open_upload_post);
			upload_post.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(), SocialMediaPost.class);
					startActivity(i);
					
				}
			});
			
			search_edit=(ImageButton)findViewById(R.id.searchpeople_main);
			search_edit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Intent i=new Intent(getApplicationContext(), Social_SearchPeople.class);
					startActivity(i);
				}
			});
	        
	       homeLoad();
	       
	       home_txt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				homeLoad();
			}
		});
	       notification_txt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				notificationLoad();
			}
		});
	       friend_txt.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				friendReqLoad();
			}
		});
	       
						
			
	 }

	 public void homeLoad(){
		   home_txt.setBackgroundColor(Color.parseColor("#000000"));
	        home_txt.setTextColor(Color.parseColor("#ffffff"));
	        notification_txt.setBackgroundColor(Color.parseColor("#ffffff"));
	        notification_txt.setTextColor(Color.parseColor("#000000"));
	        friend_txt.setBackgroundColor(Color.parseColor("#ffffff"));
	        friend_txt.setTextColor(Color.parseColor("#000000"));
	        friendreq_layout.setBackgroundColor(Color.parseColor("#ffffff"));
	       
	        list.setVisibility(View.VISIBLE);
	        friendlist.setVisibility(View.GONE);
	        notificationlist.setVisibility(View.GONE);
	        socialAdapter.clear();
	        nextpageno=0;
	        loadHomeFromParse(nextpageno);
	       

	        
	       
	 }
	 public void loadHomeFromParse(int pageno){
		 
		 HashMap<String, Object> inputparm=new HashMap<String, Object>();
			inputparm.put("pageno", pageno);

			ParseCloud.callFunctionInBackground("showNewsFeed", inputparm, new FunctionCallback<List<ParseObject>>() {
	

					@Override
					public void done(List<ParseObject> result, ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							resultno=result.size();
							 nextpageno++;
							for(int i=0;i<result.size();i++){
								ParseObject obj=result.get(i);
								ParseObject userobj=obj.getParseObject("post_by");
								ParseFile pf=obj.getParseFile("main_photo");
								ParseFile pf2=userobj.getParseFile("profile_pic");
								
								//String name=result.get(i).getString("name");
							 socialclass=new Social_Class();
							// socialclass.name=name;
							 if(pf!=null)
							 socialclass.imageurl=pf.getUrl();
							 if(pf2!=null)
							 socialclass.prof_pic=pf2.getUrl();
							 socialclass.no_comments=Integer.parseInt(obj.getNumber("commentCount").toString());
							 socialclass.no_likes=Integer.parseInt(obj.getNumber("LIKECount").toString());
							 socialclass.id=obj.getObjectId();
							 socialclass.post_text=obj.getString("post_text");
							 
							 Date d= obj.getCreatedAt();
							 socialclass.time_stamp=CommonFunctions.getTimeStamp(d);
							 
							 
							 socialclass.name=userobj.getString("name");
							 socialclass.ownerid=userobj.getObjectId();
							 socialAdapter.add(socialclass);
							
							}
						}
					}});
		 
	 }
 public void loadNotificationFromParse(int pageno){
		 
		 HashMap<String, Object> inputparm=new HashMap<String, Object>();
			inputparm.put("pageno", pageno);

			ParseCloud.callFunctionInBackground("showNotifications", inputparm, new FunctionCallback<List<ParseObject>>() {
	

					@Override
					public void done(List<ParseObject> result, ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							resultno1=result.size();
							 nextpageno1++;
							for(int i=0;i<result.size();i++){
								ParseObject obj=result.get(i);
								ParseObject userobj=obj.getParseObject("last_action_user");
								ParseObject postobj=obj.getParseObject("post_obj");
								ParseFile pf2=userobj.getParseFile("profile_pic");
								
								//String name=result.get(i).getString("name");
							 noticlass=new Notification_Class();
							// socialclass.name=name;
							 if(pf2!=null)
							 
							 noticlass.profpic=pf2.getUrl();
							
							String name,action,actionstring,sentance,posttype;
							int noofothers;
							name=userobj.getString("name");
							action=obj.getString("action_type");
							
							if(!action.equals("accept_req")){
							noofothers=Integer.parseInt(obj.getNumber("total_action_count").toString());
							noofothers--;
							posttype=postobj.getString("post_type");
							if(posttype.equals("text")){
								posttype="post";
							}
							else
								posttype="photo";
							
							noticlass.postid=postobj.getObjectId();
							if(action.equals("like")){
								actionstring="liked";
							}
							else
							{
								actionstring="commented";
							}
							
							sentance=" "+name+" and "+noofothers+" others "+actionstring+" on your "+posttype+".";
							
							
							}
							else{
								noticlass.postid=userobj.getObjectId();
								sentance=" "+name+" accepted your friend request.";
							}
								noticlass.action=action;
							 noticlass.notify=sentance;
							notiAdapter.add(noticlass);
							
							}
						}
					}});
		 
	 }
	
	 public void friendReqLoad(){
		friend_txt.setBackgroundColor(Color.parseColor("#000000"));
	       friend_txt.setTextColor(Color.parseColor("#ffffff"));
	        notification_txt.setBackgroundColor(Color.parseColor("#ffffff"));
	        notification_txt.setTextColor(Color.parseColor("#000000"));
	        home_txt.setBackgroundColor(Color.parseColor("#ffffff"));
	        home_txt.setTextColor(Color.parseColor("#000000"));
	        friendreq_layout.setBackgroundColor(Color.parseColor("#FFD231"));
		       
	        list.setVisibility(View.GONE);
	        friendlist.setVisibility(View.VISIBLE);
	        notificationlist.setVisibility(View.GONE);
	        friendAdapter.clear();
	        HashMap<String, Object> inputparm=new HashMap<String, Object>();
			

			ParseCloud.callFunctionInBackground("viewFriendReq", inputparm, new FunctionCallback<List<ParseObject>>() {
	

					@Override
					public void done(List<ParseObject> result, ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							
							for(int i=0;i<result.size();i++){
								ParseObject obj=result.get(i);
								ParseObject userobj=obj.getParseObject("fromUser");
							
								ParseFile pf2=userobj.getParseFile("profile_pic");
								
								//String name=result.get(i).getString("name");
							 friendclass=new FriendReq_Class();
							// socialclass.name=name;
							 if(pf2!=null)
							 friendclass.profpic=pf2.getUrl();
							
							 friendclass.name=userobj.getString("name");
							 friendclass.id=userobj.getObjectId();
							 friendAdapter.add(friendclass);
							}
						}
					}});


	 }
public void notificationLoad(){
	 notification_txt.setBackgroundColor(Color.parseColor("#000000"));
     notification_txt.setTextColor(Color.parseColor("#ffffff"));
     home_txt.setBackgroundColor(Color.parseColor("#ffffff"));
     home_txt.setTextColor(Color.parseColor("#000000"));
     friend_txt.setBackgroundColor(Color.parseColor("#ffffff"));
     friend_txt.setTextColor(Color.parseColor("#000000"));
     friendreq_layout.setBackgroundColor(Color.parseColor("#FFD231"));
     
     list.setVisibility(View.GONE);
     friendlist.setVisibility(View.GONE);
     notificationlist.setVisibility(View.VISIBLE);
     notiAdapter.clear();
     nextpageno1=0;
     loadNotificationFromParse(nextpageno1);
    
     
//     HashMap<String, Object> inputparm=new HashMap<String, Object>();
//		inputparm.put("pageno", 1);
//
//		ParseCloud.callFunctionInBackground("showNewsFeed", inputparm, new FunctionCallback<List<ParseObject>>() {
//
//
//				@Override
//				public void done(List<ParseObject> result, ParseException e) {
//					// TODO Auto-generated method stub
//					if (e == null) {
//						
//						for(int i=0;i<result.size();i++){
//							ParseObject obj=result.get(i);
//							ParseObject userobj=obj.getParseObject("post_by");
//							ParseFile pf=obj.getParseFile("main_photo");
//							ParseFile pf2=userobj.getParseFile("profile_pic");
//							
//							//String name=result.get(i).getString("name");
//						 socialclass=new Social_Class();
//						// socialclass.name=name;
//						 if(pf!=null)
//						 socialclass.imageurl=pf.getUrl();
//						 socialclass.prof_pic=pf2.getUrl();
//						 socialclass.no_comments=Integer.parseInt(obj.getNumber("commentCount").toString());
//						 socialclass.no_likes=Integer.parseInt(obj.getNumber("LIKECount").toString());
//						 socialclass.id=obj.getObjectId();
//						 socialclass.post_text=obj.getString("post_text");
//						 
//						
//						 socialclass.name=userobj.getString("name");
//						 socialAdapter.add(socialclass);
//						}
//					}
//				}});


}


}
