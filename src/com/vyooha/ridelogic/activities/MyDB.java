package com.vyooha.ridelogic.activities;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.animation.FloatEvaluator;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MyDB{  

private MyDatabaseHelper dbHelper;  

private SQLiteDatabase database;  

public  String NOTI_TABLE="Notification"; // name of table 

//public  String DATE="user";
//public  String HEAD="tripname"; // id value for employee
//public  String BODY="tripdist"; 

// name of employee
/** 
 * 
 * @param context 
 */  
public MyDB(Context context){  
    dbHelper = new MyDatabaseHelper(context);  
    database = dbHelper.getWritableDatabase(); 
   // Log.d("database object in mydb constructor", database.toString());
}

// "create table UserTrips(user text,vehicle text,obid text,tripname text,tripdist integer,triponoff text,isActive text,flag text);";
//"create table FuelFillings(user text,vehicle text,litre real,cost real,amount real,flag text);";
//"create table UserDistByDay(user text,vehicle text,date_id text,dist_covered real,flag text);";

public long insert_UserTrips(String user,String vehicle,String obid,String tripname, int tripdist,String triponoff,String isActive,String flag){  
	
   ContentValues values = new ContentValues(); 
   
   values.put("user", user); 
 
   values.put("vehicle", vehicle);  
   values.put("obid", obid);
   values.put("tripname", tripname);
   values.put("tripdist", tripdist);
   
   values.put("triponoff",triponoff);
   values.put("isActive", isActive);
   values.put("flag", flag);
   
   
   long l = database.insert("UserTrips", null, values);
  
   return l;
}  

public long insert_FuelFillings(String user,String vehicle,double litre,double cost,double amount,String flag){  
	
	   ContentValues values = new ContentValues(); 
	   
	   values.put("user", user); 
	 
	   values.put("vehicle", vehicle);  
	   values.put("litre", litre);
	   values.put("cost", cost);
	   values.put("amount", amount);
	   
	 
	   values.put("flag", flag);
	   
	   
	   long l = database.insert("FuelFillings", null, values);
	  
	   return l;
	}  
//"create table UserDistByDay(user text,vehicle text,date_id text,dist_covered real,flag text);";

public long insert_UserDistByDay(String user,String vehicle,String date_id ,double dist_covered,String flag){  
	
	   ContentValues values = new ContentValues(); 
	   
	   values.put("user", user); 
	 
	   values.put("vehicle", vehicle);  
	   values.put("date_id", date_id);
	   values.put("dist_covered", dist_covered);
	  
	   
	 
	   values.put("flag", flag);
	   
	   
	   long l = database.insert("UserDistByDay", null, values);
	  
	   return l;
	}  




public Cursor selectRecords(String user) {
   String[] cols = new String[] {"vehicle","user",BODY,"triponoff","isActive","flag" }; 
   
   String[] whereArgs = new String[] {
		    user
		    
		};
   
   Cursor mCursor = database.query(true, NOTI_TABLE,cols,DATE+"=?"
            , whereArgs, null, null, HEAD, null);  
   if (mCursor != null) {  
     mCursor.moveToFirst();  
   }  
   return mCursor; 
   // iterate to get each value.
}
public Cursor selectRecordItem(String user,String tripname) {
	
	String whereClause = DATE+"=? AND "+HEAD+"=?";
	String[] whereArgs = new String[] {
	    user,
	    tripname
	};
	
	
	   String[] cols = new String[] {DATE,HEAD,BODY,"triponoff" };  
	   Cursor mCursor = database.query(true, NOTI_TABLE,cols, whereClause
	            , whereArgs, null, null, HEAD, null);  
	   if (mCursor != null) {  
	     mCursor.moveToFirst();  
	   }  
	   return mCursor; 
	   // iterate to get each value.
	}


public Cursor selectRecordOnTripOn(String user,boolean triponoff) {
	String whereClause = DATE+"=? AND triponoff=?";
	String[] whereArgs = new String[] {
	    user,
	    Boolean.toString(triponoff)
	};
	
	   String[] cols = new String[] {DATE,HEAD,BODY,"triponoff" };  
	   Cursor mCursor = database.query(true, NOTI_TABLE,cols, whereClause
	            , whereArgs, null, null, HEAD, null);  
	   if (mCursor != null) {  
	     mCursor.moveToFirst();  
	   }  
	   return mCursor; 
	   // iterate to get each value.
	}


public Cursor selectAll() {
	   
	String[] cols = new String[] {DATE,HEAD,BODY,"triponoff" };  
	   Cursor mCursor = database.query(true, NOTI_TABLE,cols,null  
	            , null, null, null, HEAD, null);  
	   if (mCursor != null) {  
	     mCursor.moveToFirst();  
	   }  
	   return mCursor; 
	   // iterate to get each value.
	}

public void updatetrip(String user,String tripname,boolean triponoff){
	ContentValues cv = new ContentValues();
	cv.put("triponoff",Boolean.toString(triponoff)); 
	String whereClause = DATE+"=? AND "+HEAD+"=?";
	String[] whereArgs = new String[] {
	    user,
	    tripname
	};
	database.update(NOTI_TABLE, cv, whereClause, whereArgs);
	
	//database.execSQL("UPDATE "+NOTI_TABLE+" SET triponoff=\'"+Boolean.toString(triponoff)+"\'  WHERE "+HEAD+"="+tripname+" AND "+DATE+"="+user+";");
}
public void resettrip(String user,String tripname,int dist){
	
	ContentValues cv = new ContentValues();
	cv.put(BODY,dist); 
	String whereClause = DATE+"=? AND "+HEAD+"=?";
	String[] whereArgs = new String[] {
	    user,
	    tripname
	};
	database.update(NOTI_TABLE, cv, whereClause, whereArgs);
	
	
	//database.execSQL("UPDATE "+NOTI_TABLE+" SET "+BODY+"="+dist+"  WHERE "+HEAD+"="+tripname+" AND "+DATE+"="+user+";");
}
public int size(){
return (int) DatabaseUtils.queryNumEntries(database, "Notification");
}

public void deleterow(String user,String tripname)
{ 
	Log.d("delete row got executed", "delete");
	String whereClause = DATE+"=? AND "+HEAD+"=?";
	String[] whereArgs = new String[] {
	    user,
	    tripname
	};
	database.delete(NOTI_TABLE, whereClause, whereArgs);
	//database.execSQL("DELETE FROM "+NOTI_TABLE+"  WHERE "+HEAD+"="+tripname+" AND "+DATE+"="+user+";");
}
public void droptable(){
	Log.d("drop got executed", "drop");
	database.execSQL("DROP TABLE "+NOTI_TABLE);

}

public void close(){
	Log.d("database", "db closed");
	database.close();
}

public void createtable(){
	Log.d("create got executed", "create");
	database.execSQL("CREATE TABLE "+NOTI_TABLE+"("+DATE+","+HEAD+","+BODY+");");

}
}