	package com.vyooha.ridelogic.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
 
public class AndroidBuildingMusicPlayerActivity extends Activity implements OnCompletionListener, SeekBar.OnSeekBarChangeListener {
 
    private ImageButton btnPlay;
//    private ImageButton btnForward;
//    private ImageButton btnBackward;
    private ImageButton btnNext;
    private ImageButton btnPrevious;
    private ImageButton btnPlaylist;
    private ImageButton btnRepeat;
    private ImageButton btnShuffle;
    private ImageButton btnMute;
    private SeekBar songProgressBar;
    private TextView songTitleLabel;
    private TextView songCurrentDurationLabel;
    private TextView songTotalDurationLabel;
    private ImageView music_image;
    
  private  AudioManager audiomanager=null;
  
    private int currentVolume;
    
     ParseApplication appState;
    
    // Media Player
    public static  MediaPlayer mp;
    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();;
    public static SongsManager songManager;
    private Utilities utils;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000; // 5000 milliseconds
    
   
    

    private ArrayList<HashMap<String, String>> songsList;// = new ArrayList<HashMap<String, String>>();
 
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
//        WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
//        layoutParams.dimAmount = .3f;
//        this.getWindow().setAttributes(layoutParams);
       
        
        setContentView(R.layout.musicplayer_activity);
        ImageView close=(ImageView)findViewById(R.id.close_img);
        
        
        
        // All player buttons
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
//        btnForward = (ImageButton) findViewById(R.id.btnForward);
//        btnBackward = (ImageButton) findViewById(R.id.btnBackward);
        
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
        btnPlaylist = (ImageButton) findViewById(R.id.btnPlaylist);
        btnRepeat = (ImageButton) findViewById(R.id.btnRepeat);
        btnShuffle = (ImageButton) findViewById(R.id.btnShuffle);
        btnMute = (ImageButton) findViewById(R.id.btnMute);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songTitleLabel = (TextView) findViewById(R.id.songTitle);
        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        music_image=(ImageView)findViewById(R.id.music_prof_img);
        // Mediaplayer
        audiomanager=(AudioManager) getSystemService(Context.AUDIO_SERVICE);
        currentVolume=audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC);
        
     //   mp = new MediaPlayer();
         appState = ((ParseApplication)getApplicationContext());
        mp = appState.mp;
        
        songManager = appState.songManager;//new SongsManager();
        utils = appState.utils;//new Utilities();
 
        // Listeners
        songProgressBar.setOnSeekBarChangeListener(this); // Important
        mp.setOnCompletionListener(this); // Important
 
        // Getting all songs list
        songsList = appState.songsList;//songManager.getPlayList(this);
 
        // By default play first song
        Log.d("SONGTAG", songsList.toString());
        Log.d("SONGTAGSIZE", Integer.toString(songsList.size()));
        
        if(mp.isPlaying()){
        	
        	//playSong(appState.currentSongIndex);
        	 String songTitle = songsList.get(appState.currentSongIndex).get("songTitle");
             songTitleLabel.setText(songTitle);
  
             // Changing Button Image to pause image
             btnPlay.setImageResource(R.drawable.btn_pause);
  
             android.media.MediaMetadataRetriever mmr = new MediaMetadataRetriever();
             mmr.setDataSource(songsList.get(appState.currentSongIndex).get("songPath"));
            
             byte [] data = mmr.getEmbeddedPicture();
                    //coverart is an Imageview object

             // convert the byte array to a bitmap
             if(data != null)
             {
                 Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                 music_image.setImageBitmap(bitmap); //associated cover art in bitmap
                // coverart.setAdjustViewBounds(true);
                // coverart.setLayoutParams(new LinearLayout.LayoutParams(500, 500));
             }
             else
             {
                 music_image.setImageResource(R.drawable.music_prof); //any default cover resourse folder
//                 coverart.setAdjustViewBounds(true);
//                 coverart.setLayoutParams(new LinearLayout.LayoutParams(500,500 ));
             }
             
             // set Progress bar values
             songProgressBar.setProgress(0 );
             songProgressBar.setMax(100);
  
             // Updating progress bar
             updateProgressBar();
        }
        else{
        if(songsList.size()>0){
        	//playSong(appState.currentSongIndex);
        	if(appState.musiconcreate){
        	   mp.start();
               
               // Displaying Song title
               String songTitle = songsList.get(appState.currentSongIndex).get("songTitle");
               songTitleLabel.setText(songTitle);
    
               // Changing Button Image to pause image
               btnPlay.setImageResource(R.drawable.btn_pause);
    
               android.media.MediaMetadataRetriever mmr = new MediaMetadataRetriever();
               mmr.setDataSource(songsList.get(appState.currentSongIndex).get("songPath"));
              
               byte [] data = mmr.getEmbeddedPicture();
                      //coverart is an Imageview object

               // convert the byte array to a bitmap
               if(data != null)
               {
                   Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                   music_image.setImageBitmap(bitmap); //associated cover art in bitmap
                  // coverart.setAdjustViewBounds(true);
                  // coverart.setLayoutParams(new LinearLayout.LayoutParams(500, 500));
               }
               else
               {
                   music_image.setImageResource(R.drawable.music_prof); //any default cover resourse folder
//                   coverart.setAdjustViewBounds(true);
//                   coverart.setLayoutParams(new LinearLayout.LayoutParams(500,500 ));
               }
               
               // set Progress bar values
               songProgressBar.setProgress(0);
               songProgressBar.setMax(100);
    
               // Updating progress bar
               updateProgressBar();

        	
        	  if(mp!=null){
                  mp.pause();
                  // Changing button image to play button
                  btnPlay.setImageResource(R.drawable.btn_play);
              }
        }
        	else{
        		playSong(appState.currentSongIndex);
        		 if(mp!=null){
                     mp.pause();
                     // Changing button image to play button
                     btnPlay.setImageResource(R.drawable.btn_play);
                 			}
        		 appState.musiconcreate=true;
        		}
        	
        }
        else{
        	Toast.makeText(getApplicationContext(), "No songs in your sd card", Toast.LENGTH_LONG).show();
        }
        }
        
        /**
         * Play button click event
         * plays a song and changes button to pause image
         * pauses a song and changes button to play image
         * */
        btnPlay.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
                // check for already playing
                if(mp.isPlaying()){
                    if(mp!=null){
                    //appState.pausepositition=mp.getCurrentPosition();
                        mp.pause();
                        // Changing button image to play button
                        btnPlay.setImageResource(R.drawable.btn_play);
                        
                    }
                }else{
                    // Resume song
                    if(mp!=null){
                        mp.start();
                        // Changing button image to pause button
                        btnPlay.setImageResource(R.drawable.btn_pause);
                    }
                }
 
            }
        });
 
        btnMute.setOnClickListener(new View.OnClickListener() {
        	 
            @Override
            public void onClick(View arg0) {
                // check for already playing
            	currentVolume=audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC);
            	if(currentVolume>0){
            	audiomanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
            	btnMute.setImageResource(R.drawable.btnmute);
            	 Toast.makeText(getApplicationContext(), "Mute ", Toast.LENGTH_LONG).show();
                 
            	}
            	else
            	{
            		audiomanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            		btnMute.setImageResource(R.drawable.btnunmute);
            Toast.makeText(getApplicationContext(), "volume increase", Toast.LENGTH_LONG).show();
            	}
                    
                }});
        
        /**
         * Forward button click event
         * Forwards song specified seconds
         * */
//        btnForward.setOnClickListener(new View.OnClickListener() {
// 
//            @Override
//            public void onClick(View arg0) {
//                // get current song position
//                int currentPosition = mp.getCurrentPosition();
//                // check if seekForward time is lesser than song duration
//                if(currentPosition + seekForwardTime <= mp.getDuration()){
//                    // forward song
//                    mp.seekTo(currentPosition + seekForwardTime);
//                }else{
//                    // forward to end position
//                    mp.seekTo(mp.getDuration());
//                }
//            }
//        });
// 
//        /**
//         * Backward button click event
//         * Backward song to specified seconds
//         * */
//        btnBackward.setOnClickListener(new View.OnClickListener() {
// 
//            @Override
//            public void onClick(View arg0) {
//                // get current song position
//                int currentPosition = mp.getCurrentPosition();
//                // check if seekBackward time is greater than 0 sec
//                if(currentPosition - seekBackwardTime >= 0){
//                    // forward song
//                    mp.seekTo(currentPosition - seekBackwardTime);
//                }else{
//                    // backward to starting position
//                    mp.seekTo(0);
//                }
// 
//            }
//        });
// 
        /**
         * Next button click event
         * Plays starttrip song by taking currentSongIndex + 1
         * */
        btnNext.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
                // check if starttrip song is there or not
                if(appState.currentSongIndex < (songsList.size() - 1)){
                    playSong(appState.currentSongIndex + 1);
                    appState.currentSongIndex = appState.currentSongIndex + 1;
                    appState.editor.putInt("index", appState.currentSongIndex);
            		appState.editor.commit();
                }else{
                    // play first song
                    playSong(0);
                    appState.currentSongIndex = 0;
                    appState.editor.putInt("index", appState.currentSongIndex);
            		appState.editor.commit();
                }
 
            }
        });
 
        /**
         * Back button click event
         * Plays previous song by currentSongIndex - 1
         * */
        btnPrevious.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
                if(appState.currentSongIndex > 0){
                    playSong(appState.currentSongIndex - 1);
                    appState.currentSongIndex = appState.currentSongIndex - 1;
                    appState.editor.putInt("index", appState.currentSongIndex);
            		appState.editor.commit();
                }else{
                    // play last song
                    playSong(songsList.size() - 1);
                    appState.currentSongIndex = songsList.size() - 1;
                    appState.editor.putInt("index", appState.currentSongIndex);
            		appState.editor.commit();
                }
 
            }
        });
 
        /**
         * Button Click event for Repeat button
         * Enables repeat flag to true
         * */
        btnRepeat.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
                if(appState.isRepeat){
                    appState.isRepeat = false;
                    Toast.makeText(getApplicationContext(), "Repeat is OFF", Toast.LENGTH_SHORT).show();
                    btnRepeat.setImageResource(R.drawable.btnrepeat);
                }else{
                    // make repeat to true
                    appState.isRepeat = true;
                    Toast.makeText(getApplicationContext(), "Repeat is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    appState.isShuffle = false;
                    btnRepeat.setImageResource(R.drawable.btnrepeat);
                    btnShuffle.setImageResource(R.drawable.btnshuffle);
                }
            }
        });
 
        /**
         * Button Click event for Shuffle button
         * Enables shuffle flag to true
         * */
        btnShuffle.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
                if(appState.isShuffle){
                    appState.isShuffle = false;
                    Toast.makeText(getApplicationContext(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
                    btnShuffle.setImageResource(R.drawable.btnshuffle);
                }else{
                    // make repeat to true
                    appState.isShuffle= true;
                    Toast.makeText(getApplicationContext(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    appState.isRepeat = false;
                    btnShuffle.setImageResource(R.drawable.btnshuffle);
                    btnRepeat.setImageResource(R.drawable.btnrepeat);
                }
            }
        });
 
        /**
         * Button Click event for Play list click event
         * Launches list activity which displays list of songs
         * */
        btnPlaylist.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), PlayListActivity.class);
                startActivityForResult(i, 100);
            }
        });
        
        
        close.setOnClickListener(new OnClickListener()
 		 {
 			 

 			@Override
 			public void onClick(View v) {
 				finish();
 			}
 			});
        
        
 
    }
 
    /**
     * Receiving song index from playlist view
     * and play the song
     * */
    @Override
    protected void onActivityResult(int requestCode,
                                     int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 100){
        	appState.currentSongIndex = data.getExtras().getInt("songIndex");
             // play selected song
             playSong(appState.currentSongIndex);
        }
 
    }
 
    /**
     * Function to play a song
     * @param songIndex - index of song
     * */
    public void  playSong(int songIndex){
        // Play song
        try {
            mp.reset();
            mp.setDataSource(songsList.get(songIndex).get("songPath"));
            mp.prepare();
            mp.start();
           
            // Displaying Song title
            String songTitle = songsList.get(songIndex).get("songTitle");
            songTitleLabel.setText(songTitle);
 
            // Changing Button Image to pause image
            btnPlay.setImageResource(R.drawable.btn_pause);
 
            android.media.MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(songsList.get(songIndex).get("songPath"));
           
            byte [] data = mmr.getEmbeddedPicture();
                   //coverart is an Imageview object

            // convert the byte array to a bitmap
            if(data != null)
            {
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                music_image.setImageBitmap(bitmap); //associated cover art in bitmap
               // coverart.setAdjustViewBounds(true);
               // coverart.setLayoutParams(new LinearLayout.LayoutParams(500, 500));
            }
            else
            {
                music_image.setImageResource(R.drawable.music_prof); //any default cover resourse folder
//                coverart.setAdjustViewBounds(true);
//                coverart.setLayoutParams(new LinearLayout.LayoutParams(500,500 ));
            }
            
            // set Progress bar values
            songProgressBar.setProgress(0);
            songProgressBar.setMax(100);
 
            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    /**
     * Update timer on seekbar
     * */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }   
 
    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
           public void run() {
               long totalDuration = mp.getDuration();
               long currentDuration = mp.getCurrentPosition();
               long remaining=totalDuration-currentDuration;
 
               // Displaying Total Duration time
               songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
               // Displaying time completed playing
               songCurrentDurationLabel.setText("-"+utils.milliSecondsToTimer(remaining));
 
               // Updating progress bar
               int progress = (int)(utils.getProgressPercentage(currentDuration, totalDuration));
               //Log.d("Progress", ""+progress);
               songProgressBar.setProgress(progress);
 
               // Running this thread after 100 milliseconds
               mHandler.postDelayed(this, 100);
           }
        };
 
    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
 
    }
 
    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }
 
    /**
     * When user stops moving the progress hanlder
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mp.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
 
        // forward or backward to certain seconds
        mp.seekTo(currentPosition);
 
        // update timer progress again
        updateProgressBar();
    }
 
    /**
     * On Song Playing completed
     * if repeat is ON play same song again
     * if shuffle is ON play random song
     * */
    @Override
    public void onCompletion(MediaPlayer arg0) {
 Log.e("trigger===============", "music player");
        // check for repeat is ON or OFF
        if(appState.isRepeat){
            // repeat is on play same song again
            playSong(appState.currentSongIndex);
            appState.editor.putInt("index", appState.currentSongIndex);
    		appState.editor.commit();
        } else if(appState.isShuffle){
            // shuffle is on - play a random song
            Random rand = new Random();
            appState.currentSongIndex = rand.nextInt((songsList.size() - 1) - 0 + 1) + 0;
            playSong(appState.currentSongIndex);
            appState.editor.putInt("index", appState.currentSongIndex);
    		appState.editor.commit();
            
        } else{
            // no repeat or shuffle ON - play starttrip song
            if(appState.currentSongIndex < (songsList.size() - 1)){
                playSong(appState.currentSongIndex + 1);
                appState.currentSongIndex = appState.currentSongIndex + 1;
                appState.editor.putInt("index", appState.currentSongIndex);
        		appState.editor.commit();
            }else{
                // play first song
                playSong(0);
                appState.currentSongIndex = 0;
                appState.editor.putInt("index", appState.currentSongIndex);
        		appState.editor.commit();
            }
        }
    }
 
    @Override
     public void onDestroy(){
     super.onDestroy();
    
       // mp.release();
     }
 
}