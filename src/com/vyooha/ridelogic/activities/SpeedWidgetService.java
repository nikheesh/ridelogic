package com.vyooha.ridelogic.activities;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.vyooha.ridelogic.views.ArcProgress;
import com.vyooha.ridelogic.views.widgetSpeedView;

public class SpeedWidgetService extends Service {

	  private WindowManager windowManager;
	  private ImageView chatHead;

	  @Override public IBinder onBind(Intent intent) {
	    // Not used
	    return null;
	  }

	  @Override public int onStartCommand(Intent intent, int flags, int startId) {
	    	super.onStartCommand(intent, flags, startId);

	    windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

	    chatHead = new ImageView(this);
	    
	    widgetSpeedView myView = new widgetSpeedView(getApplicationContext());
        myView.measure(150,150);
        myView.layout(0,0,150,150);
     //  myView.setBackgroundColor(Color.parseColor("#aaaaaa"));
        
        myView.setProgress(0);
        myView.setDrawingCacheEnabled(true);
        Bitmap bitmap=myView.getDrawingCache();
	    
	    chatHead.setImageBitmap(bitmap);
	  
	    
	    
	    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {

		    	try{
		    	
		        // Called when a new location is found by the network location provider.
		    	
		    	int pvalue=(int)(location.getSpeed()*18/5); 
		    	 
			    widgetSpeedView myView = new widgetSpeedView(getApplicationContext());
		        myView.measure(150,150);
		        myView.layout(0,0,150,150);
		     //  myView.setBackgroundColor(Color.parseColor("#aaaaaa"));
		        
		        myView.setProgress(pvalue);
		        myView.setDrawingCacheEnabled(true);
		        Bitmap bitmap=myView.getDrawingCache();
			    
			    chatHead.setImageBitmap(bitmap);
		//	    chatHead.setImageResource(R.drawable.android_head);

		    	}
		    	catch(Exception e){
		    		Log.d("errorinservice", e.toString());
		    	}
			  
			 
		
	
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}
		  };

		  
		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 0, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
		
		  
		final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
			        WindowManager.LayoutParams.WRAP_CONTENT,
			        WindowManager.LayoutParams.WRAP_CONTENT,
			        WindowManager.LayoutParams.TYPE_PHONE,
			        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
			        PixelFormat.TRANSLUCENT);

			    params.gravity = Gravity.BOTTOM | Gravity.RIGHT;
			    params.x = 20;
			    params.y = 100;
			    Log.e("=====start profile service===", "");
			    windowManager.addView(chatHead, params);
//		chatHead.setOnTouchListener(new View.OnTouchListener() {
//			  private int initialX;
//			  private int initialY;
//			  private float initialTouchX;
//			  private float initialTouchY;
//
//			  @Override public boolean onTouch(View v, MotionEvent event) {
//			    switch (event.getAction()) {
//			      case MotionEvent.ACTION_DOWN:
//			        initialX = params.x;
//			        initialY = params.y;
//			        initialTouchX = event.getRawX();
//			        initialTouchY = event.getRawY();
//			        return true;
//			      case MotionEvent.ACTION_UP:
//			        return true;
//			      case MotionEvent.ACTION_MOVE:
//			        params.x = initialX + (int) (event.getRawX() - initialTouchX);
//			        params.y = initialY + (int) (event.getRawY() - initialTouchY);
//			        windowManager.updateViewLayout(chatHead, params);
//			        return true;
//			    }
//			    return false;
//			  }
//			});
	  
		 
	  return START_STICKY;
	  }

	  @Override
	  public void onDestroy() {
	    super.onDestroy();
	    if (chatHead != null) windowManager.removeView(chatHead);
	  }
	}
