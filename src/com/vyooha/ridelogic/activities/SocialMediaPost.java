package com.vyooha.ridelogic.activities;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class SocialMediaPost  extends Activity{
	EditText post_text;
	ImageView post_image,postedimage;
	Button upload_post,cancel;
	boolean photostatus=false;
	
	RelativeLayout layout_text,layout_image,layout_loc;
	ToggleButton toggle_img,toggle_txt,toggle_loc;
	RadioGroup RdGroup;
	
	 byte[] array;
	final int SELECT_PHOTO=1;
	
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("uploading post. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}

	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.socialmedia_post);
	        
	        post_text=(EditText)findViewById(R.id.post_text);
	        post_image=(ImageView)findViewById(R.id.post_image);
	        postedimage=(ImageView)findViewById(R.id.uploadedphoto);
	        upload_post=(Button)findViewById(R.id.upload_post);
	        cancel=(Button)findViewById(R.id.cancel_post);
	        
	        toggle_img=(ToggleButton)findViewById(R.id.togglePhoto);
	        toggle_loc=(ToggleButton)findViewById(R.id.toggleLocationPost);
	        toggle_txt=(ToggleButton)findViewById(R.id.toggleText);
	        
	        layout_image=(RelativeLayout)findViewById(R.id.addPhotolayout);
	        layout_loc=(RelativeLayout)findViewById(R.id.addLocationlayout);
	        layout_text=(RelativeLayout)findViewById(R.id.addTextlayout);
	        
	        
	      RdGroup=(RadioGroup) findViewById(R.id.radiogroupbuttons);
	        
	      
	       
	        final RadioGroup.OnCheckedChangeListener ToggleListener = new RadioGroup.OnCheckedChangeListener() {
	            @Override
	            public void onCheckedChanged(final RadioGroup radioGroup, final int i) {
	                for (int j = 0; j < radioGroup.getChildCount(); j++) {
	                    final ToggleButton view = (ToggleButton) radioGroup.getChildAt(j);
	                    view.setChecked(view.getId() == i);
	                }
	            }
	        };
	        
	        
	        RdGroup .setOnCheckedChangeListener(ToggleListener);    
	        
	        toggle_txt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(toggle_txt.isChecked()){
						layout_text.setVisibility(View.VISIBLE);
						layout_loc.setVisibility(View.GONE);
						layout_image.setVisibility(View.GONE);
						toggle_img.setChecked(false);
						toggle_loc.setChecked(false);
					}
					
				}
			});
	        toggle_img.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(toggle_img.isChecked()){
						layout_text.setVisibility(View.GONE);
						layout_loc.setVisibility(View.GONE);
						layout_image.setVisibility(View.VISIBLE);
						toggle_txt.setChecked(false);
						toggle_loc.setChecked(false);
					}
				}
			});
	        toggle_loc.setOnClickListener(new OnClickListener() {
	
	        	@Override
	        	public void onClick(View v) {
	        		// TODO Auto-generated method stub
	        		if(toggle_loc.isChecked()){
	        			layout_text.setVisibility(View.GONE);
						layout_loc.setVisibility(View.VISIBLE);
						layout_image.setVisibility(View.GONE);
						toggle_img.setChecked(false);
						toggle_txt.setChecked(false);
	        		}
	        		}
	        	});
	        
	        post_image.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
					photoPickerIntent.setType("image/*");
					startActivityForResult(photoPickerIntent, SELECT_PHOTO);
				}
			});
	        
	        cancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
	        
	        upload_post.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startLoading();
					if(photostatus)
					{
						final Map<String, Object> input_param = new HashMap<String, Object>();
						input_param.put("post_type", "photo");
						
						input_param.put("post_text", post_text.getText().toString());
						
						
						final ParseFile pf=new ParseFile("image.png",array);
						 pf.saveInBackground(new SaveCallback(){

					 			@Override
					 			public void done(ParseException e) {
					 				if(e == null){
					 					
					 					input_param.put("main_photo", pf);
					 					ParseCloud.callFunctionInBackground("createPost", input_param, new FunctionCallback<String>() {
											

											

											@Override
											public void done(
													 String result,
													ParseException e) {
												// TODO Auto-generated method stub
												if(e==null){
													stopLoading();
													Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
												}
											}
											});
					 					
					 					
					 					
					 				}}});
					
					
					
					}
					else{
						
					
						
						final Map<String, Object> input_param = new HashMap<String, Object>();
						input_param.put("post_type", "text");
						
						input_param.put("post_text", post_text.getText().toString());
						
						
					 					ParseCloud.callFunctionInBackground("createPost", input_param, new FunctionCallback<String>() {
											

											

											@Override
											public void done(
													 String result,
													ParseException e) {
												// TODO Auto-generated method stub
												if(e==null){
													stopLoading();
													Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
												}
											}
											});
					 					
					 					
					 					
					 				
					
					}
					
				}
			});
	    
	 }
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
	        super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 

	        switch(requestCode) { 
	        case SELECT_PHOTO:
	            if(resultCode == RESULT_OK){
					try {
						 final Uri imageUri = imageReturnedIntent.getData();
							
							final InputStream imageStream = getContentResolver().openInputStream(imageUri);
							Log.e("image",imageStream+"");
							final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
							Bitmap selectedimage = Bitmap.createScaledBitmap(selectedImage, 256, 256
									* selectedImage.getHeight() / selectedImage.getWidth(), false);
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							selectedimage.compress(Bitmap.CompressFormat.PNG, 100, stream);
							//byte[] byteArray 
							array= stream.toByteArray();
							// array = buffer.array();
							postedimage.setImageBitmap(selectedImage);
							post_image.setVisibility(View.GONE);
							photostatus=true;
	
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

	            }
	       
	        }
	    }
	 
}
