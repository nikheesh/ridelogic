package com.vyooha.ridelogic.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class DispatchActivity extends Activity{
	private ParseUser currentUser;
	//  protected ProgressDialog proDialog;
	  private ConnectivityManager conmgr;
	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
	    
	    // Check if there is current user info
	    dispatchactivity();
	   
	   
       }
	  
	  public void dispatchactivity(){
		  conmgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
	       if(checkInternet()){
		    currentUser = ParseUser.getCurrentUser();
		    if (currentUser != null) {
		    	Intent intent=new Intent(this, MainMenuActivity.class);
		    	 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		    	startActivity(intent);
		    } else {
		    	Log.e("rendering","not logged in so to welcome page");
		      // Start and intent for the logged out activity
		    	Intent intent=new Intent(this, FirstpageActivity.class);
		    	 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		    	startActivity(intent);
		     
		    }
		    
	       }
	       else
	       { 
	    	   setContentView(R.layout.interneterror);
	    	   TextView nonet=(TextView)findViewById(R.id.no_network);
	    	   Button ref=(Button)findViewById(R.id.refresh);
	    	   RelativeLayout rel=(RelativeLayout)findViewById(R.id.relative_no_network);
	    	  // rel.setBackgroundColor(Color.WHITE);
	    	  // nonet.setTextColor(Color.rgb(232, 44, 12));
	    	  // Button ref=(Button)findViewById(R.id.refresh);
	   	    ref.setOnClickListener(new View.OnClickListener() {
	   		      public void onClick(View view) {
	   		    	 dispatchactivity();   
	   			      }
	   			    });
	    	   
	    	   
	       }
	  }
	  
	  public boolean checkInternet(){
			NetworkInfo ni = conmgr.getActiveNetworkInfo();
			if (ni == null) {
			    // There are no active networks.
			    return false;
			}else{
				boolean isConnected = ni.isConnected();
				return isConnected;
			}
	   }
	  
	
	  
//	  @Override
//		 public void onBackPressed() {
//			 AlertDialog.Builder adb = new AlertDialog.Builder(this);
//
//
//			   // adb.setView( );
//
//
//			    adb.setTitle("Do you want to Exit "+R.string.app_name+" ?");
//			   
//
//
//			    adb.setIcon(R.drawable.ic_launcher);
//
//
//			    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			        public void onClick(DialogInterface dialog, int which) {
//
//			        	finish();
//			            System.exit(1);
//			           
//			      } });
//
//
//			    adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//			        public void onClick(DialogInterface dialog, int which) {
//
//			            finish();
//			      } });
//			    adb.show();
//
//		    
//		 }
		 
		  
}
