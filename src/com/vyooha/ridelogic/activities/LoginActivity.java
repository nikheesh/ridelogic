package com.vyooha.ridelogic.activities;



import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity
{
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Trying to Login. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				        WindowManager.LayoutParams.FLAG_FULLSCREEN);
				     setContentView(R.layout.login);
				     
				     ImageView login=(ImageView)findViewById(R.id.login_botton);
				  final   EditText usernameEditText=(EditText)findViewById(R.id.email_login);
				  final   EditText  passwordEditText=(EditText)findViewById(R.id.password_login);
				  
				  
				  ImageView back=(ImageView)findViewById(R.id.left_arrow);
				  
				  
				     login.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								startLoading();
								
								 String username = usernameEditText.getText().toString();
								    String password = passwordEditText.getText().toString();

								 ParseUser.logInInBackground(username, password, new LogInCallback() {
								      @Override
								      public void done(ParseUser user, ParseException e) {
								      
								        if (e != null) {
								          stopLoading();
								          Toast.makeText(getApplicationContext(), "username or password incorrect..!", Toast.LENGTH_LONG).show();
//								        
								        } else {
								        	stopLoading();
								          // Start an intent for the dispatch activity
								        	   ParseApplication appState = ((ParseApplication)getApplicationContext());
								      	     appState.user=user;
								          Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
								          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
								          startActivity(intent);
								        }
								      }
								    });
				  				//stopLoading();
								
							}
				        });
				     
				     back.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								
								finish();
								
							}
							});
				     
				     
				     
	 }
				     
}
