package com.vyooha.ridelogic.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FirstpageActivity extends Activity
{
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				        WindowManager.LayoutParams.FLAG_FULLSCREEN);
				     setContentView(R.layout.first_page);
				     
				     Button login=(Button)findViewById(R.id.login);
				     Button sign_up=(Button)findViewById(R.id.sign_up);
				     
				     login.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),LoginActivity.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
				     
				     sign_up.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),SignupActivity.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
				     
				     
				     
	 }
	     
//	 @Override
//	 public void onBackPressed() {
//		 AlertDialog.Builder adb = new AlertDialog.Builder(this);
//
//
//		   // adb.setView( );
//
//
//		    adb.setTitle("Do you want to Exit "+R.string.app_name+" ?");
//		   
//
//
//		    adb.setIcon(R.drawable.ic_launcher);
//
//
//		    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//		        public void onClick(DialogInterface dialog, int which) {
//
//		        	finish();
//		            System.exit(1);
//		           
//		      } });
//
//
//		    adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//		        public void onClick(DialogInterface dialog, int which) {
//
//		            finish();
//		      } });
//		    adb.show();
//
//	    
//	 }
	 
	   
}
