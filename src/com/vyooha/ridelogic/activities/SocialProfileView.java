package com.vyooha.ridelogic.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.vyooha.ridelogic.views.RoundedImageView;

public class SocialProfileView  extends Activity{
	
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Retrieving Profile. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}

	
	 ArrayAdapter<String> socialAdapter;
	 RoundedImageView profpic;
	 ImageView coverphoto;
	 Button isFriend_btn;
	 TextView name,location,noof_friend,total_dist,trip_taken;
	 GridView list;
	 
	 ParseUser user;
	 RoundedImageView prof_pic;
	 ImageView upload_post;
	   ImageButton search_edit;
	boolean isfriend=false;
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     user=ParseUser.getCurrentUser();
		 
	     final DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
  				.cacheOnDisc(true).resetViewBeforeLoading(true)
  				//.showImageForEmptyUri(fallback)
  				//.showImageOnFail(fallback)
  			//	.showImageOnLoading(fallback)
  				.build();
   
  socialAdapter = new ArrayAdapter<String>(this, 0) {
         @Override
         public View getView(int position, View convertView, ViewGroup parent) {
             if (convertView == null)
                 convertView = getLayoutInflater().inflate(R.layout.gridphoto_item, null);
             
            
             String url=this.getItem(position);
             

//ViewPager  viewpager=(ViewPager)findViewById(R.id.image_pager_social);
             ImageLoader imageLoader = ImageLoader.getInstance();
ImageView feedimage=(ImageView)convertView.findViewById(R.id.grid_img);
          	imageLoader.displayImage(url,feedimage,options);
             	
             
            
             
            
             
             return convertView;
         }
     };
	     
	     	requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.socialprofileview);
	        
	        list=(GridView)findViewById(R.id.gridviewphoto);
	        list.setAdapter(socialAdapter);
					startLoading();
					
					profpic=(RoundedImageView)findViewById(R.id.singleprof_prof_pic);
					name=(TextView)findViewById(R.id.singleprof_profname);
					location=(TextView)findViewById(R.id.singleprof_location);
					noof_friend=(TextView)findViewById(R.id.singleprof_noof_friends);
					trip_taken=(TextView)findViewById(R.id.singleprof_tripstaken);
					total_dist=(TextView)findViewById(R.id.singleprof_distance);
					
					isFriend_btn=(Button)findViewById(R.id.singleprof_addfriend_btn);
					
					
					 prof_pic =(RoundedImageView)findViewById(R.id.social_prof_img);
				        ParseFile pf=user.getParseFile("profile_pic");
				        byte[] bitmapdata;
				        Bitmap bitmap = null;
						try {
							bitmapdata = pf.getData();
							 bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
						}
						catch (Exception e){
							Log.e("", e.toString());
						}
						if(bitmap!=null)
						{
							prof_pic.setImageBitmap(bitmap);
						}
						
						 prof_pic.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									 PopupMenu popup = new PopupMenu(getApplicationContext(), prof_pic);
						                //Inflating the Popup using xml file
						                popup.getMenuInflater()
						                    .inflate(R.menu.profilemenu, popup.getMenu());
						                
						                //registering popup with OnMenuItemClickListener
						                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
						                    public boolean onMenuItemClick(MenuItem item) {
						                    	int id=item.getItemId();
						                     switch (id) {
											case R.id.one:
												Intent i=new Intent(getApplicationContext(), SocialProfileView.class);
												i.putExtra("userid", user.getObjectId());
												startActivity(i);
												break;
											case R.id.three:
												Intent i1=new Intent(getApplicationContext(), MainMenuActivity.class);
												
												startActivity(i1);
												break;
											case R.id.four:
												ParseUser.logOut();				
												  startActivity(new Intent(getApplicationContext(), FirstpageActivity.class));
												break;
											default:
												break;
											}
						                    	   
						                       
						                    	
						                    	
						                        return true;
						                    }
						                });

						                popup.show(); //showing popup menu
								}
							});
					
					 upload_post=(ImageView)findViewById(R.id.open_upload_post);
						upload_post.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								Intent i=new Intent(getApplicationContext(), SocialMediaPost.class);
								startActivity(i);
								
							}
						});
						
						search_edit=(ImageButton)findViewById(R.id.searchpeople_main);
						search_edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View arg0) {
								Intent i=new Intent(getApplicationContext(), Social_SearchPeople.class);
								startActivity(i);
							}
						});
						
					final	String userid=getIntent().getStringExtra("userid");
						
						isFriend_btn.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if(!isfriend){
								 Map<String, Object> input_param = new HashMap<String, Object>();
									input_param.put("target_userId", userid);
							
									
								 					ParseCloud.callFunctionInBackground("sendFriendReq", input_param, new FunctionCallback<String>() {
														

														

														@Override
														public void done(
																 String result,
																ParseException e) {
															// TODO Auto-generated method stub
															if(e==null){
																
																Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
																
															}
														}
														});
								}
								 					
							}
						});
				        
					
					
					 
				       // String ownerid=getIntent().getStringExtra("ownerid");
				        
				       
						final Map<String, Object> input_param = new HashMap<String, Object>();
						//input_param.put("post_owner_userId", ownerid);
						
						input_param.put("target_userId", userid);
						
					//	input_param.put("comment_text", post_text.getText().toString());
						
					 					ParseCloud.callFunctionInBackground("viewProfile", input_param, new FunctionCallback<Map<String,Object>>() {
											

											

											@Override
											public void done(
													Map<String,Object> result,
													ParseException e) {
												// TODO Auto-generated method stub
												if(e==null){
													stopLoading();
													//Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
													  
													 displayResult(result);
													 
												
												}
											}
											});
					 					
					 					
					 					
					 			
					
					
					
					
					
					
//				}
//			});
	    
	 }
	
	public void displayResult(Map<String,Object> result){
		@SuppressWarnings("unchecked")
		ArrayList<ParseObject> userall=(ArrayList<ParseObject>) result.get("profile");
		ParseObject userdet=userall.get(0); 
		ParseFile pfuser=userdet.getParseFile("profile_pic");
		ImageLoader imgldr=ImageLoader.getInstance();
		imgldr.displayImage(pfuser.getUrl(), profpic);
		name.setText(userdet.getString("name"));
		ParseGeoPoint pg=userdet.getParseGeoPoint("current_location");
		if(pg!=null)
			location.setText(CommonFunctions.getAddress(pg.getLatitude(),pg.getLongitude(),getApplicationContext()));
		//location.setText(userdet.get)
		noof_friend.setText(userdet.getNumber("total_friends").toString());
		total_dist.setText(userdet.getNumber("total_ptrip_distance").toString());
		trip_taken.setText(userdet.getNumber("total_ptrips").toString());
		
		Boolean isfri=(Boolean) result.get("isFriend");
		isfriend=isfri.booleanValue();
		if (isfriend==true) {
			isFriend_btn.setText("FRIEND");
			
		}
		else{
			isFriend_btn.setText("ADD AS FRIEND");
		}
		
		ArrayList<ParseObject> postedimages=(ArrayList<ParseObject>) result.get("photos");
		for(int i=0;i<postedimages.size();i++){
			ParseObject obj=postedimages.get(i);
			ParseFile pf=obj.getParseFile("main_photo");
			socialAdapter.add(pf.getUrl());
		}
		
		
	}

}

