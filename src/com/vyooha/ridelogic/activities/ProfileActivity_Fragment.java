package com.vyooha.ridelogic.activities;

import com.parse.ParseUser;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity_Fragment extends Fragment{
	
	
	  @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	       View contacts=inflater.inflate(R.layout.profileactivity, container, false); 
	       return contacts;
	    }  
	       
	       
	    @Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
            getActivity().requestWindowFeature(Window.FEATURE_NO_TITLE);
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
          //  getActivity().setContentView(R.layout.profileactivity);
	        TextView prof_name=(TextView)getActivity().findViewById(R.id.person_name);
	        TextView viewprof=(TextView)getActivity().findViewById(R.id.viewprofile);
	        TextView setting=(TextView)getActivity().findViewById(R.id.viewprofilesettings);
	        ImageView opencompass=(ImageView)getActivity().findViewById(R.id.openmain_btn);
	        ParseUser user=ParseUser.getCurrentUser();
	        prof_name.setText(user.getString("name"));
//	        intent=new Intent(getApplicationContext(), SpeedWidgetService.class);
//	        startService(intent);
	       

	      
	        
	        
	        viewprof.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getActivity().getApplicationContext(),ProfileDetails.class);
 					i.putExtra("choose", false);
 				startActivity(i);
				}
			});
	        
 setting.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getActivity().getApplicationContext(),SettingsActivity.class);
// 					i.putExtra("choose", false);
 				startActivity(i);
				}
			});
	       
 opencompass.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
//		stopService(intent);
		 Intent nextActivity = new Intent(getActivity().getApplicationContext(),CompassActivity.class);
		  startActivity(nextActivity);
		  //push from bottom to top
		  getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
});
	}


}
