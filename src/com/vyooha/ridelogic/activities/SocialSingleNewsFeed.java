package com.vyooha.ridelogic.activities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.vyooha.ridelogic.views.RoundedImageView;

public class SocialSingleNewsFeed  extends Activity{
	
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Retreiving Postdetails. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}

	
	 ArrayAdapter<SocialComment_Class> socialAdapter;
	 ImageView postimage;
	 ToggleButton likeimage;
	 ImageView coverphoto;
	 Button sendcomment;
	 TextView timestamp,ownedby,noof_likes,noof_comments,post_text;
	 EditText editcomment;
	 ListView list;
	 
	 ParseUser user;
	 RoundedImageView prof_pic;
	 ImageView upload_post;
	   ImageButton search_edit;
	
	 SocialComment_Class sc;
	 String postid,ownerid;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     user=ParseUser.getCurrentUser();
		 
	     final DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
  				.cacheOnDisc(true).resetViewBeforeLoading(true)
  				//.showImageForEmptyUri(fallback)
  				//.showImageOnFail(fallback)
  			//	.showImageOnLoading(fallback)
  				.build();
   
	     socialAdapter = new ArrayAdapter<SocialComment_Class>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.socialcomment_item, null);
	                
	               sc=new SocialComment_Class();
	                sc=this.getItem(position);
	                
RoundedImageView friend_img=(RoundedImageView)convertView.findViewById(R.id.singlenews_people_img);
TextView prof_name=(TextView)convertView.findViewById(R.id.singlenews_peoplename);
TextView post_text=(TextView)convertView.findViewById(R.id.singlenews_peoplecomment);
TextView time_stamp=(TextView)convertView.findViewById(R.id.singlenews_peopletimestamp);

//ViewPager  viewpager=(ViewPager)findViewById(R.id.image_pager_social);

	   RelativeLayout feedimage_layout=(RelativeLayout)convertView.findViewById(R.id.image_social_layout);
	    ImageLoader imageLoader = ImageLoader.getInstance();
	  
	                if(sc.name!=null){
	                	Log.e("sc .name", sc.name);
	                	
	                	prof_name.setText(sc.name);
	                	prof_name.setVisibility(View.VISIBLE);
	                	
	                }
	                else{
	                	prof_name.setVisibility(View.GONE);
	                }
	                if(sc.postcomment!=null){
	                	post_text.setText(sc.postcomment);
	                	post_text.setVisibility(View.VISIBLE);
	                }
	                else
	                {
	                	post_text.setVisibility(View.GONE);
	                }
	                
	                if(sc.timestamp!=0){
	                	time_stamp.setText(Long.toString(sc.timestamp));
	                	time_stamp.setVisibility(View.VISIBLE);
	                	
	                }
	                else{
	                	time_stamp.setVisibility(View.GONE);
	                }
	                
	                if(sc.profpic!=null){
	                	
	                	friend_img.setVisibility(View.VISIBLE);
	                	imageLoader.displayImage(sc.profpic,friend_img);
	                }
	                else{
	                	friend_img.setVisibility(View.GONE);
	                }
	                
	               
	                
	               
	                
	                
	                return convertView;
	            }
	        };
	     	requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.socialsinglenewsfeed);
	        
	        list=(ListView)findViewById(R.id.singlenews_comment_listview);
	        list.setAdapter(socialAdapter);
					startLoading();
					
					
					
					postimage=(ImageView)findViewById(R.id.singlenews_post_img);
					post_text=(TextView)findViewById(R.id.singlenews_post_text);
					likeimage=(ToggleButton)findViewById(R.id.singlenews_like_img);
					timestamp=(TextView)findViewById(R.id.singlenews_timestamp);
					ownedby=(TextView)findViewById(R.id.singlenews_ownedby);
					noof_likes=(TextView)findViewById(R.id.singlenews_noof_likes);
					noof_comments=(TextView)findViewById(R.id.singlenews_noof_comments);
					editcomment=(EditText)findViewById(R.id.singlenews_addcomment);
					
					sendcomment=(Button)findViewById(R.id.singlenews_sendcomment);
					 postid=getIntent().getStringExtra("postid");
			         ownerid=getIntent().getStringExtra("ownerid");
			         
			         prof_pic =(RoundedImageView)findViewById(R.id.social_prof_img);
				        ParseFile pf=user.getParseFile("profile_pic");
				        byte[] bitmapdata;
				        Bitmap bitmap = null;
						try {
							bitmapdata = pf.getData();
							 bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
						}
						catch (Exception e){
							Log.e("", e.toString());
						}
						if(bitmap!=null)
						{
							prof_pic.setImageBitmap(bitmap);
						}
						
						 prof_pic.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									 PopupMenu popup = new PopupMenu(getApplicationContext(), prof_pic);
						                //Inflating the Popup using xml file
						                popup.getMenuInflater()
						                    .inflate(R.menu.profilemenu, popup.getMenu());
						                
						                //registering popup with OnMenuItemClickListener
						                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
						                    public boolean onMenuItemClick(MenuItem item) {
						                    	int id=item.getItemId();
						                     switch (id) {
											case R.id.one:
												Intent i=new Intent(getApplicationContext(), SocialProfileView.class);
												i.putExtra("userid", user.getObjectId());
												startActivity(i);
												break;
											case R.id.three:
												Intent i1=new Intent(getApplicationContext(), MainMenuActivity.class);
												
												startActivity(i1);
												break;
											case R.id.four:
												ParseUser.logOut();				
												  startActivity(new Intent(getApplicationContext(), FirstpageActivity.class));
												break;
											default:
												break;
											}
						                    	   
						                       
						                    	
						                    	
						                        return true;
						                    }
						                });

						                popup.show(); //showing popup menu
								}
							});
			        
			         upload_post=(ImageView)findViewById(R.id.open_upload_post);
						upload_post.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								Intent i=new Intent(getApplicationContext(), SocialMediaPost.class);
								startActivity(i);
								
							}
						});
						
						search_edit=(ImageButton)findViewById(R.id.searchpeople_main);
						search_edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View arg0) {
								Intent i=new Intent(getApplicationContext(), Social_SearchPeople.class);
								startActivity(i);
							}
						});
						
						
						
				        likeimage.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								
								
								
								 Map<String, Object> input_param = new HashMap<String, Object>();
								
								input_param.put("original_postId", postid);
								
								
							 	ParseCloud.callFunctionInBackground("likeORunlikeAPost", input_param, new FunctionCallback<Map<String,Object>>() {
													

													

													@Override
													public void done(
															Map<String,Object> result,
															ParseException e) {
														// TODO Auto-generated method stub
														if(e==null){
															stopLoading();
															int actioncode=(Integer) result.get("action_code");
															if(actioncode==1){
																likeimage.setChecked(false);
															}
															else {
																likeimage.setChecked(true);
															}
															
														}
													}
													});
							 					
								
							}
						});
			     
					sendcomment.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							startLoading();
							   
								 Map<String, Object> input_param = new HashMap<String, Object>();
								input_param.put("post_owner_userId", ownerid);
								
								input_param.put("original_postId", postid);
								
								input_param.put("comment_text", editcomment.getText().toString());
								
							 					ParseCloud.callFunctionInBackground("commentOnPost", input_param, new FunctionCallback<String>() {
													

													

													@Override
													public void done(
															 String result,
															ParseException e) {
														// TODO Auto-generated method stub
														if(e==null){
															stopLoading();
															Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
														}
													}
													});
							 					
							 					
							 					
							 			
							
							
							
							
							
							
						}
					});
					
					
					// String userid=getIntent().getStringExtra("postid");
				       // String ownerid=getIntent().getStringExtra("ownerid");
				        
				       
						final Map<String, Object> input_param = new HashMap<String, Object>();
						//input_param.put("post_owner_userId", ownerid);
						
						input_param.put("post_id", postid);
						
					//	input_param.put("comment_text", post_text.getText().toString());
						
					 					ParseCloud.callFunctionInBackground("showPostDetail", input_param, new FunctionCallback<Map<String,Object>>() {
											

											

											@Override
											public void done(
													Map<String,Object> result,
													ParseException e) {
												// TODO Auto-generated method stub
												if(e==null){
													
													
													
													//Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
													  
													 displayResult(result);
													 stopLoading();
												
												}
											}
											});
					 					
					 					
					 					
					 			
					
					
					
					
					
					
//				}
//			});
	    
	 }
	
	public void displayResult(Map<String,Object> result){
		
		
		ParseObject userdet=(ParseObject) result.get("postdetail"); 
		if(userdet.getString("post_type").equals("photo")){
			postimage.setVisibility(View.VISIBLE);
			post_text.setVisibility(View.GONE);
			ParseFile pfuser=userdet.getParseFile("main_photo");
			ImageLoader imgldr=ImageLoader.getInstance();
			imgldr.displayImage(pfuser.getUrl(), postimage);
			
		}
		else{
			postimage.setVisibility(View.GONE);
			post_text.setVisibility(View.VISIBLE);
			post_text.setText(userdet.getString("post_text"));
			
		}
		
		
		Boolean isliked=(Boolean) result.get("isLiked");
		if(isliked.booleanValue()){
			likeimage.setChecked(false);
		}
		else
		{
		likeimage.setChecked(true);	
		}
		
		
		Date d=userdet.getCreatedAt();
		
		timestamp.setText(CommonFunctions.getTimeStamp(d));
		ParseObject postedby=userdet.getParseObject("post_by");
		ownedby.setText(postedby.getString("name"));
		
		// TextView timestamp,ownedby,noof_likes,noof_comments,editcomment;
		
		
		//location.setText(userdet.get)
		noof_comments.setText(userdet.getNumber("commentCount").toString()+" Comments");
		noof_likes.setText(userdet.getNumber("LIKECount").toString()+" Likes");
		
		
		ArrayList<ParseObject> postedcomment=(ArrayList<ParseObject>) result.get("comments");
		for(int i=0;i<postedcomment.size();i++){
			 sc=new SocialComment_Class();
			ParseObject obj=postedcomment.get(i);
			ParseObject ownerobj=obj.getParseObject("comment_by");
			ParseFile pf=ownerobj.getParseFile("profile_pic");
			sc.profpic=pf.getUrl();
			sc.timestamp=1;
			sc.name=ownerobj.getString("name");
			sc.postcomment=obj.getString("comment_text");
			
		}
		
		
	}

}

