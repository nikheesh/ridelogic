package com.vyooha.ridelogic.activities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.vyooha.ridelogic.views.RoundedImageView;

public class Social_SearchPeople extends Activity{
	 ArrayAdapter<Social_people> socialAdapter;
	 ListView list;
   ParseUser user;
   
   Social_people socialclass;
 
   EditText searchpeople_edit;
   Button search_btn;
   
   Social_people sc;
   
   protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Loading. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
   
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     user=ParseUser.getCurrentUser();
	 
	     final DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
	     				.cacheOnDisc(true).resetViewBeforeLoading(true)
	     				//.showImageForEmptyUri(fallback)
	     				//.showImageOnFail(fallback)
	     			//	.showImageOnLoading(fallback)
	     				.build();
	      
	     socialAdapter = new ArrayAdapter<Social_people>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.social_searchpeople_item, null);
	                
	               sc=new  Social_people();
	                sc=this.getItem(position);
	                
	                RoundedImageView friend_img=(RoundedImageView)convertView.findViewById(R.id.people_img);
	                TextView prof_name=(TextView)convertView.findViewById(R.id.people_name);
	               RelativeLayout layout=(RelativeLayout)convertView.findViewById(R.id.people_layout);


	//  ImageView feedimage=(ImageView)convertView.findViewById(R.id.image_social);
	//  RelativeLayout feedimage_layout=(RelativeLayout)convertView.findViewById(R.id.image_social_layout);
	    ImageLoader imageLoader = ImageLoader.getInstance();
	  
	                if(sc.name!=null){
	                	Log.e("sc .name", sc.name);
	                	
	                	prof_name.setText(sc.name);
	                	prof_name.setVisibility(View.VISIBLE);
	                }
	               
	                if(sc.prof_img!=null){
	                	
	                	friend_img.setVisibility(View.VISIBLE);
	                	imageLoader.displayImage(sc.prof_img,friend_img);
	                }
	               
	                layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i=new Intent(getApplicationContext(),SocialProfileView.class);
							i.putExtra("userid", sc.id);
							startActivity(i);
						}
					});
	               
	                
	                return convertView;
	            }
	        };
	     
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.social_searchpeople);
	        list=(ListView)findViewById(R.id.listiew_searchpeople);
	        list.setAdapter(socialAdapter);
	      
	      
	        
	       searchpeople_edit=(EditText)findViewById(R.id.searchpeople_main);
	        search_btn=(Button)findViewById(R.id.searchpeople_btn);
			search_btn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					startLoading();
					HashMap<String, String> inputparm=new HashMap<String, String>();
					inputparm.put("email", searchpeople_edit.getText().toString());

					ParseCloud.callFunctionInBackground("searchPeople", inputparm, new FunctionCallback<List<ParseObject>>() {
			

							@Override
							public void done(List<ParseObject> result, ParseException e) {
								// TODO Auto-generated method stub
								if (e == null) {
									stopLoading();
									Log.e("result size=", ""+result.size());
									for(int i=0;i<result.size();i++){
										ParseFile pf=result.get(i).getParseFile("profile_pic");
										String name=result.get(i).getString("name");
										
									 socialclass=new Social_people();
									 socialclass.name=name;
									 socialclass.prof_img=pf.getUrl();
									 socialclass.id=result.get(i).getObjectId();
									 socialAdapter.add(socialclass);
									}
								}
								else{
									Log.e("error loading [people]", "error loading");
								}
							}});
					
				}
			});
			
	        
	       
	      
	       
	 }
	             

}
