package com.vyooha.ridelogic.activities;

import java.io.InputStream;

import com.vyooha.ridelogic.views.RoundedImageView;

import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



public class ContactActivity  extends Fragment {
    // adapter that holds tweets, obviously :)
    ArrayAdapter<ContactClass> contactAdapter;
//    private JSONObject json;
//    private JSONArray jsonarray;
    ListView list;
    EditText search;
    ContactClass contclass;
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
       View contacts=inflater.inflate(R.layout.contact_list, container, false); 
       return contacts;
    }  
       
       
    @Override 
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);  
    
   
         contactAdapter = new ArrayAdapter<ContactClass>(getActivity(), 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.contact_item, null);

                   final   ContactClass cont = this.getItem(position);
                      try{
             
                RoundedImageView imageView = (RoundedImageView)convertView.findViewById(R.id.cont_img);
              
                

                  TextView name = (TextView)convertView.findViewById(R.id.cont_name);
                  name.setText(cont.name);
                  
                  RelativeLayout layout=(RelativeLayout)convertView.findViewById(R.id.layoutcontact);
                  
                  layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						//	String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
//							int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));
//							//
////											if (hasPhoneNumber > 0) {
//							//
							ContentResolver contentResolver = getActivity().getContentResolver();
							
							Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
							String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
							String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
												Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { cont.contact_id }, null);
							String phoneNumber="";
												while (phoneCursor.moveToNext()) {
													phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
													
//												
							
								}
							
												phoneCursor.close();
												
												if(phoneNumber!=""){
													Log.i("Make call", "");

												      Intent phoneIntent = new Intent(Intent.ACTION_CALL);
												      phoneIntent.setData(Uri.parse("tel:"+phoneNumber));

												      try {
												         startActivity(phoneIntent);
												         getActivity().finish();
												         Log.i("Finished making a call...", "");
												      } catch (android.content.ActivityNotFoundException ex) {
												         Toast.makeText(getActivity().getApplicationContext(), 
												         "Call faild, please try again later.", Toast.LENGTH_SHORT).show();
												      }
													
												}
												
						}
					});

//                TextView phone = (TextView)convertView.findViewById(R.id.cont_number);
//                phone.setText(cont.phone);
                
                if(cont.photo!=null){
                	imageView.setImageBitmap(cont.photo);
                	Log.e("imagesetted", "hi");
                }
                
                
                      }
                      catch (Exception e){}
                      
            //    text.setText(tweet.get("mdesc").getAsString());
                return convertView;
            }
        };
    //	requestWindowFeature(Window.FEATURE_NO_TITLE);
//setContentView(R.layout.contact_list);

ImageView close1=(ImageView)getActivity().findViewById(R.id.close_img);
close1.setOnClickListener(new OnClickListener() {
    

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		getActivity().finish();
		
	}
});

search=(EditText)getActivity().findViewById(R.id.searchedit);



 list=(ListView)getActivity().findViewById(R.id.listofcontacts);
list.setAdapter(contactAdapter);
fetchContacts();
search.addTextChangedListener(new TextWatcher() {

    public void afterTextChanged(Editable s) {

      // you can call or do what you want with your EditText here
    

    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    	
    	fetchContacts(search.getText().toString());
    }
 });

    }
    
    
    public void fetchContacts() {
    	contactAdapter.clear();
		String phoneNumber = null;


		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		


		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null,null, null, null);	

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {
				 contclass=new ContactClass();
				String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
				String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
				//Log.e("name=====", name);
			//	String phone="";
				InputStream image;
				contclass.name=name;
				contclass.contact_id=contact_id;
//				contclass.photo=photo_image;
				contactAdapter.add(contclass);
			
			}
			
			


		}
	}

    public void fetchContacts(String namelike) {
    	
		String phoneNumber = null;
		contactAdapter.clear();

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		
		final String sa1 = namelike+"%";
		 final String SELECTION =
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
	            Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?" :
	            Contacts.DISPLAY_NAME + " LIKE ?";


		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null,SELECTION, new String[] { sa1 }, null);	

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {
				 contclass=new ContactClass();
				String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
				String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
				Log.e("name=====", name);
			//	String phone="";
				InputStream image;
				contclass.name=name;
				contclass.contact_id=contact_id;
//				contclass.photo=photo_image;
				contactAdapter.add(contclass);
			
			}
			
			


		}
	}


}
