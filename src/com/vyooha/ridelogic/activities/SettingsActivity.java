package com.vyooha.ridelogic.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

public class SettingsActivity extends Activity{
	private final int REQUEST_ENABLE_BT = 1;
	BluetoothAdapter mBluetoothAdapter;
	
	private AudioManager audioManager = null;
	
	boolean hasBluetooth;
	  ToggleButton airplane_btn;
	   ToggleButton location_btn;
	    ToggleButton bluetooth_btn;
	@Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 
		 setVolumeControlStream(AudioManager.STREAM_MUSIC);
		 
		 
		 setContentView(R.layout.settings);
		 
		 
		 ImageView close=(ImageView)findViewById(R.id.close_img);
			  SeekBar seekbright=(SeekBar)findViewById(R.id.seekBarbrightness);
			  seekbright.setMax(255);
			  final SeekBar seekvolume=(SeekBar)findViewById(R.id.seekBarvolume);
		 
		 
		 bluetooth_btn=(ToggleButton)findViewById(R.id.togglebluetooth);
		   airplane_btn=(ToggleButton)findViewById(R.id.toggleairplane);
		 final ToggleButton orientation_btn=(ToggleButton)findViewById(R.id.togglescreenrotation);
		  location_btn=(ToggleButton)findViewById(R.id.togglelocation);
		 
		 mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		  if(mBluetoothAdapter != null)
		  {
		  hasBluetooth=true;
		  if(mBluetoothAdapter.isEnabled()){
			  bluetooth_btn.setChecked(true);
		  }
		  else {
			  bluetooth_btn.setChecked(false);
		}
		  }
		  else
			  hasBluetooth=false;
		  
		 
		//  boolean isAirplane;
		  
		//  if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
//				isAirplane = Settings.System.getInt(this.getContentResolver(),
//						Settings.System.AIRPLANE_MODE_ON, 0) == 1;
		//	 }
		//	 else {
		//		  isAirplane = Settings.System.getInt(this.getContentResolver(),
		//					Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
		//	}
		 if(isAirplaneModeOn(getApplicationContext()))
		 {
			 airplane_btn.setChecked(true);
		 }
		 else
		 {
			 airplane_btn.setChecked(false);
		 }
		 
		 
		 boolean isScreenRot;
		 
		 
		 
		 isScreenRot = Settings.System.getInt(this.getContentResolver(),
					Settings.System.ACCELEROMETER_ROTATION, 0) == 1;
	//	 }
	//	 else {
	//		  isAirplane = Settings.System.getInt(this.getContentResolver(),
	//					Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
	//	}
	 if(isScreenRot)
	 {
		 orientation_btn.setChecked(true);
	 }
	 else
	 {
		 orientation_btn.setChecked(false);
	 }
		 
	 
//	 
//	 Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//	     intent.putExtra("enabled", true);
//	     sendBroadcast(intent);
	
	// String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	 if(isLocationModeOn(getApplicationContext())){
		 location_btn.setChecked(true);
	 }
	 else {
		 location_btn.setChecked(false);
	}
		
		 
		 
		 bluetooth_btn.setOnClickListener(new OnClickListener()
		 {
			 

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 if (bluetooth_btn.isChecked())
				    {
				      if (hasBluetooth && !mBluetoothAdapter.isEnabled())
				      {
				        // prompt the user to turn BlueTooth on
				        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				      }
				    }
				    else
				    {
				      if (hasBluetooth && mBluetoothAdapter.isEnabled())
				      {
				        // you should really prompt the user for permission to turn
				        // the BlueTooth off as well, e.g., with a Dialog
				        boolean isDisabling = mBluetoothAdapter.disable();
				        if (!isDisabling)
				        {
				           // an immediate error occurred - perhaps the bluetooth is already off?
				        }
				      }
				    }
			}
			});
		 
		 
		 airplane_btn.setOnClickListener(new OnClickListener()
		 {
			 

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i = new	Intent(android.provider.Settings.ACTION_AIRPLANE_MODE_SETTINGS);
					startActivityForResult(i, 4);
//				if(airplane_btn.isChecked()){
//					modifyAirplanemode(true);
//				}
//				else
//					modifyAirplanemode(false);
			}
			});
		 
		 
		 
		 float curBrightnessValue = 0;
		  try {
		   curBrightnessValue = android.provider.Settings.System.getInt(
		     getContentResolver(),
		     android.provider.Settings.System.SCREEN_BRIGHTNESS);
		  } catch (SettingNotFoundException e) {
		   e.printStackTrace();
		  }

		   int screen_brightness = (int) curBrightnessValue;
		  seekbright.setProgress(screen_brightness);
		  seekbright.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
		   int progress = 0;

		    @Override
		   public void onProgressChanged(SeekBar seekBar, int progresValue,
		     boolean fromUser) {
		    progress = progresValue;
		   }

		    @Override
		   public void onStartTrackingTouch(SeekBar seekBar) {
		    // Do something here,
		    // if you want to do anything at the start of
		    // touching the seekbar
		   }

		    @Override
		   public void onStopTrackingTouch(SeekBar seekBar) {
		    android.provider.Settings.System.putInt(getContentResolver(),
		      android.provider.Settings.System.SCREEN_BRIGHTNESS,
		      progress);
		   }
		  });

		 
		 
		  audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
          seekvolume.setMax(audioManager
                  .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
          seekvolume.setProgress(audioManager
                  .getStreamVolume(AudioManager.STREAM_MUSIC));   


          seekvolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
          {
              @Override
              public void onStopTrackingTouch(SeekBar arg0) 
              {
            	  seekvolume.setProgress(audioManager
                          .getStreamVolume(AudioManager.STREAM_MUSIC));   


              }

              @Override
              public void onStartTrackingTouch(SeekBar arg0) 
              {
            	  seekvolume.setProgress(audioManager
                          .getStreamVolume(AudioManager.STREAM_MUSIC));   


              }

              @Override
              public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) 
              {
                  audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                          progress, 0);
              }
          });
		 
		 
          orientation_btn.setOnClickListener(new OnClickListener()
 		 {
 			 

 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				if(orientation_btn.isChecked()){
 					setAutoOrientationEnabled(true);
 				}
 				else
 					setAutoOrientationEnabled(false);
 			}
 			});
          
          
          
          location_btn.setOnClickListener(new OnClickListener()
  		 {
  			 

  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				
  				Intent i = new	Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
  						startActivity(i);
  				if(location_btn.isChecked()){
//  				 Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//  			     intent.putExtra("enabled", true);
//  			     sendBroadcast(intent);
//  				 final Intent poke = new Intent();
//  		        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
//  		        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
//  		        poke.setData(Uri.parse("3")); 
//  		        sendBroadcast(poke);
  				}
  				else
  				{
//  					 Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//  	  			     intent.putExtra("enabled", false);
//  	  			     sendBroadcast(intent);
//  	  				 final Intent poke = new Intent();
//  	  		        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
//  	  		        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
//  	  		        poke.setData(Uri.parse("3")); 
//  	  		        sendBroadcast(poke);
  				}
  			}
  			});
		 
		 
          
          close.setOnClickListener(new OnClickListener()
  		 {
  			 

  			@Override
  			public void onClick(View v) {
  				finish();
  			}
  			});
  			
		 
	 }
	 @Override
	 protected void onActivityResult (int requestCode, int resultCode, Intent data)
	 {
	   if ((requestCode == REQUEST_ENABLE_BT) && (resultCode == RESULT_OK))
	   {
	     boolean isEnabling = mBluetoothAdapter.enable();
	     if (!isEnabling)
	     {
	       // an immediate error occurred - perhaps the bluetooth is already on?
	     }
	     else if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON)
	     {
	       // the system, in the background, is trying to turn the Bluetooth on
	       // while your activity carries on going without waiting for it to finish;
	       // of course, you could listen for it to finish yourself - eg, using a
	       // ProgressDialog that checked mBluetoothAdapter.getState() every x
	       // milliseconds and reported when it became STATE_ON (or STATE_OFF, if the
	       // system failed to start the Bluetooth.)
	     }
	   }
	   
	   if((requestCode==4) && (resultCode==RESULT_OK)){
		   if(isAirplaneModeOn(getApplicationContext()))
			 {
				 airplane_btn.setChecked(true);
			 }
			 else
			 {
				 airplane_btn.setChecked(false);
			 }
		   
	   }
	   
	 }
	 
	 @Override
	 protected void onResume() {
	 	

	 	 super.onResume();
	 	 
	 	//  if((requestCode==4) && (resultCode==RESULT_OK)){
			   if(isAirplaneModeOn(getApplicationContext()))
				 {
					 airplane_btn.setChecked(true);
				 }
				 else
				 {
					 airplane_btn.setChecked(false);
				 }
			   if(isLocationModeOn(getApplicationContext())){
					 location_btn.setChecked(true);
				 }
				 else {
					 location_btn.setChecked(false);
				}
			   if(mBluetoothAdapter != null)
				  {
				  hasBluetooth=true;
				  if(mBluetoothAdapter.isEnabled()){
					  bluetooth_btn.setChecked(true);
				  }
				  else {
					  bluetooth_btn.setChecked(false);
				}
				  }
				  else
					  hasBluetooth=false;
	 	 
	 }
	 
//	 public void airPlanemodeON(View v) {
//			boolean isEnabled;
//		 if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
//			isEnabled = Settings.System.getInt(this.getContentResolver(),
//					Settings.System.AIRPLANE_MODE_ON, 0) == 1;
//		 }
//		 else {
//			  isEnabled = Settings.System.getInt(this.getContentResolver(),
//						Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
//		}
//			if (isEnabled == false) {
//				modifyAirplanemode(true);
//				Toast.makeText(getApplicationContext(), "Airplane Mode ON",
//						Toast.LENGTH_LONG).show();
//			}
//		}
//
//	 	public void airPlanemodeOFF(View v) {
//	 		boolean isEnabled;
//	 		
//	 		 if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
//			 isEnabled = Settings.System.getInt(this.getContentResolver(),
//					Settings.System.AIRPLANE_MODE_ON, 0) == 1;
//	 		 }
//	 		 else
//	 		 {
//	 			isEnabled = Settings.System.getInt(this.getContentResolver(),
//						Settings.Global.AIRPLANE_MODE_ON, 0) == 1; 
//	 		 }
//			if (isEnabled == true)// means this is the request to turn ON AIRPLANE mode
//			{
//				modifyAirplanemode(false);
//				Toast.makeText(getApplicationContext(), "Airplane Mode OFF",
//						Toast.LENGTH_LONG).show();
//			}
//		}

	 	
		public void modifyAirplanemode(boolean mode) {
	 	//	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
	 			Settings.System.putInt(getContentResolver(),
	 			Settings.System.AIRPLANE_MODE_ON, mode?1:0);
	 //			} else {
	// 			Settings.Global.putInt(getContentResolver(),
	// 			Settings.Global.AIRPLANE_MODE_ON, mode?1:0);
	 //			}
			Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);// creating intent and Specifying action for AIRPLANE mode.
			intent.putExtra("state", !mode);// indicate the "state" of airplane mode is changed to ON/OFF
			sendBroadcast(intent);// Broadcasting and Intent

	 	}
		
		private static boolean isAirplaneModeOn(Context context) {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
				 return Settings.System.getInt(context.getContentResolver(),
				           Settings.System.AIRPLANE_MODE_ON, 0) != 0;
				   
			}
			else
			{
				 return Settings.System.getInt(context.getContentResolver(),
				           Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
			}
				

			}
		
		private static boolean isLocationModeOn(Context context) {
			 LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );

		    if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
		        return true;
		    }
		    else
		    	return false;
//			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
//				 return Settings.System.getInt(context.getContentResolver(),
//				           Settings.System.LOCATION_PROVIDERS_ALLOWED, 0) != 0;
//				   
//			}
//			else
//			{
//				 return Settings.System.getInt(context.getContentResolver(),
//				           Settings., 0) != 0;
//			}
//				
			

			}

		 public  void setAutoOrientationEnabled( boolean enabled)
		    {
		          Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, enabled ? 1 : 0);
		    }

}
