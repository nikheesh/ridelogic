package com.vyooha.ridelogic.activities;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;

public class BatteryService  extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	super.onStartCommand(intent, flags, startId);
        Log.d("MyService", "onStartCommand");
        // do not receive all available system information (it is a filter!)
        final IntentFilter battChangeFilter = new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED);
        // register our receiver
        this.registerReceiver(this.batteryChangeReceiver, battChangeFilter);
        return START_STICKY;
    }

    private final BroadcastReceiver batteryChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(final Context context, final Intent intent) {
          int per=  checkBatteryLevel(intent);
          intent.setAction("BATTERYLEVEL");
          intent.putExtra("perc", per);
          Log.e("=====batterry=======perc", per+"");
         context.sendBroadcast(intent);
            
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        // There are Bound an Unbound Services - you should read something about
        // that. This one is an Unbounded Service.
        return null;
    }

    private int checkBatteryLevel(Intent batteryChangeIntent) {
        // some calculations
        final int currLevel = batteryChangeIntent.getIntExtra(
                BatteryManager.EXTRA_LEVEL, -1);
        final int maxLevel = batteryChangeIntent.getIntExtra(
                BatteryManager.EXTRA_SCALE, -1);
        final int percentage = (int) Math.round((currLevel * 100.0) / maxLevel);

        Log.d("MySerive", "current battery level: " + percentage);

        // full battery
//        if (percentage == 100) {
//            Log.d("MySerive", "battery fully loaded");
//            Intent intent = new Intent(getBaseContext(), SecondActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            getApplication().startActivity(intent);
//        }
        // do not forget to unregister
      //  unregisterReceiver(batteryChangeReceiver);
        return percentage;
        
    }
    
    @Override
	  public void onDestroy() {
	    super.onDestroy();
	    unregisterReceiver(batteryChangeReceiver);
	  }

}