package com.vyooha.ridelogic.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.vyooha.ridelogic.views.SlidingTabLayout;

public class Main_PagerActivity extends ActionBarActivity {
		 
	    // Declaring Your View and Variables
	 
	   // Toolbar toolbar;
	    ViewPager pager;
	   MainPagerAdapter adapter;
	    SlidingTabLayout tabs;
	    LinearLayout linear_contact;
	    
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
	        setContentView(R.layout.mainactivity_fragment);
	        
//	        Intent intent = getIntent();
//	        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
//	        linear_contact=(LinearLayout)findViewById(R.id.linear_contact);
//	        BitmapDrawable background = new BitmapDrawable();
//	        
//	        linear_contact.
	       CharSequence Titles[]={"Dialer","Contacts"};
	        int Numboftabs =2;
	     
	        // Creating The Toolbar and setting it as the Toolbar for the activity
	 
//	        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//	        setSupportActionBar(toolbar);
	// 
	 
	        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
	        adapter =  new MainPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);
	 
	        // Assigning ViewPager View and setting the adapter
	        pager = (ViewPager) findViewById(R.id.mainpager);
	        pager.setAdapter(adapter);
	 
	        // Assiging the Sliding Tab Layout View
	     //   tabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
	       // tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
	 
	        // Setting Custom Color for the Scroll bar indicator of the Tab View
//	        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//	            @Override
//	            public int getIndicatorColor(int position) {
//	                return getResources().getColor(Color.RED);
//	            }
	//
//				@Override
//				public int getDividerColor(int position) {
//					// TODO Auto-generated method stub
//					 return getResources().getColor(Color.BLACK);
//				}
//	        });
	// 
	        // Setting the ViewPager For the SlidingTabsLayout
	      //  tabs.setViewPager(pager);
	 
	 
	 
	    }
	 

}
