package com.vyooha.ridelogic.activities;

import com.parse.ParseFile;
import com.parse.ParseUser;
import com.vyooha.ridelogic.activities.SimpleGestureFilter.SimpleGestureListener;
import com.vyooha.ridelogic.widget.SpeedometerService;
import com.vyooha.ridelogic.widget.SpeedometerWidgetProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ProfileActivity extends Activity implements SimpleGestureListener{
	private SimpleGestureFilter detector;
//	 Intent intent;
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
			requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
	        setContentView(R.layout.profileactivity);
	        
	        detector=new SimpleGestureFilter(this, this);
	        
	        TextView prof_name=(TextView)findViewById(R.id.person_name);
	        TextView viewprof=(TextView)findViewById(R.id.viewprofile);
	        TextView setting=(TextView)findViewById(R.id.viewprofilesettings);
	        RelativeLayout opencompass=(RelativeLayout)findViewById(R.id.openmain_btn);
	        ImageView profile_photo=(ImageView)findViewById(R.id.profile_photo);
	        
	        ParseUser user=ParseUser.getCurrentUser();
	        prof_name.setText(user.getString("name"));
	        ParseFile pf=user.getParseFile("profile_pic");
	        byte[] bitmapdata;
	        Bitmap bitmap = null;
			try {
				bitmapdata = pf.getData();
				 bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
			}
			catch (Exception e){
				Log.e("", e.toString());
			}
			if(bitmap!=null)
			{
				profile_photo.setImageBitmap(bitmap);
			}
//	        intent=new Intent(getApplicationContext(), SpeedWidgetService.class);
//	        startService(intent);
	       

	      
	        
	        
	        viewprof.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(),ProfileDetails.class);
 					i.putExtra("choose", false);
 				startActivity(i);
				}
			});
	        
 setting.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(),SettingsActivity.class);
// 					i.putExtra("choose", false);
 				startActivity(i);
				}
			});
	       
 opencompass.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
//		stopService(intent);
		 Intent nextActivity = new Intent(getApplicationContext(),CompassActivity.class);
		  startActivity(nextActivity);
		  //push from bottom to top
		  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
});
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent me){
	    // Call onTouchEvent of SimpleGestureFilter class
	     this.detector.onTouchEvent(me);
	   return super.dispatchTouchEvent(me);
	}
	
	
	@Override
	public void onSwipe(int direction) {
		// TODO Auto-generated method stub
		
		 String str = "";
		  
		  switch (direction) {
		  
		  case SimpleGestureFilter.SWIPE_RIGHT : 
		                                           break;
		  case SimpleGestureFilter.SWIPE_LEFT : 
		 
		
			  Intent nextActivity = new Intent(getApplicationContext(),CompassActivity.class);
			  startActivity(nextActivity);
			  //push from bottom to top
			  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

		  
		                                                 break;
		  case SimpleGestureFilter.SWIPE_DOWN : 
		                                                 break;
		  case SimpleGestureFilter.SWIPE_UP :    
		                                                 break;
		  
		  }
		
		
	}

	@Override
	public void onDoubleTap() {
		// TODO Auto-generated method stub
		
	}
	
}
