package com.vyooha.ridelogic.activities;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PlanATrip1 extends Fragment {
	EditText nametxt;
	TextView datetxt;
	Button next;
	String Name;
	 private DatePicker datePicker;
	   private Calendar calendar;
	  
	   private int year, month, day;
	 @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	       View contacts=inflater.inflate(R.layout.plan_a_trip1, container, false); 
	       return contacts;
	    }  
	       
	       
	    @Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
	        
	        nametxt=(EditText)getActivity().findViewById(R.id.name_plantrip);
	        nametxt.setHintTextColor(Color.BLACK);
	        datetxt=(TextView)getActivity().findViewById(R.id.date_plantrip);
	        next=(Button)getActivity().findViewById(R.id.Next1); 
	        calendar = Calendar.getInstance();
	        year = calendar.get(Calendar.YEAR);
	        month = calendar.get(Calendar.MONTH);
	        day = calendar.get(Calendar.DAY_OF_MONTH);
	        datetxt.setText(day+"/"+month+"/"+year);
	        //showDate(year, month+1, day);
	        ImageView back=(ImageView) getActivity().findViewById(R.id.left_arrow);
	        back.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getActivity(), MainMenuActivity.class);
					startActivity(i);
				}
			});
	        next.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(nametxt.getText().toString()!=""){
			PlanATrip2 ptrip2=new PlanATrip2();
			Bundle bundle=new Bundle();
			 bundle.putString("name", nametxt.getText().toString());
			 
			 bundle.putString("date", datetxt.getText().toString());
			 Log.e("passsed date is=====", datetxt.getText().toString());
			 ptrip2.setArguments(bundle);
			 getFragmentManager().beginTransaction()
			 .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
		        .replace(R.id.fragment1, ptrip2 )
		         .addToBackStack("tag1")
		        .commit();
					}
					else
					{
						Toast.makeText(getActivity(), "Please add a name", Toast.LENGTH_LONG).show();
					}
			
				}
			});
	        datetxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					DialogFragment newFragment = new SelectDateFragment();
		            newFragment.show(getFragmentManager(), "DatePicker");
				
				}
			});
	    
	    }
	    
	    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

	        @Override
	        public Dialog onCreateDialog(Bundle savedInstanceState) {
	        final Calendar calendar = Calendar.getInstance();
	        int yy = calendar.get(Calendar.YEAR);
	        int mm = calendar.get(Calendar.MONTH);
	        int dd = calendar.get(Calendar.DAY_OF_MONTH);
	        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
	        }

	        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
	            populateSetDate(yy, mm+1, dd);
	        }
	        public void populateSetDate(int year, int month, int day) {
	            datetxt.setText(day+"/"+month+"/"+year);
	            }

	    }
	    

}
