package com.vyooha.ridelogic.activities;



import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends Activity
{	  private final int SELECT_PHOTO = 1;
	ImageView selected_photo;
	  byte[] array;
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Signing in. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				        WindowManager.LayoutParams.FLAG_FULLSCREEN);
				     setContentView(R.layout.signup);
				     
				     ImageView login=(ImageView)findViewById(R.id.signup_botton);
				     final EditText usernameEditText=(EditText)findViewById(R.id.username_signup);
				     final EditText nameEditText=(EditText)findViewById(R.id.name_signup);
				    final EditText passwordEditText=(EditText)findViewById(R.id.password_signup);
				   final  EditText mobileNumberEditText=(EditText)findViewById(R.id.mobilenumber_signup);
				    final EditText emailEditText=(EditText)findViewById(R.id.email_signup);
				  selected_photo=(ImageView)findViewById(R.id.profile_photo);
				     
				    ImageView back=(ImageView)findViewById(R.id.left_arrow);
				   
				    
					    //final String adTitle = adTitleEditText.getText().toString().trim();
					   // final String email = emailEditText.getText().toString().trim();
					   
				     
				     final ParseUser user = new ParseUser();
				     
				     
				     
				     
				     selected_photo.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
							photoPickerIntent.setType("image/*");
							startActivityForResult(photoPickerIntent, SELECT_PHOTO);
						}
					});
				     
				     
				     login.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								startLoading();
								
								  final String username = usernameEditText.getText().toString();
								  final String password = passwordEditText.getText().toString().trim();
								  final String mobileNumber = mobileNumberEditText.getText().toString().trim();
								  final String email = emailEditText.getText().toString();
								   final String name=nameEditText.getText().toString();
								   final ParseFile pf=new ParseFile("image.png",array);
								
								 boolean validationError = false;
								    int c=0;
								    
								    String errorMessage="";
								    if (name.length() == 0) {
								    	c++;
								    	
								        validationError = true;
								        errorMessage="Enter your name";
								    }
								    
								    
								    if (username.length() == 0) {
								    	c++;
								    	
								        validationError = true;
								        errorMessage="Enter user name";
								    }
								 if (password.length() == 0) {
								      
								      validationError = true;
								      if(c==0){
								    	  errorMessage="Enter password";
								    	}
								      c++;
								      
								    }
								    if (mobileNumber.length()==0) {
								    	validationError=true;
								    	 if(c==0){
									    	  errorMessage="Enter Mobile Number";
									    	}
									      c++;
								      validationError = true;
								     // validationErrorMessage.append(getString(R.string.error_mismatched_passwords));
								    }
								    
								    if ( email.length() == 0) {
								    	validationError = true;
								    	 if(c==0){
									    	  errorMessage="Enter email";
									    	}
									      c++;
									    	
								    	
								      }
								    else 
								    	
								    	{
								    	//Log.e("mmmmmmmmm",email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@ [A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")+"");
								    	if(isEmailValid(email)==false)
								           {
								    	validationError = true;
								    	 if(c==0){
									    	  errorMessage="Enter a valid email address";
									    	}
									      c++;
								           }
								    	}
								    
								
								    if (validationError) {
									  Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_LONG).show();
									    stopLoading();
									     
									      
									    }
								    else{
								    	 pf.saveInBackground(new SaveCallback(){

								 			@Override
								 			public void done(ParseException e) {
								 				if(e == null){
								
								 user.setUsername(username);
								    user.setPassword(password);
								    user.setEmail(email);
								    user.put("phone",mobileNumber);
								   user.put("name", name);
								   user.put("profile_pic",pf); 
								   user.put("isLocationPublic", true);
								   user.put("isActive", true);
								 user.signUpInBackground(new SignUpCallback() {
								      @Override
								      public void done(ParseException e) {
								        
								        if (e != null) {
								          // Show the error message
								         
								        } else {
								        	stopLoading();
								         
								        	   ParseApplication appState = ((ParseApplication)getApplicationContext());
								      	     appState.user=user;
								        	
								        	
								        	 Intent intent = new Intent(getApplicationContext(),ProfileDetails.class);
									         // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
									      	intent.putExtra("choose", false);
									          startActivity(intent);
								        }
								      }
								    });
								 
								 				}}});
								   
//				  				stopLoading();
								    }
							}
				        });
				     back.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								
								finish();
								
							}
							});
				     
				     
				     
				     
				     
	 }
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
	        super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 

	        switch(requestCode) { 
	        case SELECT_PHOTO:
	            if(resultCode == RESULT_OK){
					try {
						 final Uri imageUri = imageReturnedIntent.getData();
							
							final InputStream imageStream = getContentResolver().openInputStream(imageUri);
							Log.e("image",imageStream+"");
							final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
							Bitmap selectedimage = Bitmap.createScaledBitmap(selectedImage, 256, 256
									* selectedImage.getHeight() / selectedImage.getWidth(), false);
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							selectedimage.compress(Bitmap.CompressFormat.PNG, 100, stream);
							//byte[] byteArray 
							array= stream.toByteArray();
							Log.e("byte array", array.toString());
							// array = buffer.array();
							selected_photo.setImageBitmap(selectedImage);
	
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

	            }
	       
	        }
	    }
	 
	 public static boolean isEmailValid(String email) {
		    boolean isValid = false;

		    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		    CharSequence inputStr = email;

		    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		    Matcher matcher = pattern.matcher(inputStr);
		    if (matcher.matches()) {
		        isValid = true;
		    }
		    return isValid;
		}
		

}
