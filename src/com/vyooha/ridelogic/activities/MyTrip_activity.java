package com.vyooha.ridelogic.activities;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.vyooha.ridelogic.views.RoundedImageView;

public class MyTrip_activity  extends Fragment {
  
    ArrayAdapter<TripItemClass> mytripAdapter;

    ListView list;
    EditText search;
    TripItemClass contclass;
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
       View contacts=inflater.inflate(R.layout.mytripactivity, container, false); 
       return contacts;
    }  
       
       
    @Override 
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);  
    
   
         mytripAdapter = new ArrayAdapter<TripItemClass>(getActivity(), 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.mytrip_item, null);

                   final   TripItemClass cont = this.getItem(position);
                      try{
                    	  TextView tripname = (TextView)convertView.findViewById(R.id.get_tripname);
                          tripname.setText(cont.tripname);
                       TextView source = (TextView)convertView.findViewById(R.id.get_sourceplanned);
                       	  source.setText(cont.source);
                       TextView destination = (TextView)convertView.findViewById(R.id.get_destination);
                      	  destination.setText(cont.destination);
                       TextView createdby = (TextView)convertView.findViewById(R.id.get_createdby);
                      	  createdby.setText(cont.createdby);
                       TextView planneddate = (TextView)convertView.findViewById(R.id.get_planneddate);
                      	  planneddate.setText(cont.planned_date);
                      	       Button viewdetails=(Button)convertView.findViewById(R.id.trip_details_planned);
               viewdetails.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					 Intent i=new Intent(getActivity().getApplicationContext(), TripInDetailActivity.class);
					i.putExtra("id", cont.tripid);
					i.putExtra("parentid", cont.parentid);
					i.putExtra("tripname", cont.tripname);
					i.putExtra("source", cont.source);
					i.putExtra("dest", cont.destination);
					i.putExtra("createdby", cont.createdby);
					i.putExtra("date", cont.planned_date);
					i.putExtra("type", "mytrip");
					
					 
					 startActivity(i);
					
				}
			});
               
              
                
                
                      }
                      catch (Exception e){}
                      
            //    text.setText(tweet.get("mdesc").getAsString());
                return convertView;
            }
        };
    



 list=(ListView)getActivity().findViewById(R.id.mytrip_listview);
list.setAdapter(mytripAdapter);

ParseCloud.callFunctionInBackground("viewMyPTrips", new HashMap<String, Object>(), new FunctionCallback<List<ParseObject>>() {
	

	@Override
	public void done(List<ParseObject> result, ParseException e) {
		// TODO Auto-generated method stub
		if (e == null) {
		      // result is "Hello world!"
			Toast.makeText(getActivity(), "successfully loaded", Toast.LENGTH_LONG).show();
			for(int i=0;i<result.size();i++){
				
			ParseObject trip=result.get(i).getParseObject("pTrip_obj");
		
		contclass=new TripItemClass();
		contclass.tripname=trip.getString("trip_name");
		contclass.source=trip.getString("src_place_planned");
		contclass.destination=trip.getString("dest_place_planned");
		contclass.createdby=trip.getParseObject("user_obj").getString("name");
		contclass.planned_date="18/08/2014";
		contclass.tripid=result.get(i).getObjectId();
		contclass.parentid=trip.getObjectId();
		
		mytripAdapter.add(contclass);
			
			}
			}
	}
	});


    }

}
