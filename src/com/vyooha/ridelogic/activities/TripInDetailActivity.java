package com.vyooha.ridelogic.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;  
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;



//import com.google.maps.android.clustering.ClusterManager;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import com.vyooha.ridelogic.views.RoundedImageView;

public class TripInDetailActivity extends FragmentActivity implements OnCameraChangeListener {
	
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Retrieving data. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	
	final int min=0,max=20;
	
	
	
	SupportMapFragment map;

	ArrayAdapter<JSONObject> waypointAdapter;
	ArrayAdapter<InvitiesClass> invitiesAdapter;
	
	LatLng latlngnow;
	
	TextView tripname,source,destination,createdby,planneddate;
	ListView invity_listview,waypoint_listview;
	
Button finished,accept,reject,startptrip;
Intent intent;

InvitiesClass invitesobject;

ImageView img_back;

JSONArray listofmarkers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        invitiesAdapter = new ArrayAdapter<InvitiesClass>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.invities_item, null);

                   final   InvitiesClass cont = this.getItem(position);
                   TextView invity_txt=(TextView)convertView.findViewById(R.id.invity_item_text);
                   ImageView acceptrej=(ImageView)convertView.findViewById(R.id.invity_item_accept);
                   
                   invity_txt.setText(cont.name);
                   if(cont.acceptreject.equals("accepted")){
                	   acceptrej.setImageResource(R.drawable.near_visible);
                       
                   }
                   else if(cont.acceptreject.equals("rejected"))
                   {
                	   acceptrej.setImageResource(R.drawable.near_notvisible);
                       
                   }
                   else
                   {
                	   acceptrej.setImageResource(R.drawable.near_notvisible);
                       
                   }
                   
                   RoundedImageView invity_img=(RoundedImageView)convertView.findViewById(R.id.invity_img);
                   if(cont.prof_pic!=null)
                   {
                	   invity_img.setImageBitmap(cont.prof_pic);
                   }
                   else
                	   invity_img.setImageResource(R.drawable.def_contact);
                   
                   
                   return convertView;
                   
            }};
        
            waypointAdapter = new ArrayAdapter<JSONObject>(this, 0) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    if (convertView == null)
                        convertView = getLayoutInflater().inflate(R.layout.waypoint_item, null);

                       final   JSONObject cont = this.getItem(position);
                       
                       TextView waypoint_txt=(TextView)convertView.findViewById(R.id.waypoint_item_text);
                       try {
						waypoint_txt.setText(cont.getString("place_name"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                       return convertView;
                       
                }};
        
        
        
        
        
        
		
		setContentView(R.layout.planned_trip_details);
		
		startLoading();
		
	intent=getIntent();

		 tripname = (TextView)findViewById(R.id.getdetail_tripname);
         tripname.setText(intent.getStringExtra("tripname"));
       source = (TextView)findViewById(R.id.getdetail_sourceplanned);
      	  source.setText(intent.getStringExtra("source"));
       destination = (TextView)findViewById(R.id.getdetail_destination);
     	  destination.setText(intent.getStringExtra("dest"));
       createdby = (TextView)findViewById(R.id.getdetail_createdby);
     	  createdby.setText(intent.getStringExtra("createdby"));
       planneddate = (TextView)findViewById(R.id.getdetail_planneddate);
     	  planneddate.setText(intent.getStringExtra("date"));
      
	invity_listview=(ListView)findViewById(R.id.get_invities_listview);
	invity_listview.setAdapter(invitiesAdapter);
	waypoint_listview=(ListView)findViewById(R.id.get_waypoint_listview);
	waypoint_listview.setAdapter(waypointAdapter);
	
	
	
	map = (SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.get_plannedtrip_map);
	
	img_back=(ImageView)findViewById(R.id.left_arrow);
	img_back.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
		}
	});
	

	HashMap<String, String> inputparm=new HashMap<String, String>();
	
	
	if(intent.getStringExtra("type").equals("mytrip")){
		
			inputparm.put("childPTrip_objid", intent.getStringExtra("id"));

			ParseCloud.callFunctionInBackground("viewMyTripDetails", inputparm, new FunctionCallback<Map<String,Object>>() {
	

					@Override
					public void done(Map<String,Object> result, ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							
							stopLoading();
							
							@SuppressWarnings("unchecked")
							ArrayList<Map<String,Object>> invities=(ArrayList<Map<String, Object>>) result.get("invitees");
							Log.e("no of invities retrieved", Integer.toString(invities.size()));
							for(int i=0;i<invities.size();i++){
								Map<String,Object> row=invities.get(i);
							InvitiesClass	invitesobject=new InvitiesClass();
								invitesobject.acceptreject=row.get("status").toString();
								Log.e("invity status", invitesobject.acceptreject);
								invitesobject.name=row.get("name").toString();
								Log.e("invity status", invitesobject.name);
								invitesobject.obid=row.get("userid").toString();
								invitiesAdapter.add(invitesobject);
								
							}
							
							ParseObject ob1=(ParseObject) result.get("trip_details");
							ParseObject ob=ob1.getParseObject("pTrip_obj");
							JSONArray waypointlatlng=ob.getJSONArray("waypoints");
							Log.e("list of waypoints",waypointlatlng.toString());
							listofmarkers=new JSONArray();
							for(int i=0;i<waypointlatlng.length();i++){
								JSONObject json=new JSONObject();
								String waypoint;
								double lat,lng;
								try {
									lat = waypointlatlng.getJSONObject(i).getDouble("lat");
									 lng=waypointlatlng.getJSONObject(i).getDouble("lng");
									
									 waypoint=getAddress(lat, lng);
									//waypoint="waypoint"+i;
									 json.put("lat", lat);
									 json.put("lng", lng);
									 json.put("place_name", waypoint);
									 Log.e("waypoints", waypoint);
									 waypointAdapter.add(json);
									 listofmarkers.put(i, json);
									 
									 
									 
								} catch (JSONException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								
								
							}
							JSONObject source=new JSONObject();
							JSONObject dest=new JSONObject();
							
							ParseGeoPoint geopoint=ob.getParseGeoPoint("src_geopoint_planned");
							try {
								source.put("lat", geopoint.getLatitude());
								source.put("lng", geopoint.getLongitude());
								source.put("place_name", ob.getString("src_place_planned"));
								
								dest.put("lat", ob.getDouble("dest_lat_planned"));
								dest.put("lng", ob.getDouble("dest_lon_planned"));
								dest.put("place_name", ob.getString("dest_place_planned"));
								
								
							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							

							 LatLng customer_loc,origin1,dest1;
							 ArrayList<LatLng> listwaypoints1=new ArrayList<LatLng>();
							try {
								
								ArrayList<Marker> markers=new ArrayList<Marker>();
								customer_loc = new LatLng(source.getDouble("lat"), source.getDouble("lng"));
								origin1=customer_loc;
								map.getMap().clear();
							    markers.add(map.getMap().addMarker(new MarkerOptions().position(customer_loc).title(source.getString("place_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))));
							       
							       CameraPosition cameraPosition = new CameraPosition.Builder().target(customer_loc).zoom(16.0f).build();
								   CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
							       map.getMap().animateCamera(cameraUpdate);
							       
							       customer_loc = new LatLng(dest.getDouble("lat"), dest.getDouble("lng"));
							     dest1=customer_loc;
							       markers.add(map.getMap().addMarker(new MarkerOptions().position(customer_loc).title(dest.getString("place_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))));
								     Log.e("no of markers",Integer.toString(listofmarkers.length()));
							       for(int i=0;i<listofmarkers.length();i++){
							    	   Log.e("index", ""+i);
							    	   JSONObject js=listofmarkers.getJSONObject(i);
							    	   customer_loc = new LatLng(js.getDouble("lat"), js.getDouble("lng"));
										listwaypoints1.add(customer_loc);
							    	   markers.add(map.getMap().addMarker(new MarkerOptions().position(customer_loc).title(js.getString("place_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))));
							       Log.e("placename", js.getString("place_name"));
							       }
				//for adjust camera position			
							       LatLngBounds.Builder b = new LatLngBounds.Builder();
							       for (Marker m : markers) {
							           b.include(m.getPosition());
							       }
							       LatLngBounds bounds = b.build();
							       //Change the padding as per needed
							       CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100,100,1);
							      
							       map.getMap().animateCamera(cu);
							       
							       
							       String url = getDirectionsUrl(origin1, dest1,listwaypoints1);
									 
					                 DownloadTask1 downloadTask = new DownloadTask1();

					                 // Start downloading json data from Google Directions API
					                 downloadTask.execute(url);
							       
							       
							       
							       
							       
							       
							       
							       
							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						      
								}
					}
					});

		
		
	}
	else{
		
		inputparm.put("pTrip_objid", intent.getStringExtra("id"));
		
		ParseCloud.callFunctionInBackground("viewInvitedTripDetails", inputparm, new FunctionCallback<Map<String,Object>>() {
			

			@Override
			public void done(Map<String,Object> result, ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					
					stopLoading();
					
					@SuppressWarnings("unchecked")
					ArrayList<Map<String,Object>> invities=(ArrayList<Map<String, Object>>) result.get("invitees");
					for(int i=0;i<invities.size();i++){
						Map<String,Object> row=invities.get(i);
						invitesobject=new InvitiesClass();
						invitesobject.acceptreject=row.get("status").toString();
						invitesobject.name=row.get("name").toString();
						invitesobject.obid=row.get("userid").toString();
						invitiesAdapter.add(invitesobject);
						
					}
					
					ParseObject ob=(ParseObject) result.get("trip_details");
					JSONArray waypointlatlng=ob.getJSONArray("waypoints");
					listofmarkers=new JSONArray();
					for(int i=0;i<waypointlatlng.length();i++){
						JSONObject json=new JSONObject();
						String waypoint;
						double lat,lng;
						try {
							lat = waypointlatlng.getJSONObject(i).getDouble("lat");
							 lng=waypointlatlng.getJSONObject(i).getDouble("lng");
							
							 waypoint=getAddress(lat, lng);
							
							 json.put("lat", lat);
							 json.put("lng", lng);
							 json.put("place_name", waypoint);
							 waypointAdapter.add(json);
							 listofmarkers.put(i, json);
							 
							 
							 
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
					}
					JSONObject source=new JSONObject();
					JSONObject dest=new JSONObject();
					
					ParseGeoPoint geopoint=ob.getParseGeoPoint("src_geopoint_planned");
					try {
						source.put("lat", geopoint.getLatitude());
						source.put("lng", geopoint.getLongitude());
						source.put("place_name", ob.getString("src_place_planned"));
						
						dest.put("lat", ob.getDouble("dest_lat_planned"));
						dest.put("lng", ob.getDouble("dest_lon_planned"));
						dest.put("place_name", ob.getString("dest_place_planned"));
						
						
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
//							
//							 ParseFile pf=user.getParseFile("profile_pic");
//			        byte[] bitmapdata;
//			        Bitmap bitmap = null;
//					try {
//						bitmapdata = pf.getData();
//						 bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
//					}
//					catch (Exception e){
//						Log.e("", e.toString());
//					}
//					if(bitmap!=null)
//					{
//						prof_pic.setImageBitmap(bitmap);
//					}
//			        
					 LatLng customer_loc;
					try {
						
						
						customer_loc = new LatLng(source.getDouble("lat"), source.getDouble("lng"));
						 map.getMap().clear();
					       map.getMap().addMarker(new MarkerOptions().position(customer_loc).title(source.getString("place_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
					       
					       CameraPosition cameraPosition = new CameraPosition.Builder().target(customer_loc).zoom(16.0f).build();
						   CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
					       map.getMap().animateCamera(cameraUpdate);
					       
					       customer_loc = new LatLng(dest.getDouble("lat"), dest.getDouble("lng"));
					       map.getMap().addMarker(new MarkerOptions().position(customer_loc).title(dest.getString("place_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
						     
					       for(int i=0;i<listofmarkers.length();i++){
					    	   JSONObject js=listofmarkers.getJSONObject(i);
					    	   customer_loc = new LatLng(js.getDouble("lat"), js.getDouble("lng"));
								
					    	   map.getMap().addMarker(new MarkerOptions().position(customer_loc).title(js.getString("place_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
								  
					       }
					
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				      
					}
			}
			});
		
		
	}
	
	
	
	
	accept=(Button)findViewById(R.id.accept_ptrip);
	
accept.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			HashMap<String,String> input_param=new HashMap<String, String>();
			if(intent.getStringExtra("type").equals("mytrip"))
			{
			input_param.put("pTrip_objid",intent.getStringExtra("parentid") );
			}
			else{
				input_param.put("pTrip_objid",intent.getStringExtra("id") );
			}
			ParseCloud.callFunctionInBackground("acceptPTrip", input_param, new FunctionCallback<Map<String,Object>>() {
				

				@Override
				public void done(Map<String,Object> result, ParseException e) {
					// TODO Auto-generated method stub
					 if (e == null) {
					      // result is "Hello world!"
						 Toast.makeText(getApplicationContext(), result.get("message").toString(), Toast.LENGTH_LONG).show();
						
					   
					 }
				}
				});
		}
	});
	
reject=(Button)findViewById(R.id.reject_ptrip);
reject.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			HashMap<String,String> input_param=new HashMap<String, String>();
			if(intent.getStringExtra("type").equals("mytrip"))
			{
			input_param.put("pTrip_objid",intent.getStringExtra("parentid") );
			}
			else{
				input_param.put("pTrip_objid",intent.getStringExtra("id") );
			}
			ParseCloud.callFunctionInBackground("rejectPTrip", input_param, new FunctionCallback<String>() {
				

				@Override
				public void done(String result, ParseException e) {
					// TODO Auto-generated method stub
					 if (e == null) {
					      // result is "Hello world!"
						 Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
						
					   
					 }
				}
				});
		}
	});
	
startptrip=(Button)findViewById(R.id.start_ptrip);
startptrip.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			HashMap<String,Object> input_param=new HashMap<String, Object>();
			input_param.put("pTrip_objid",intent.getStringExtra("id") );
			ParseCloud.callFunctionInBackground("startPTrip", input_param, new FunctionCallback<Map<String,Object>>() {
				

				@Override
				public void done(Map<String,Object> result, ParseException e) {
					// TODO Auto-generated method stub
					 if (e == null) {
					      // result is "Hello world!"
						 Toast.makeText(getApplicationContext(), result.get("message").toString(), Toast.LENGTH_LONG).show();
							
					   
					 }
				}
				});
		}
	});
	
	
	
			finished=(Button)findViewById(R.id.finish_details);
			
			finished.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(),MainTabTrip.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
					finish();
				}
			});
			

			
			
	//		retrieveUsers();
			
			
			
			
				
		
			
		
	}
	
	  private String getAddress(double latitude, double longitude) {
	        StringBuilder result = new StringBuilder();
	        try {
	            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
	            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
	            if (addresses.size() > 0) {
	                Address address = addresses.get(0);
	                result.append(address.getLocality()).append("\n");
	                result.append(address.getCountryName());
	            }
	        } catch (IOException e) {
	            Log.e("tag", e.getMessage());
	        }

	        return result.toString();
	    }
	
	public void retrieveUsers()
	{
		
	}

	
	public void addMarkers(final double rad,LatLng latlng){
		map.getMap().clear();
		// map.getMap().animateCamera(CameraUpdateFactory.zoomTo(12));
		  map.getMap().moveCamera(CameraUpdateFactory.newLatLng(latlng));
		//ParseUser currentuser=ParseUser.getCurrentUser();
		ParseGeoPoint g=new ParseGeoPoint(latlng.latitude,latlng.longitude);//currentuser.getParseGeoPoint("current_location");
		//final double cLat=g.getLatitude();
		//final double cLong=g.getLongitude();
		//map.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 14));
		
	
		
		
	       
	//	map.getMap().addMarker(new MarkerOptions().position(latlng).title("nikheesh").icon(BitmapDescriptorFactory.fromBitmap(b)));
		
		
		
		
		
	}
	public void addMarkers(ArrayList<String> searchuser){
		
		
	
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.near_by_map, menu);
		return true;
	}

	@Override
	public void onCameraChange(CameraPosition arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	 @Override
	  public void onDestroy() {
	    super.onDestroy();
//	   if(widIntent!=null){
//		   stopService(widIntent);
//	   }
	  }
	 
	   /** A method to download json data from url */
	    private String downloadUrl(String strUrl) throws IOException{
	        String data = "";
	        InputStream iStream = null;
	        HttpURLConnection urlConnection = null;
	        try{
	            URL url = new URL(strUrl);
	 
	            // Creating an http connection to communicate with url
	            urlConnection = (HttpURLConnection) url.openConnection();
	 
	            // Connecting to url
	            urlConnection.connect();
	 
	            // Reading data from url
	            iStream = urlConnection.getInputStream();
	 
	            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
	 
	            StringBuffer sb  = new StringBuffer();
	 
	            String line = "";
	            while( ( line = br.readLine())  != null){
	                sb.append(line);
	            }
	 
	            data = sb.toString();
	 
	            br.close();
	 
	        }catch(Exception e){
	            Log.d("Exception while downloading url", e.toString());
	        }finally{
	            iStream.close();
	            urlConnection.disconnect();
	        }
	        return data;
	    }
	 
	  
	  
	    private class DownloadTask1 extends AsyncTask<String, Void, String>{
	    	 
	        // Downloading data in non-ui thread
	        @Override
	        protected String doInBackground(String... url) {
	 
	            // For storing data from web service
	            String data = "";
	 
	            try{
	                // Fetching the data from web service
	                data = downloadUrl(url[0]);
	            }catch(Exception e){
	                Log.d("Background Task",e.toString());
	            }
	            return data;
	        }
	 
	        // Executes in UI thread, after the execution of
	        // doInBackground()
	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	 
	            ParserTask1 parserTask = new ParserTask1();
	 
	            // Invokes the thread for parsing the JSON data
	            parserTask.execute(result);
	        }
	    }
	    

	    private String getDirectionsUrl(LatLng origin,LatLng dest,ArrayList<LatLng> listwaypoint){
	    	 
	        // Origin of route
	        String str_origin = "origin="+origin.latitude+","+origin.longitude;
	 
	        // Destination of route
	        String str_dest = "destination="+dest.latitude+","+dest.longitude;
	        String waypoint="waypoints=";
	        int i;
	        for( i=0;i<listwaypoint.size()-1;i++){
	        	waypoint=waypoint+listwaypoint.get(i).latitude+","+listwaypoint.get(i).longitude+"|";
	        }
	        if(i==listwaypoint.size()-1){
	        	waypoint=waypoint+listwaypoint.get(i).latitude+","+listwaypoint.get(i).longitude;
	 	       
	        }
	 
	        // Sensor enabled
	        String sensor = "sensor=false";
	 
	        // Building the parameters to the web service
	        String parameters = str_origin+"&"+str_dest+"&"+waypoint+"&"+sensor;
	 
	        // Output format
	        String output = "json";
	 
	        // Building the url to the web service
	        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
	 
	        return url;
	    }
	 
	    
	    private class ParserTask1 extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
	    	 
	        // Parsing the data in non-ui thread
	        @Override
	        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
	 
	            JSONObject jObject;
	            List<List<HashMap<String, String>>> routes = null;
	 
	            try{
	                jObject = new JSONObject(jsonData[0]);
	                DirectionsJSONParser parser = new DirectionsJSONParser();
	 
	                // Starts parsing data
	                routes = parser.parse(jObject);
	            }catch(Exception e){
	                e.printStackTrace();
	            }
	            return routes;
	        }
	 
	        // Executes in UI thread, after the parsing process
	        @Override
	        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
	            ArrayList<LatLng> points = null;
	            PolylineOptions lineOptions = null;
	            MarkerOptions markerOptions = new MarkerOptions();
	 
	            // Traversing through all the routes
	            for(int i=0;i<result.size();i++){
	                points = new ArrayList<LatLng>();
	                lineOptions = new PolylineOptions();
	 
	                // Fetching i-th route
	                List<HashMap<String, String>> path = result.get(i);
	 
	                // Fetching all the points in i-th route
	                for(int j=0;j<path.size();j++){
	                    HashMap<String,String> point = path.get(j);
	 
	                    double lat = Double.parseDouble(point.get("lat"));
	                    double lng = Double.parseDouble(point.get("lng"));
	                    LatLng position = new LatLng(lat, lng);
	 
	                    points.add(position);
	                }
	 
	                // Adding all the points in the route to LineOptions
	                lineOptions.addAll(points);
	                lineOptions.width(2);
	                lineOptions.color(Color.RED);
	            }
	 
	            // Drawing polyline in the Google Map for the i-th route
	            try{
	            map.getMap().addPolyline(lineOptions);
	            }
	            catch(Exception e)
	            {
	            	Toast.makeText(getApplicationContext(), "add route other than sea points", Toast.LENGTH_LONG).show();
	            }
	        }
	    }
	 
	 
	 

	 
}


