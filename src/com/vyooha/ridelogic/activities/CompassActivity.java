 package com.vyooha.ridelogic.activities;



import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;
import com.vyooha.ridelogic.activities.SimpleGestureFilter.SimpleGestureListener;
import com.vyooha.ridelogic.views.ArcProgress;
import com.vyooha.ridelogic.views.CompassView;



public class CompassActivity  extends Activity implements Listener,SimpleGestureListener,SensorEventListener
{
	private SimpleGestureFilter detector;
	public static float mScreenRatio;
	static final int NAVIGATION_REQUEST = 4; 
	    ArcProgress sp;
	    LocationManager locationManager;
	
	protected ProgressDialog proDialog;
	
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Saving Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	  private ConnectivityManager conmgr;
//	 private volatile AnimThread mAnimThread;
	//private CompassMode mCompassMode;
	 
SensorManager sensorManager;
private Sensor sensorAccelerometer;
private Sensor sensorMagneticField;

private TextView temperaturelabel;
ArrayList<String> weather = new ArrayList<String>();
String temperature;
retrieve_weatherTask callweather;
//private SensorManager sensormanagertemp;
//private Sensor temperature;

public CompassView  mCompassView;
  
private float[] valuesAccelerometer;
private float[] valuesMagneticField;
  
private float[] matrixR;
private float[] matrixI;
private float[] matrixValues;

//private SensorManager mgr;
//private Sensor temp;
  
private TextView battery;
private ImageView batimage;
private Intent batintent;
LinearLayout blur_id,compass_id;

ImageView blurImageView;


public Bitmap antenaimage;

boolean isOpen;
RelativeLayout layout_show;
LinearLayout Layout_buttons;


private SensorEventListener mOrientationListener = new SensorEventListener()
{
  public void onAccuracyChanged(Sensor paramAnonymousSensor, int paramAnonymousInt) {}
  
  public void onSensorChanged(SensorEvent paramAnonymousSensorEvent)
  {
    float f = paramAnonymousSensorEvent.values[0];
    TextView dateview = (TextView) findViewById(R.id.date_text);
    TextView timeview = (TextView) findViewById(R.id.time_txt);
   
    Date d=new Date();
    SimpleDateFormat sdf=new SimpleDateFormat("EE dd MMM");
    SimpleDateFormat sdf2=new SimpleDateFormat("hh:mm a");
    String currentDateTimeString = sdf.format(d);
    String currentDateTimeString2 = sdf2.format(d);
     
    timeview.setText(currentDateTimeString2);
    dateview.setText(currentDateTimeString);
  
   
      CompassActivity.this.onBearingChanged(f);
    
    
  }
};



private SensorManager mSensorManager;

  
 /** Called when the activity is first created. */
 @Override
 public void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
     
     DisplayMetrics localDisplayMetrics = new DisplayMetrics();
     getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
     mScreenRatio = localDisplayMetrics.density;
    
     setContentView(R.layout.compass_activity);
     
     compass_id=(LinearLayout)findViewById(R.id.compassactivity_id);
     blur_id=(LinearLayout)findViewById(R.id.blur_id);
     compass_id.setVisibility(View.VISIBLE);
     blur_id.setVisibility(View.GONE);
     
     conmgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
     
     detector = new SimpleGestureFilter(this,this);
     
     this.mCompassView=((CompassView)findViewById(R.id.CompassView1));
     
     this.mSensorManager = ((SensorManager)getSystemService("sensor"));
     
     isOpen=false;
    // sensormanagertemp = (SensorManager)getSystemService(SENSOR_SERVICE);
     
    // mgr = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
     temperaturelabel = (TextView) findViewById(R.id.temp_text);

//     temp = mgr.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
//     mgr.registerListener(this, temp, SensorManager.SENSOR_DELAY_NORMAL);
     
     
battery=(TextView) findViewById(R.id.battery_text);
batimage=(ImageView)findViewById(R.id.battery_image);
 batintent = new Intent(getApplicationContext(), BatteryService.class);
startService(batintent);
IntentFilter filter = new IntentFilter();
filter.addAction("BATTERYLEVEL");
registerReceiver(batteryLevelReceiver, filter);

//     temperature= sensormanagertemp.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
//     temperaturelabel = (TextView) findViewById(R.id.temp_text);
//     temperaturelabel.setText(""+temperature.getPower());

     
     
sp=(ArcProgress) findViewById(R.id.speed11);
     
     
        this.mSensorManager.registerListener(this.mOrientationListener, this.mSensorManager.getDefaultSensor(3), 2);

        TextView dateview = (TextView) findViewById(R.id.date_text);
        TextView timeview = (TextView) findViewById(R.id.time_txt);
         
        Date d=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("EE dd MMM");
        SimpleDateFormat sdf2=new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        String currentDateTimeString2 = sdf2.format(d);
         
        timeview.setText(currentDateTimeString2);
        dateview.setText(currentDateTimeString);
        
        
        layout_show=(RelativeLayout)findViewById(R.id.layout_show);
        Layout_buttons=(LinearLayout)findViewById(R.id.layout_buttons);
        Layout_buttons.setVisibility(View.GONE);
        RelativeLayout im=(RelativeLayout)findViewById(R.id.down_button);
        im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showhidebuttons();
				Runnable mRunnable;
				Handler mHandler=new Handler();

				mRunnable=new Runnable() {

				            @Override
				            public void run() {
				                // TODO Auto-generated method stub
				               // yourLayoutObject.setVisibility(View.INVISIBLE); //If you want just hide the View. But it will retain space occupied by the View.
				            	 TranslateAnimation anim = null;

				            	Animation.AnimationListener collapseListener = new Animation.AnimationListener() {
				            	    public void onAnimationEnd(Animation animation) {
				            	    	Layout_buttons.setVisibility(View.GONE);
						            	layout_show.setVisibility(View.VISIBLE);
				            	       // layout_show.setVisibility(View.VISIBLE);
				            	    }

				            	    @Override
				            	    public void onAnimationRepeat(Animation animation) {
				            	    }

				            	    @Override
				            	    public void onAnimationStart(Animation animation) {
				            	    }
				            	};

				               
				              //  layout_show.setVisibility(View.GONE);
				            	 anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, Layout_buttons.getHeight());
				                anim.setAnimationListener(collapseListener);
				                anim.setDuration(300);
				                anim.setInterpolator(new AccelerateInterpolator(1.0f));
				                Layout_buttons.startAnimation(anim);

				            	
				            	
				            	
				            	
				               //This will remove the View. and free s the space occupied by the View    
				            }
				        };
				        mHandler.postDelayed(mRunnable,10*1000);
			}
		});
        
        
        RelativeLayout cont=(RelativeLayout)findViewById(R.id.call_image);
        
   
        cont.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				
				
				Intent i=new Intent(getApplicationContext(),MainTabContactActivity.class);
  					//i.putExtra("isoffer", listcompany.isoffer);
			//	i.putExtra("BitmapImage", blur_b);
  				startActivity(i);
  				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
  				//stopLoading();
				
			}
        });
        
        RelativeLayout music=(RelativeLayout)findViewById(R.id.music_image);
        
        
        music.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
//				startLoading();
				Intent i=new Intent(getApplicationContext(),AndroidBuildingMusicPlayerActivity.class);
  					//i.putExtra("isoffer", listcompany.isoffer);
		
  				startActivity(i);
  				overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
//  				stopLoading();
				
			}
        });
        
        
        RelativeLayout settings=(RelativeLayout)findViewById(R.id.settings_image);
        
        
        settings.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),SettingsActivity.class);
					//i.putExtra("isoffer", listcompany.isoffer);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				
			}
        });
        
        
RelativeLayout power_image=(RelativeLayout)findViewById(R.id.share_image);
        
        
        power_image.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//startLoading();
				WindowManager.LayoutParams params = getWindow().getAttributes();
				 if(params.screenBrightness == 0){
					 		 Log.d("PARAMS", "NOW ZERO");
					 		 params.screenBrightness = -1;
				 }
				 else{
							 Log.d("PARAMS", "NOW ONE");		
							 params.screenBrightness = 0;
				 }
					 	
				 getWindow().setAttributes(params);
				
			}
        });
        
     
        
        
        ImageView fuel_image=(ImageView)findViewById(R.id.fuel_image);
        
        
        fuel_image.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//startLoading();
				Intent i=new Intent(getApplicationContext(),FuelDialogActivity.class);
  					//i.putExtra("isoffer", listcompany.isoffer);
  				startActivity(i);
  				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
  				//stopLoading();
				
			}
        });
        
ImageView trip=(ImageView)findViewById(R.id.flag_image);
        
        
        trip.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),TripDialogActivity.class);
					//i.putExtra("isoffer", listcompany.isoffer);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				
			}
        });
        callweather=  new retrieve_weatherTask();
        
        //Retrieve Weather data 
       
        
     locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		      // Called when a new location is found by the network location provider.
		    	sp.setProgress((int)(location.getSpeed()*18/5)); 
	
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}
		  };

		  if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
			 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 0, locationListener);
		  if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
		  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
		
		
 }
 
@Override
protected void onResume() {
	
 
	 super.onResume();
	 
	 if(callweather.getStatus()==AsyncTask.Status.PENDING)
	        if(checkInternet())
	      callweather.execute();
	
	 compass_id.setVisibility(View.VISIBLE);
     blur_id.setVisibility(View.GONE);
	 this.mSensorManager.registerListener(this.mOrientationListener, this.mSensorManager.getDefaultSensor(3), 2);

	 TextView dateview = (TextView) findViewById(R.id.date_text);
     TextView timeview = (TextView) findViewById(R.id.time_txt);
      
     Date d=new Date();
     SimpleDateFormat sdf=new SimpleDateFormat("EE dd MMM");
     SimpleDateFormat sdf2=new SimpleDateFormat("hh:mm a");
     String currentDateTimeString = sdf.format(d);
     String currentDateTimeString2 = sdf2.format(d);
      
     timeview.setText(currentDateTimeString2);
     dateview.setText(currentDateTimeString);
     startService(batintent);
     IntentFilter filter = new IntentFilter();
     filter.addAction("BATTERYLEVEL");
     registerReceiver(batteryLevelReceiver, filter);

}
 
@Override
protected void onPause() {
	 super.onPause();
    this.mSensorManager.unregisterListener(this.mOrientationListener);
 //   mgr.unregisterListener(this, temp);
    unregisterReceiver(batteryLevelReceiver);
//    this.sensormanagertemp.unregisterListener(this.mOrientationListener1);
    if(batintent!=null)
 stopService(batintent);
   
    compass_id.setDrawingCacheEnabled(true);

    compass_id.buildDrawingCache();

  Bitmap  blur_b = Bitmap.createBitmap(compass_id.getDrawingCache());
    compass_id.setDrawingCacheEnabled(false);
    
    compass_id.setVisibility(View.GONE);
    
    blur_id.setVisibility(View.VISIBLE);
    blurImageView=(ImageView)findViewById(R.id.blur_imageview);
    
   
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
    	blur_b=BlurImage(blur_b);
    }
    
    blurImageView.setImageBitmap(blur_b);
 

}



public void showhidebuttons() {
    TranslateAnimation anim = null;

    isOpen =false;

    if (!isOpen) {
    	Animation.AnimationListener collapseListener = new Animation.AnimationListener() {
    	    public void onAnimationEnd(Animation animation) {
    	    	layout_show.setVisibility(View.GONE);
    	        Layout_buttons.setVisibility(View.VISIBLE);
    	       // layout_show.setVisibility(View.VISIBLE);
    	    }

    	    @Override
    	    public void onAnimationRepeat(Animation animation) {
    	    }

    	    @Override
    	    public void onAnimationStart(Animation animation) {
    	    }
    	};

       
      //  layout_show.setVisibility(View.GONE);
        anim = new TranslateAnimation(0.0f, 0.0f, Layout_buttons.getHeight(), 0.0f);
        anim.setAnimationListener(collapseListener);
        anim.setDuration(300);
        anim.setInterpolator(new AccelerateInterpolator(1.0f));
        Layout_buttons.startAnimation(anim);
//    } else {
//        anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, Layout_buttons.getHeight());
//        anim.setAnimationListener(collapseListener);
//    
//
//    anim.setDuration(300);
//    anim.setInterpolator(new AccelerateInterpolator(1.0f));
//    Layout_buttons.startAnimation(anim);
    }
}




@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
Bitmap BlurImage (Bitmap input)
{
	RenderScript rsScript = RenderScript.create( getApplicationContext() );
	Allocation alloc = Allocation.createFromBitmap( rsScript, input, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT );

	ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create (rsScript,  Element.U8_4( rsScript ) );
	blur.setRadius (3);
	blur.setInput (alloc);

	Bitmap result = Bitmap.createBitmap (input.getWidth(), input.getHeight(), input.getConfig());
	Allocation outAlloc = Allocation.createFromBitmap (rsScript, result);
	blur.forEach (outAlloc);
	outAlloc.copyTo (result);

	rsScript.destroy ();
	return result;
}

 @Override
public void onAccuracyChanged(Sensor sensor, int accuracy) {

}
@Override
public void onSensorChanged(SensorEvent event) {
	String msg;
   temperaturelabel.setText(Float.toString(event.values[0])+"\u2103");
  
  Log.e("temp",Float.toString(event.values[0]));
 // temperaturelabel.invalidate();
}
private void onBearingChanged(float paramFloat)
{
//  if (this.mAnimThread != null) {
//    this.mAnimThread.setBearing(paramFloat);
//  }
	mCompassView.onBearingChanged(paramFloat);
}

@Override
public void onBackPressed() {
   finish();
   Intent i=new Intent(getApplicationContext(),MainMenuActivity.class);
		//i.putExtra("isoffer", listcompany.isoffer);
	startActivity(i);
   
}


@Override
public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.profile, menu);
	return true;
}
@Override
public boolean onOptionsItemSelected(MenuItem item){
  switch(item.getItemId()) {
  case R.id.action_logout:	          
	  ParseUser.logOut();				
	  startActivity(new Intent(this, FirstpageActivity.class));
      break;
 
  }
  return true;
}


@Override
public boolean dispatchTouchEvent(MotionEvent me){
    // Call onTouchEvent of SimpleGestureFilter class
     this.detector.onTouchEvent(me);
   return super.dispatchTouchEvent(me);
}
@Override
 public void onSwipe(int direction) {
  String str = "";
  
  switch (direction) {
  
  case SimpleGestureFilter.SWIPE_RIGHT : str = "Swipe Right";
  
  
  
  
  Intent nextActivity = new Intent(this,ProfileActivity.class);
  startActivity(nextActivity);
  //push from bottom to top 
  overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
  //slide from right to left
  //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                           break;
  case SimpleGestureFilter.SWIPE_LEFT :  str = "Swipe Left";
 

  Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?"));
  intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
                      startActivity(intent);

//  Intent mapActivity = new Intent(this,NearByMapActivity.class);
//  startActivity(mapActivity);
  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

  
                                                 break;
  case SimpleGestureFilter.SWIPE_DOWN :  str = "Swipe Down";
                                                 break;
  case SimpleGestureFilter.SWIPE_UP :    str = "Swipe Up";
                                                 break;
  
  }
  // Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
 }

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	try{
		Log.e("inside widget services", "hello");
		Log.d("RESULT CODE",Integer.toString(resultCode));
		// Check which request we're responding to
	    if (requestCode == NAVIGATION_REQUEST) {
	    	Log.e("inside widget services", "haihello");
	        // Make sure the request was successful
	      //  if (resultCode == RESULT_OK) {
	        	Log.e("inside widget services", "hai");
	        	
	        	//startService(widIntent);
	        	Log.e("inside widget services", "hai");
	        	
//	        	stopService(batintent);
	        //	startService(widIntent);
	            // The user picked a contact.
	            // The Intent's data Uri identifies which contact was selected.

	            // Do something with the contact here (bigger example below)
	       // }
	    }
	}catch(Exception ex){
		
		Log.d("servicestart",ex.toString());
	}

}

  
 @Override
 public void onDoubleTap() {
   // Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
 }

 BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
	 @Override
     public void onReceive(Context context, Intent intent) {
		 int per= intent.getIntExtra("perc", 0);
		 Log.e("battery inside broadcast", per+"");
		 if(intent.getAction().equals("BATTERYLEVEL")){
			int perc= intent.getIntExtra("perc", 0);
			battery.setText(Integer.toString(perc)+"%");
			int res=perc/20;
			switch (res) {
			case 0:batimage.setImageResource(R.drawable.battery_img_0);
				
				break;
			case 1:batimage.setImageResource(R.drawable.battery_img_20);
			
			break;
			case 2:batimage.setImageResource(R.drawable.battery_img_40);
			
			break;
			case 3:batimage.setImageResource(R.drawable.battery_img_60);
			
			break;
			case 4:batimage.setImageResource(R.drawable.battery_img_80);
			
			break;
			case 5:batimage.setImageResource(R.drawable.battery_img_100);
			
			break;

			default:
				break;
			}
			
			
			
		 }

     }
 };	

 
 
 protected class retrieve_weatherTask extends AsyncTask<Void, String, String> {
     @Override
	 protected void onPreExecute(){
	//startLoading();
	 }
     @Override
protected String doInBackground(Void... arg0) {
	 // TODO Auto-generated method stub
	 String qResult = "";
	 HttpClient httpClient = new DefaultHttpClient();
	 HttpContext localContext = new BasicHttpContext();
	 HttpGet httpGet = new HttpGet("http://weather.yahooapis.com/forecastrss?w=2295425&u=c&#8221;");

	 try {
	 HttpResponse response = httpClient.execute(httpGet,
	 localContext);
	 HttpEntity entity = response.getEntity();

	 if (entity != null) {
	 InputStream inputStream = entity.getContent();
	 Reader in = new InputStreamReader(inputStream);
	 BufferedReader bufferedreader = new BufferedReader(in);
	 StringBuilder stringBuilder = new StringBuilder();
	 String stringReadLine = null;
	 while ((stringReadLine = bufferedreader.readLine()) != null) {
	 stringBuilder.append(stringReadLine + "\n");
	 }
	 qResult = stringBuilder.toString();
	 }

	 } catch (ClientProtocolException e) {
	 e.printStackTrace();
//	 Toast.makeText(Weather.this, e.toString(), Toast.LENGTH_LONG)
//	 .show();
	 } catch (IOException e) {
	 e.printStackTrace();
//	 Toast.makeText(Weather.this, e.toString(), Toast.LENGTH_LONG)
//	 .show();
	 }

	 Document dest = null;
	 DocumentBuilderFactory dbFactory = DocumentBuilderFactory
	 .newInstance();
	 DocumentBuilder parser;
	 try {
	 parser = dbFactory.newDocumentBuilder();
	 dest = parser
	 .parse(new ByteArrayInputStream(qResult.getBytes()));
	 } catch (ParserConfigurationException e1) {
	 e1.printStackTrace();
	 Log.e( e1.toString(), "");
	 } catch (SAXException e) {
	 e.printStackTrace();
	 Log.e( e.toString(), "");
	 } catch (IOException e) {
	 e.printStackTrace();
	 Log.e( e.toString(), "");
	 }

	 Node temperatureNode = dest.getElementsByTagName("yweather:condition").item(0);
	 
	 temperature = temperatureNode.getAttributes()
	 .getNamedItem("temp").getNodeValue().toString();
	 Node tempUnitNode = dest.getElementsByTagName("yweather:units").item(0);
	 temperature = temperature + "�" +tempUnitNode.getAttributes().getNamedItem("temperature").getNodeValue().toString();


	 return qResult;

	 }

	 protected void onPostExecute(String result) {
	 System.out.println("POST EXECUTE");

	 temperaturelabel.setText(temperature);

	 }
	
	 }
 
 public boolean checkInternet(){
		NetworkInfo ni = conmgr.getActiveNetworkInfo();
		if (ni == null) {
		    // There are no active networks.
		    return false;
		}else{
			boolean isConnected = ni.isConnected();
			return isConnected;
		}
}

@Override
public void onGpsStatusChanged(int event) {
	// TODO Auto-generated method stub
	  int satellites = 0;
	  if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS || event == GpsStatus.GPS_EVENT_FIRST_FIX) {
          GpsStatus status = locationManager.getGpsStatus(null);
          Iterable<GpsSatellite> sats = status.getSatellites();
          // Check number of satellites in list to determine fix state
          
          for (GpsSatellite sat : sats) {
              //if(sat.usedInFix())
              satellites++;
          }
         
	    sp.setnoofSatellites(satellites); 
	    
}
}
 
 
 
 
}