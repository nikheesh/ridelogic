package com.vyooha.ridelogic.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class DialingActivity extends Fragment {
	 private  LinearLayout[] l;
	 private TextView[] txt;
	 EditText edittext;
	 String gettingtext;
	 Button callbtn,backbtn;
	 InputMethodManager imm;
	 int[] textids;
	 int i;

	  @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	
		  
	       View contacts=inflater.inflate(R.layout.dialpage, container, false); 
	       return contacts;
	    } 
    @Override 
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);  
        edittext=(EditText)getActivity().findViewById(R.id.phoneEdit);
      //  edittext.setFocusable(false);
        backbtn=(Button)getActivity().findViewById(R.id.backbutton);
         imm = (InputMethodManager)getActivity().getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
		
        
//        InputMethodManager imm = (InputMethodManager)
//        		getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//
//        		if (imm != null){
//        		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
//        		}
        l=new LinearLayout[12];
        txt=new TextView[12];
        
      l[0]=(LinearLayout)getActivity().findViewById(R.id.numl0);
      l[1]=(LinearLayout)getActivity().findViewById(R.id.numl1);
      l[2]=(LinearLayout)getActivity().findViewById(R.id.numl2);
      l[3]=(LinearLayout)getActivity().findViewById(R.id.numl3);
      l[4]=(LinearLayout)getActivity().findViewById(R.id.numl4);
      l[5]=(LinearLayout)getActivity().findViewById(R.id.numl5);
      l[6]=(LinearLayout)getActivity().findViewById(R.id.numl6);
      l[7]=(LinearLayout)getActivity().findViewById(R.id.numl7);
      l[8]=(LinearLayout)getActivity().findViewById(R.id.numl8);
      l[9]=(LinearLayout)getActivity().findViewById(R.id.numl9);
      l[10]=(LinearLayout)getActivity().findViewById(R.id.numlstar);
      l[11]=(LinearLayout)getActivity().findViewById(R.id.numlhash);
 
      txt[0]=(TextView)getActivity().findViewById(R.id.Num0);
      txt[1]=(TextView)getActivity().findViewById(R.id.Num1);
      txt[2]=(TextView)getActivity().findViewById(R.id.Num2);
      txt[3]=(TextView)getActivity().findViewById(R.id.Num3);
      txt[4]=(TextView)getActivity().findViewById(R.id.Num4);
      txt[5]=(TextView)getActivity().findViewById(R.id.Num5);
      txt[6]=(TextView)getActivity().findViewById(R.id.Num6);
      txt[7]=(TextView)getActivity().findViewById(R.id.Num7);
      txt[8]=(TextView)getActivity().findViewById(R.id.Num8);
      txt[9]=(TextView)getActivity().findViewById(R.id.Num9);
      txt[10]=(TextView)getActivity().findViewById(R.id.Numstar);
      txt[11]=(TextView)getActivity().findViewById(R.id.Numhash);
       
       callbtn=(Button)getActivity().findViewById(R.id.callbutton);
       backbtn=(Button)getActivity().findViewById(R.id.backbutton);
       for( i=0;i<12;i++){
//    	  l[i]=(LinearLayout)getActivity().findViewById(ids[i]);
//    	  txt[i]=(TextView)getActivity().findViewById(textids[i]);
   final int j=i;
    	  
    	  l[i].setOnClickListener(new OnClickListener() {
              

  			@Override
  			public void onClick(View arg0) {
  				
  			//edittext.setText(edittext.getText().toString()+txt[j].getText().toString());
  			//edittext.setFocusable(true);
  			int curPos = edittext.getSelectionStart();
  			String str = edittext.getText().toString();

  			String str1 = (String) str.substring(0, curPos);


  			String str2 = (String) str.substring(curPos);


  			edittext.setText(str1+ txt[j].getText() +str2);
  			edittext.setSelection(curPos+1);
  			 imm = (InputMethodManager)getActivity().getSystemService(
  			      Context.INPUT_METHOD_SERVICE);
  		imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
  			
  			
  			
  				
  			}
          });
    	   
       }
       
       
       edittext.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			//edittext.setFocusable(true);
			
			
	  imm = (InputMethodManager)getActivity().getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
		}
	});
      
       edittext.setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener() {
		
		@Override
		public void onSystemUiVisibilityChange(int visibility) {
			// TODO Auto-generated method stub
			 imm = (InputMethodManager)getActivity().getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
		}
	});
       
       edittext.setOnFocusChangeListener(new OnFocusChangeListener() {
		
		@Override
		public void onFocusChange(View arg0, boolean arg1) {
			// TODO Auto-generated method stub
			 imm = (InputMethodManager)getActivity().getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
		}
	});
       
 
       
       callbtn.setOnClickListener(new OnClickListener() {
           

			@Override
			public void onClick(View arg0) {
				
				Log.i("Make call", "");

			      Intent phoneIntent = new Intent(Intent.ACTION_CALL);
			      phoneIntent.setData(Uri.parse("tel:"+edittext.getText().toString()));

			      try {
			         startActivity(phoneIntent);
			         getActivity().finish();
			         Log.i("Finished making a call...", "");
			      } catch (android.content.ActivityNotFoundException ex) {
			         Toast.makeText(getActivity().getApplicationContext(), 
			         "Call faild, please try again later.", Toast.LENGTH_SHORT).show();
			      }
				
			}
       });
        
       
       backbtn.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			int curPos = edittext.getSelectionStart();
  			String str = edittext.getText().toString();
  			String str1="";
if(curPos>0)
  	 str1 = (String) str.substring(0, curPos);
  			try{
if(str1.length()>0)
{
	str1=(String) str.substring(0, curPos-1);
}}
  			catch (Exception e){}

  			String str2 = (String) str.substring(curPos);


  			edittext.setText(str1 +str2);
  			
  			if(curPos==0)
  			{edittext.setSelection(0);
  			
  			}
  			else
  			edittext.setSelection(curPos-1);
		}
	});
       
       // String numberToDial = "tel:";
        //getActivity().startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(numberToDial)));
    }
}
