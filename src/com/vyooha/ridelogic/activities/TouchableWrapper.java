package com.vyooha.ridelogic.activities;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.Toast;

public class TouchableWrapper extends FrameLayout{

	//public static boolean mMapIsTouched = false;
	
	public TouchableWrapper(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public TouchableWrapper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
    switch (ev.getAction()) {
        case MotionEvent.ACTION_DOWN:
       ParseApplication.mMapIsTouched = true;
        break;

    case MotionEvent.ACTION_UP:
        ParseApplication.mMapIsTouched = false;
        break;
        
        }

        return super.dispatchTouchEvent(ev);
    }
}
