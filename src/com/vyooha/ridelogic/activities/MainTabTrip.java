package com.vyooha.ridelogic.activities;

import com.vyooha.ridelogic.views.SlidingTabLayout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainTabTrip   extends ActionBarActivity {
 
    // Declaring Your View and Variables
 
   // Toolbar toolbar;
    ViewPager pager;
   PageAdapterPlanTrip adapter;
    SlidingTabLayout tabs;
  
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.main_tab_trip);
        ImageView back=(ImageView) findViewById(R.id.left_arrow);
        back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(), MainMenuActivity.class);
				startActivity(i);
			}
		});
//        Intent intent = getIntent();
//        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
//        linear_contact=(LinearLayout)findViewById(R.id.linear_contact);
//        BitmapDrawable background = new BitmapDrawable();
//        
//        linear_contact.
        CharSequence Titles[]={"My Trips","Invited Trip"};
        int Numboftabs =2;
     
      
 
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new PageAdapterPlanTrip(getSupportFragmentManager(),Titles,Numboftabs);
 
        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager_trip);
        pager.setAdapter(adapter);
 
        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs_trip);
      
        tabs.setViewPager(pager);
 
 
 
    }
 
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
 
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
 
        return super.onOptionsItemSelected(item);
    }
}