package com.vyooha.ridelogic.activities;

import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class LocationService  extends Service {
	
	
	Location oldloc,oldlocdistinc;
	boolean firsttake,flagdistcurrent,flagdistinc;
	 ParseUser user;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	super.onStartCommand(intent, flags, startId);
    	firsttake=true;
    	user=ParseUser.getCurrentUser();
    	 LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

 		// Define a listener that responds to location updates
 		LocationListener locationListener = new LocationListener() {
 		    public void onLocationChanged(Location location) {
 		      // Called when a new location is found by the network location provider.
 		   	if(firsttake==true){
 		    	oldloc=location;
 		    	oldlocdistinc=location;
 		    firsttake=false;	
 		   	}
 		   	
 		    	double dist1= location.distanceTo(oldloc);
 		    	double dist2=location.distanceTo(oldloc);
 		    if(dist1>=500){
 		    	
 		    	ParseGeoPoint loc=new ParseGeoPoint(location.getLatitude(),location.getLongitude());
 		    user.put("current_location",loc );
 		    try {
				user.save();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 		    oldloc=location;
 		    }
 		 if(dist2>1000){
 			 
 			 //update trips
 			ParseQuery<ParseObject> query = ParseQuery.getQuery("UserTrips");
 			 query.whereEqualTo("trip_user", user);
 			 query.whereEqualTo("trip_onoff", true);
 		
 			 query.findInBackground(new FindCallback<ParseObject>() {
 				

 				@Override
 				public void done(List<ParseObject> obj, ParseException e) {
 					// TODO Auto-generated method stub
 					if(e==null){
 						for(int i=0;i<obj.size();i++){
 					int dist=Integer.parseInt(obj.get(i).getNumber("dist_covered").toString());
 					obj.get(i).put("dist_covered",dist+1 );
 					try {
						obj.get(i).save();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
 						}
 					}
 					}
 			 });
 			 
 			 //to add vehicle fuel remaining etc
 			 
 			 ParseQuery<ParseUser> query1 = ParseUser.getQuery();
 			query1.whereEqualTo("objectId", user.getObjectId());
 			
 			query1.include("current_vehicle");
 			query1.include("current_vehicle.vehicle_model");
 			query1.findInBackground(new FindCallback<ParseUser>() {
 				@Override
 			  public void done(List<ParseUser> objects, ParseException e) {
 			    if (e == null) {
 			
 			ParseObject obj= objects.get(0).getParseObject("current_vehicle");
 			 float mileage=Float.parseFloat(obj.getParseObject("vehicle_model").getNumber("kmpl_onroad").toString());
 			 float fuelremain=Float.parseFloat(obj.getNumber("fuel_remaining").toString());
 			 fuelremain-=1/mileage;
 			float total_dist_travel=Float.parseFloat(obj.getNumber("total_dist_travelled").toString());
 			 total_dist_travel+=1;
 			 obj.put("fuel_remaining", fuelremain);
 			 obj.put("total_dist_travelled", total_dist_travel);
 			 try {
				obj.save();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 			    }}});
 			 oldlocdistinc=location;
 			 	
 		 }
 	
 		    }
 		   @Override
 		    public void onStatusChanged(String provider, int status, Bundle extras) {}
 		  @Override
 		    public void onProviderEnabled(String provider) {}
 		 @Override
 		    public void onProviderDisabled(String provider) {}
 		  };

 		// Register the listener with the Location Manager to receive location updates
 		 if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
			 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 0, locationListener);
		  if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
		  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
		
        return START_STICKY;
    }

  
    @Override
    public IBinder onBind(Intent intent) {
        // There are Bound an Unbound Services - you should read something about
        // that. This one is an Unbounded Service.
        return null;
    }

   
    
    @Override
	  public void onDestroy() {
	    super.onDestroy();
	   
	  }

}