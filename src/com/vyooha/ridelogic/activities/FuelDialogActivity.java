package com.vyooha.ridelogic.activities;

import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FuelDialogActivity extends Activity{
	TextView fuelfilled,fuelrs,calc_dist;
	LinearLayout add_fuel_visible;
	EditText edit_add,edit_addcost;
	Button modifyfuel,reset,add;
	ParseUser user;
//	SharedPreferences prefs;
//	SharedPreferences.Editor editor;
	ImageView close;
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	setContentView(R.layout.fuel);
	
	user=ParseUser.getCurrentUser();
	
	 fuelfilled=(TextView)findViewById(R.id.fuelfilled_value);
	 fuelrs=(TextView)findViewById(R.id.fuelRS_value);
	 calc_dist=(TextView)findViewById(R.id.calc_distance_value);
	 add_fuel_visible=(LinearLayout)findViewById(R.id.addfuel_linear);
	 edit_add=(EditText)findViewById(R.id.edit_add_fuel);
	 edit_addcost=(EditText)findViewById(R.id.edit_add_fuelcost);
	 modifyfuel=(Button)findViewById(R.id.modify_fuel);
	 reset=(Button)findViewById(R.id.reset_fuel_btn);
	 add=(Button)findViewById(R.id.add_fuel);
	 
	 close=(ImageView)findViewById(R.id.close_img);
	 
	
	
	 ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("objectId", user.getObjectId());
		
		query.include("current_vehicle");
		query.include("current_vehicle.vehicle_model");
		query.findInBackground(new FindCallback<ParseUser>() {
			@Override
		  public void done(List<ParseUser> objects, ParseException e) {
		    if (e == null) {
	 
	ParseObject vehicle=objects.get(0).getParseObject("current_vehicle");
	float  remain_fuel=Float.parseFloat(vehicle.getNumber("fuel_remaining").toString());
	float mileage= Float.parseFloat(vehicle.getParseObject("vehicle_model").getNumber("kmpl_onroad").toString());
	int k=(int)(mileage*remain_fuel);
	setvalues(0, 0,k );
		   
		    
		    
		    
		    }}});
		
		
	reset.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			add.setVisibility(View.VISIBLE);
			resetfuel();
			edit_add.setText("");
			edit_addcost.setText("");
			add_fuel_visible.setVisibility(View.GONE);
			InputMethodManager imm = (InputMethodManager)getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edit_add.getWindowToken(), 0);
		}
	});
	add.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			add_fuel_visible.setVisibility(View.VISIBLE);
			
			InputMethodManager imm = (InputMethodManager)getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(edit_add, 0);
			add.setVisibility(View.GONE);
			
		}
	});
	modifyfuel.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			  InputMethodManager imm = (InputMethodManager)getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edit_add.getWindowToken(), 0);
			String i=edit_add.getText().toString();
			int litre=Integer.parseInt(i);
			i=edit_addcost.getText().toString();
			int cost=Integer.parseInt(i);;
			addfuel(litre, cost);

			edit_add.setText("");
			add.setVisibility(View.VISIBLE);
			add_fuel_visible.setVisibility(View.GONE);
			
				
		}
	});
	
	
	close.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			finish();
		}
	});
	
	
	 }

	 public void setvalues(int f,int r,int k){
		 fuelfilled.setText(Integer.toString(f)+" L");
		 fuelrs.setText(Integer.toString(r)+" RS");
		 calc_dist.setText(Integer.toString(k)+" KM");
		 
	 }
	 
	 public void addfuel(final float litre,final float cost){
		 
	
		 
		 ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("objectId", user.getObjectId());
			
			query.include("current_vehicle");
			query.include("current_vehicle.vehicle_model");
			query.findInBackground(new FindCallback<ParseUser>() {
						@Override
					  public void done(List<ParseUser> objects, ParseException e) {
					    if (e == null) {
				
				ParseObject obj= objects.get(0).getParseObject("current_vehicle");
				float  remain_ltr= Float.parseFloat(obj.getNumber("fuel_remaining").toString());
				float total_fuel=Float.parseFloat(obj.getNumber("total_fuel_filled").toString());
				remain_ltr+=litre;
				total_fuel+=litre;
				obj.put("fuel_remaining",remain_ltr );
				obj.put("total_fuel_filled", total_fuel);
				try {
					obj.save();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				  
				ParseObject FuelFillings=new ParseObject("FuelFillings");
				FuelFillings.put("cost", cost);
				FuelFillings.put("litres", litre);
				FuelFillings.put("amount", cost*litre);
				FuelFillings.put("user_obj",user);
				FuelFillings.put("vehicle_obj", obj);
				try {
					FuelFillings.save();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		
			 }
		}});
		
			 ParseQuery<ParseUser> query1 = ParseUser.getQuery();
				query1.whereEqualTo("objectId", user.getObjectId());
				
				query1.include("current_vehicle");
				query1.include("current_vehicle.vehicle_model");
				query1.findInBackground(new FindCallback<ParseUser>() {
					@Override
				  public void done(List<ParseUser> objects, ParseException e) {
				    if (e == null) {
			 
			ParseObject vehicle=objects.get(0).getParseObject("current_vehicle");
			float  remain_fuel=Float.parseFloat(vehicle.getNumber("fuel_remaining").toString());
			float mileage= Float.parseFloat(vehicle.getParseObject("vehicle_model").getNumber("kmpl_onroad").toString());
			int k=(int)(mileage*remain_fuel);
			setvalues(0, 0,k );
				   
				    
				    
				    
				    }}});
				
		 
	 }
	 
	 public void resetfuel(){
		 user=ParseUser.getCurrentUser();
		 
		 ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("objectId", user.getObjectId());
			
			query.include("current_vehicle");
			query.include("current_vehicle.vehicle_model");
			query.findInBackground(new FindCallback<ParseUser>() {
				@Override
			  public void done(List<ParseUser> objects, ParseException e) {
			    if (e == null) {
		
		 ParseObject obj= objects.get(0).getParseObject("current_vehicle");
		 obj.put("fuel_remaining", 0);
		 obj.put("total_dist_travelled", 0);
		 obj.put("total_fuel_filled", 0);
		 obj.put("total_time_onApp", 0);
		 try {
			obj.save();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 setvalues(0, 0, 0);
			    }}});
	 }
	 
	 
}
