package com.vyooha.ridelogic.activities;

import java.util.List;

import org.json.JSONObject;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class TripDialogActivity extends Activity{
	
	private TripClass trip;
	MyDB db;
	private ParseUser user;
	LinearLayout linear;
	Button addnewtrip;
	EditText editadd;
	ArrayAdapter<TripClass> tripAdapter;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
	    
	     
	     
	     db=new MyDB(getApplicationContext());
	     Log.d("jhhjh", "aftter  db");
	     ParseApplication appState = ((ParseApplication)getApplicationContext());
	     user = appState.user;
	     Log.d("jhhjh", "aftter  parse user");
	     tripAdapter = new ArrayAdapter<TripClass>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.trip_item, null);

	                     final TripClass cont = this.getItem(position);
	                      try{
	             
	               

	                  final TextView name = (TextView)convertView.findViewById(R.id.tripname);
	                  name.setText(cont.tripname);
	                  final ToggleButton trip_on_off=(ToggleButton)convertView.findViewById(R.id.trip_on_off);
	                   trip_on_off.setChecked(cont.triponoff);
	                  ImageButton removetrip=(ImageButton)convertView.findViewById(R.id.removetrip);
	                  final TextView tripdist=(TextView)convertView.findViewById(R.id.trip_km);
	                  if(cont.triponoff)
	                	  tripdist.setTextColor(Color.parseColor("#FED231"));
	                  else
	              		tripdist.setTextColor(Color.parseColor("#666666"));
	                  tripdist.setText(Integer.toString(cont.tripdist)+" KM");
	                  trip_on_off.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						//	db.updatetrip(user.getUsername(),cont.tripname,trip_on_off.isChecked());
							 ParseQuery<ParseObject> query = ParseQuery.getQuery("UserTrips");
							 query.whereEqualTo("trip_user", user);
							 query.whereEqualTo("trip_name", cont.tripname);
							// query.fromLocalDatastore();
							 query.findInBackground(new FindCallback<ParseObject>() {
								

								@Override
								public void done(List<ParseObject> obj, ParseException e) {
									// TODO Auto-generated method stub
									if(e==null){
										for(int i=0;i<obj.size();i++){
									obj.get(i).put("trip_onoff", trip_on_off.isChecked());
									try {
										obj.get(i).save();
									} catch (ParseException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
										}
									}
									}
							 });
							
							
							if(trip_on_off.isChecked()==true){
								tripdist.setTextColor(Color.parseColor("#FED231"));
							}
							else
								tripdist.setTextColor(Color.parseColor("#666666"));
								clearadapter();
								settrips();
							//db.close();
						}
					});
                   
	                  
	                  removetrip.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
				//				db.deleterow(user.getUsername(), cont.tripname);
								
								 ParseQuery<ParseObject> query = ParseQuery.getQuery("UserTrips");
								 query.whereEqualTo("trip_user", user);
								 query.whereEqualTo("trip_name", cont.tripname);
								// query.fromLocalDatastore();
								 query.findInBackground(new FindCallback<ParseObject>() {
									

									@Override
									public void done(List<ParseObject> obj, ParseException e) {
										// TODO Auto-generated method stub
										if(e==null){
											for(int i=0;i<obj.size();i++){
										try {
											obj.get(i).delete();
										} catch (ParseException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
											}
										}
										}
								 });
								 	
								
								clearadapter();
								settrips();
								//db.close();
							}
						});
              
	                }
	                      catch (Exception e){
	                    	  
	                    	  
	                    	  Log.e("hrhjh", e.toString());
	                      }
	                      
	            
	                return convertView;
	            }
	        };


	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);  
	setContentView(R.layout.tripdialog);
	
	Log.d("jhhjh", "aftter  content layout");
	ImageView close=(ImageView)findViewById(R.id.close_img);
	Button addtrip=(Button)findViewById(R.id.add_trip_btn);
	Button resettrip=(Button)findViewById(R.id.reset_trip_btn);
	
	 linear=(LinearLayout)findViewById(R.id.linear_add_trip);
	 addnewtrip=(Button)findViewById(R.id.add_new_trip);
	 editadd=(EditText)findViewById(R.id.edit_add_trip);
	
	
	 close.setOnClickListener(new OnClickListener()
		 {
			 

			@Override
			public void onClick(View v) {
				finish();
			}
			});
	 Log.d("jhhjh", "aftter  close");
	 addtrip.setOnClickListener(new OnClickListener()
	 {
		 

		@Override
		public void onClick(View v) {
			linear.setVisibility(View.VISIBLE);
			
		}
		});
	 
	 addnewtrip.setOnClickListener(new OnClickListener()
	 {
		 

		@Override
		public void onClick(View v) {
			
			ParseObject  pobject=new ParseObject("UserTrips");
			pobject.put("trip_user", user);
			pobject.put("trip_name", editadd.getText().toString());
			pobject.put("dist_covered", 0);
			pobject.put("trip_onoff", true);
		//	pobject.pinInBackground();
			try {
				pobject.save();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
	//	db.createRecords(user.getUsername(), editadd.getText().toString(), 0);
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		editadd.setText("");
		Toast.makeText(getApplicationContext(), "Successfully added..!", Toast.LENGTH_LONG).show();
		imm.hideSoftInputFromWindow(editadd.getWindowToken(), 0);
		linear.setVisibility(View.GONE);
		clearadapter();
		settrips();
		
	
		}
		});
	 
	 
	 resettrip.setOnClickListener(new OnClickListener()
	 {
		 

		@Override
		public void onClick(View v) {
			Toast.makeText(getApplicationContext(), "Successfully reset..!", Toast.LENGTH_LONG).show();
			linear.setVisibility(View.GONE);
			resetDistanceTripOn();
			clearadapter();
			settrips();
			
		}
		});
	 
	
ListView list=(ListView)findViewById(R.id.trip_listview);
list.setAdapter(tripAdapter);

settrips();



	     }
	    


	 
	 private void settrips()
	 {
		
		 
		 ParseQuery<ParseObject> query = ParseQuery.getQuery("UserTrips");
		 query.whereEqualTo("trip_user", user);
		// query.fromLocalDatastore();
		 query.findInBackground(new FindCallback<ParseObject>() {
			

			@Override
			public void done(List<ParseObject> obj, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					for(int i=0;i<obj.size();i++){
				trip=new TripClass();
				
				trip.tripname=obj.get(i).getString("trip_name");
			 	trip.tripdist=Integer.parseInt(obj.get(i).getNumber("dist_covered").toString());
			 	trip.triponoff=obj.get(i).getBoolean("trip_onoff");
				tripAdapter.add(trip);
					}
				}
				}
		 });
		 	
		 

//		 }
//db.close();

	 }
	 private void clearadapter()
	 {
		 tripAdapter.clear();
	 }
	 private void resetDistanceTripOn(){
//		 Cursor c=db.selectRecordOnTripOn(user.getUsername(),true);
//
//
//		 Log.d("dbsize", Integer.toString(db.size()));
//
//		 for(int i=0;i<c.getCount();i++){
//		 	c.moveToPosition(i);
//		 	db.resettrip(user.getUsername(),c.getString(1), 0);
//		 	Log.d("head"+i, c.getString(1));
//		 	Log.d("body"+i, c.getString(2));
//		 	
		 	//c.moveToPrevious();
		 	
		 	//tripAdapter.add(trip);

//		 }
		 
		// db.close();
		 
		 ParseQuery<ParseObject> query = ParseQuery.getQuery("UserTrips");
		 query.whereEqualTo("trip_user", user);
		 query.whereEqualTo("trip_onoff", true);
		// query.fromLocalDatastore();
		 query.findInBackground(new FindCallback<ParseObject>() {
			

			@Override
			public void done(List<ParseObject> obj, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					for(int i=0;i<obj.size();i++){
				obj.get(i).put("dist_covered", 0);
				try {
					obj.get(i).save();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					}
				}
				}
		 });
		 	
		 
		 
		 
		 
		 
	 }
	 
	 @Override
	    protected void onDestroy() {
	        super.onDestroy();
	       db.close();
	    }
	 
}

