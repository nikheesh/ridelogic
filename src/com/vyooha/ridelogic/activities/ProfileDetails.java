package com.vyooha.ridelogic.activities;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ProfileDetails extends Activity{
	//private  TextView vehicle_model,prof_name,km_travelled,mileage,reg_no;
	boolean choose_vehicle;
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Signing in. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	private  TextView vehicle_model,prof_name,km_travelled,mileage,reg_no;
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
			requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
	        setContentView(R.layout.profiledetails);
	        
	        choose_vehicle=false;
	       final ParseUser user=ParseUser.getCurrentUser();
	        prof_name=(TextView)findViewById(R.id.profilename);
	        km_travelled=(TextView)findViewById(R.id.kmtravelled);
	        mileage=(TextView)findViewById(R.id.comp_spec_mileage);
	        reg_no=(TextView)findViewById(R.id.regno);
	        vehicle_model=(TextView)findViewById(R.id.vehiclemodel);
	        
	       ImageView sel_model=(ImageView)findViewById(R.id.modelimage);
	       ImageView create_prof=(ImageView)findViewById(R.id.create_botton);
	       ImageView back=(ImageView)findViewById(R.id.left_arrow);
	       prof_name.setText(user.getString("name"));
	       if(user.has("current_vehicle")){
	    	//   vehicle_model.setText(user.getString("model"));
	    	   ParseQuery<ParseUser> query1 = ParseUser.getQuery();
	 			query1.whereEqualTo("objectId", user.getObjectId());
	 			
	 			query1.include("current_vehicle");
	 			query1.include("current_vehicle.vehicle_model");
	 			query1.findInBackground(new FindCallback<ParseUser>() {
	 				@Override
	 			  public void done(List<ParseUser> objects, ParseException e) {
	 			    if (e == null) {
	 			
	 			ParseObject obj= objects.get(0).getParseObject("current_vehicle");
	 			vehicle_model.setText( obj.getParseObject("vehicle_model").getString("company").toString()+" "+obj.getParseObject("vehicle_model").getString("model_name").toString());
	 			 km_travelled.setText(obj.getNumber("total_dist_travelled").toString()+" KM");
	 			 mileage.setText(obj.getParseObject("vehicle_model").getNumber("kmpl_onroad").toString()+" kmpl");
	 			 reg_no.setText(obj.getString("vehicle_regno").toString());
	 			    }}});
	    	   
	    	   
	    	   
	    	   
	    	   
	    	   
	       }
	       
	       
	        choose_vehicle=getIntent().getExtras().getBoolean("choose");
	       if(choose_vehicle){
	       String vehiclemodelstring=getIntent().getExtras().getString("model");
	       vehicle_model.setText(vehiclemodelstring);
	       }
	       sel_model.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(),VehicleListActivity.class);
// 					i.putExtra("choose", false);
 				startActivity(i);
// 				stopLoading();
				}
			});

	        
	       create_prof.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startLoading();
					user.put("model", vehicle_model.getText().toString());
					user.saveInBackground(new SaveCallback(){

						@Override
						public void done(ParseException e) {
							if(e == null){
								
								stopLoading();
								Toast.makeText(getApplicationContext(), "Successfully created Profile..!", Toast.LENGTH_LONG).show();
								Intent i=new Intent(getApplicationContext(),MainMenuActivity.class);
//			 					i.putExtra("choose", false);
			 				startActivity(i);
							
							}}
						
					
					
					});
					
				}
			});
	       
	       
	       back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	       
	}
}
