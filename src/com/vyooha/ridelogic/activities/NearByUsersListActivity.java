package com.vyooha.ridelogic.activities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vyooha.ridelogic.views.RoundedImageView;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NearByUsersListActivity extends Activity{
	 ArrayAdapter<SocialClass> contactAdapter;
//   private JSONObject json;
//   private JSONArray jsonarray;
   ListView list;
   EditText search;
   SocialClass contclass;
   int total_contacts;
   

	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     total_contacts=0;
	     contactAdapter = new ArrayAdapter<SocialClass>(this, 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getLayoutInflater().inflate(R.layout.nearcontactitem, null);

	                   final   SocialClass cont = this.getItem(position);
	                      try{
	             
	                RoundedImageView imageView = (RoundedImageView)convertView.findViewById(R.id.nearby_cont_img);
	              final ImageView imgtick=(ImageView)convertView.findViewById(R.id.nearvisibility);
	                

	                  TextView name = (TextView)convertView.findViewById(R.id.nearby_cont_name);
	                  name.setText(cont.name);
	                  
	                
	                
	                if(cont.photo!=null){
	                	imageView.setImageBitmap(cont.photo);
	                	Log.e("imagesetted", "hi");
	                }
	                else
	                {
	                	imageView.setImageResource(R.drawable.def_contact);
	                }
	                
	                if(cont.isvisibleNearBy){
	                	imgtick.setImageResource(R.drawable.near_visible);
	                }
	                else
	                {
	                	imgtick.setImageResource(R.drawable.near_notvisible);
	                }
	                
	                RelativeLayout layout=(RelativeLayout)convertView.findViewById(R.id.layoutcontact);
	                  
	                  layout.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								if(!cont.isvisibleNearBy){
									cont.isvisibleNearBy=true;
									imgtick.setImageResource(R.drawable.near_visible);
								}
								else{
									cont.isvisibleNearBy=false;
									imgtick.setImageResource(R.drawable.near_notvisible);
								}
													
							}
						});
	                
//	                imgtick.setOnClickListener(new OnClickListener() {
//						
//						@Override
//						public void onClick(View v) {
//							// TODO Auto-generated method stub
//							
//						}
//					});
	                
	                
	                
	                
	                      }
	                      catch (Exception e){}
	                      
	            //    text.setText(tweet.get("mdesc").getAsString());
	                return convertView;
	            }
	        };
	     
	     
	        setContentView(R.layout.nearcontactslist);
	        list=(ListView)findViewById(R.id.nearbyusers_listview);
	        list.setAdapter(contactAdapter);
	        
	        try {
				ParseObject.unpinAll("allnearby");
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	        ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

	   	 query.fromLocalDatastore();
	   	 query.whereEqualTo("isRideLogik", true);
	   	 query.findInBackground(new FindCallback<ParseObject>() {
	   		

	   		@Override
	   		public void done(List<ParseObject> obj, ParseException e) {
	   			// TODO Auto-generated method stub
	   			if(e==null){
	   				if(obj.size()>0){
	   					Log.e("loading from parse", "=================");
	   					total_contacts=obj.size();
	   		for(int i=0;i<obj.size();i++){
	   			
	   			contclass=new SocialClass();
	   			contclass.name=obj.get(i).getString("name");
	   			contclass.phonenumber=obj.get(i).getString("phone");
	   			contclass.isRideLogik=obj.get(i).getBoolean("isRideLogik");
	   			contclass.isvisibleNearBy=false;
	   			Log.d("CUSTOM NEW",contclass.name+",  "+contclass.phonenumber+" , "+contclass.isRideLogik);
	   			
	   			if(obj.get(i).has("profile_pic")){
	   				ParseFile pf=obj.get(i).getParseFile("profile_pic");
	   			if(pf!=null){
	   			byte[] bitmapdata;
	   			try {
	   				bitmapdata = pf.getData();
	   				Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
	   				contclass.photo=bitmap;
	   			} catch (ParseException e1) {
	   				// TODO Auto-generated catch block
	   				e1.printStackTrace();
	   			}
	   			}
	   			}
	   			contactAdapter.add(contclass);
	   			
	   				}
	   				
	   				}
	   				
	   				
	   				
	   			}
	   			
	   			
	   			}
	   	 });
	   	 	
	    ImageView done_nearby=(ImageView)findViewById(R.id.done_nearby_botton);
	    done_nearby.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<ParseObject> list_nearby=new ArrayList<ParseObject>();
				
				for(int i=0;i<contactAdapter.getCount();i++){
					SocialClass cont=contactAdapter.getItem(i);
					if(cont.isvisibleNearBy){
						ParseObject nearby_contact=new ParseObject("NearByContact");
						nearby_contact.put("name", cont.name);
			    		nearby_contact.put("phone", cont.phonenumber);
			    		nearby_contact.put("isRideLogik", cont.isRideLogik);
			    		nearby_contact.put("isVisibleNearBy", cont.isvisibleNearBy);
			    		if(cont.photo!=null)
			    		{Bitmap bit=cont.photo;
			    			ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bit.compress(Bitmap.CompressFormat.PNG, 100, stream);
							//byte[] byteArray 
							byte[] array= stream.toByteArray();
			    			
			    			
			    			ParseFile pf=new ParseFile("img.png", array);
			    			try {
								pf.save();
								nearby_contact.put("profile_pic",pf);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			    		
			    		
			    		}
			    		list_nearby.add(nearby_contact);
					}
				}
				
				
				try {ParseObject.unpinAll("allnearby");
					ParseObject.pinAll("allnearby",list_nearby);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				Intent i=new Intent(getApplicationContext(),NearByMapActivity.class);
		i.putExtra("total_contant", total_contacts);
				startActivity(i);
				
				
			}
		});
	     
	 }
   
}
