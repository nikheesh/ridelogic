package com.vyooha.ridelogic.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;

public class PlanATrip_Get2  extends Fragment{
	/*
	
	String plantrip_id;
	
	 SupportMapFragment fragment;
	GoogleMap googleMap;
	 Marker marker;

   final int PLACES=0;
   final int PLACES_DETAILS=1;
   
  
	ArrayList<String> list_array;
	ArrayList<ParseGeoPoint> listwaypoints;
	ArrayList<JSONObject> listwaypointsg;
  ListView listview_wapoint;
  ListView listView_invities;
  
	
   LatLng point;
   LatLng source,destination;
   JSONObject pointg;
  JSONObject sourceg,destinationg;
  Geocoder gcd;
  List<Address> addresses;
	
	 @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	       View contacts=inflater.inflate(R.layout.setroute, container, false); 
	       return contacts;
	    }  
	       
	       
	    @SuppressLint("SimpleDateFormat")
		@Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
	    plantrip_id= getArguments().getString("name");
			
	    FragmentManager fm = getFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.get_plannedtrip_map);
            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.get_plannedtrip_map, fragment).commit();
            }

            // Getting GoogleMap from SupportMapFragment
            googleMap = fragment.getMap();
            CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);

            // Showing the user input location in the Google Map
           
            googleMap.animateCamera(cameraZoom);
            googleMap.setMyLocationEnabled(true);
            
			 
			
		        // Setting an item click listener for the AutoCompleteTextView dropdown list
		       
		        
		        Button setsource=(Button)getActivity().findViewById(R.id.setsource);
		        Button setdest=(Button)getActivity().findViewById(R.id.setdest); 
		        Button setwaypoint=(Button)getActivity().findViewById(R.id.setwaypoint);
		        Button completion=(Button)getActivity().findViewById(R.id.finish);
		        Button drawroute=(Button)getActivity().findViewById(R.id.draw_route);
		        listwaypoints=new ArrayList<ParseGeoPoint>();
		        listwaypointsg=new ArrayList<JSONObject>();
		        sourceg=new JSONObject();
		        destinationg =new JSONObject();
		        pointg=new JSONObject();
		        gcd = new Geocoder(getActivity(), Locale.getDefault());
		        setsource.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(googleMap!=null){
							//source=new ParseGeoPoint(point.latitude, point.longitude);
							 // point = new LatLng(latitude, longitude);
							point=googleMap.getCameraPosition().target;
							
				                 Log.e("lat long",point.toString());
				                 
				               // CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(point);
//				                CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);
//				 
//				                // Showing the user input location in the Google Map
//				               
//				                googleMap.animateCamera(cameraZoom);
				              //  googleMap.moveCamera(cameraPosition);
							
							
							
							source=point;
							try {
								sourceg.put("lat",point.latitude);
								sourceg.put("lng",point.longitude);
								
//								addresses = gcd.getFromLocation(point.latitude, point.longitude, 1);
//								if (addresses.size() > 0) 
									sourceg.put("place_name",getAddress(point.latitude, point.longitude));
								
								
								//sourceg.put("place_name","Perinthalmanna");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
					
							 MarkerOptions options = new MarkerOptions();
				                options.position(point);
				                options.title("starting point");
				               // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
				 
				                // Adding the marker in the Google Map
				           
				               googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				              
							
							//marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
							Log.e("geopoint in source",source.toString());
						}
						
					}
				});
		        
setdest.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(googleMap!=null){
							
							point=googleMap.getCameraPosition().target;
			                 Log.e("lat long",point.toString());
			                 
			               // CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(point);
//			                CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);
//			 
//			                // Showing the user input location in the Google Map
//			               
//			                googleMap.animateCamera(cameraZoom);
			              //  googleMap.moveCamera(cameraPosition);
						
						
						
						destination=point;
						try {
							destinationg.put("lat",point.latitude);
							destinationg.put("lng",point.longitude);
							
//							addresses = gcd.getFromLocation(point.latitude, point.longitude, 1);
//							if (addresses.size() > 0) 
								destinationg.put("place_name",getAddress(point.latitude, point.longitude));
								Log.e("place",getAddress(point.latitude, point.longitude));
							
						//	destinationg.put("place_name","Pandikkad");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						
						
						 MarkerOptions options = new MarkerOptions();
			                options.position(point);
			                options.title("destination point");
			               // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
			 
			                // Adding the marker in the Google Map
			           
			               googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
			              
						
							Log.e("geopoint in dest",destination.toString());
						
						}
						
						
						
					}
				});
setwaypoint.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			if(googleMap!=null){
				
				//marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
				point=googleMap.getCameraPosition().target;
				
				try {
					pointg.put("lat",point.latitude);
					pointg.put("lng",point.longitude);
//					addresses = gcd.getFromLocation(point.latitude, point.longitude, 1);
//					if (addresses.size() > 0) 
						pointg.put("place_name",getAddress(point.latitude, point.longitude));
					
					
					//pointg.put("place_name","palakkd");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
               Log.e("lat long",point.toString());
               
             // CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(point);
             
            //  googleMap.moveCamera(cameraPosition);
			
			if(listwaypointsg.size()<=6){
			
		JSONObject points=new JSONObject();
			points=pointg;
			listwaypoints.add(new ParseGeoPoint(point.latitude, point.longitude));
              listwaypointsg.add(points);
			
			 MarkerOptions options = new MarkerOptions();
              options.position(point);
              options.title("way point");
             // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);

              // Adding the marker in the Google Map
         
             googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
			}
			else{
				Toast.makeText(getActivity(),"Maximum 6 way points allowed", Toast.LENGTH_LONG).show();
			}
			
				Log.e("geopoint in list",listwaypoints.toString());
			
			}
		}
	});

		completion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if((source==null )||(destination==null)||listwaypointsg.size()==0)
				{
					Toast.makeText(getActivity(), "set all points for trip", Toast.LENGTH_LONG).show();
				}
				else{
//					
//					ParseObject plantrip=new ParseObject("PlannedTrips");
//					
//					
//					plantrip.put("created_by", ParseUser.getCurrentUser());
//					plantrip.put("destinationLat", destination.latitude);
//					plantrip.put("destinationLng", destination.longitude);
//					plantrip.put("sourceLat", source.latitude);
//					plantrip.put("sourceLng", source.longitude);
//					plantrip.put("trip_name",name);
//					SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
//				
//				 
//					try {
//				 
//						Date date1 = formatter.parse(date);
//
//					
//					plantrip.put("trip_start_date", date1);
//					}
//					catch(Exception e){}
//					
//					plantrip.put("waypoints", listwaypoints);
//					plantrip.put("participants_objidArray", list_array);
//					plantrip.saveEventually();
					
				
					
					Map<String, Object> input_param = new HashMap<String, Object>();
					
					Log.d("geopointissue", sourceg.toString());
				
					input_param.put("source", sourceg);
					
					input_param.put("destination", destinationg);
					input_param.put("waypoints", listwaypointsg);
					JSONObject oj = new JSONObject();
					JSONArray jsa = new JSONArray();
					
					try {
						jsa.put(0, sourceg);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					input_param.put("invited_users", list_array);
					SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
					
					 
						try {
					 
							Date date1 = formatter.parse(date);
							Log.e("inside date methode", date1.toString());
						
						input_param.put("date", date1);
						
						}
						catch(Exception e){}
					
						input_param.put("tripname", name);
					ParseCloud.callFunctionInBackground("createPTrip", input_param, new FunctionCallback<Map<String,Object>>() {
					

						@Override
						public void done(Map<String,Object> result, ParseException e) {
							// TODO Auto-generated method stub
							 if (e == null) {
							      // result is "Hello world!"
								 Toast.makeText(getActivity(), "successfully", Toast.LENGTH_LONG).show();
							    }
						}
						});

					
//					Toast.makeText(getActivity(), "Successfully set the trip", Toast.LENGTH_LONG).show();
//					Intent i=new Intent(getActivity().getApplicationContext(), MainMenuActivity.class);
//					startActivity(i);
					
					
				}
				
				
				
				
				
			}
		});        
	        
		drawroute.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if((source==null )||(destination==null)||listwaypoints.size()==0)
				{
					Toast.makeText(getActivity(), "set all points for trip", Toast.LENGTH_LONG).show();
				}
				else{
					
					
					googleMap.clear();
					
					 MarkerOptions options = new MarkerOptions();
		                options.position(source);
		                options.title("starting point");
		               // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
		 
		                // Adding the marker in the Google Map
		           
		               googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		              
					options.position(destination);
	                options.title("destination point");
	                googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
		              
	                for(int i=0;i<listwaypoints.size();i++){
	                	LatLng waypoint=new LatLng(listwaypoints.get(i).getLatitude(), listwaypoints.get(i).getLongitude());
	                options.position(waypoint);
	                options.title("way point");
	                googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
	                }  
					
								}
				
			}
		});
		
		
	    }
	    
	    private String getAddress(double latitude, double longitude) {
	        StringBuilder result = new StringBuilder();
	        try {
	            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
	            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
	            if (addresses.size() > 0) {
	                Address address = addresses.get(0);
	                result.append(address.getLocality()).append("\n");
	                result.append(address.getCountryName());
	            }
	        } catch (IOException e) {
	            Log.e("tag", e.getMessage());
	        }

	        return result.toString();
	    }
	    
		 
*/

}
