package com.vyooha.ridelogic.activities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.vyooha.ridelogic.views.RoundedImageView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PlanATrip2 extends Fragment{
	
	 ArrayAdapter<SocialClass> contactAdapter;
	   //   private JSONObject json;
	   //   private JSONArray jsonarray;
	      ListView list;
	      EditText search;
	      SocialClass contclass;
	
	 @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	       View contacts=inflater.inflate(R.layout.plan_a_trip2, container, false); 
	       return contacts;
	    }  
	       
	       
	    @Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
	        
	        this.getView().setFocusableInTouchMode(true);

	        this.getView().setOnKeyListener( new OnKeyListener()
	        {
	            @Override
	            public boolean onKey( View v, int keyCode, KeyEvent event )
	            {
	                if( keyCode == KeyEvent.KEYCODE_BACK )
	                {
	                    return false;
	                }
	                return false;
	            }
	        } );
	        
	        ImageView back=(ImageView) getActivity().findViewById(R.id.left_arrow);
	        back.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					PlanATrip1 ptrip1=new PlanATrip1();
					
					 getFragmentManager().beginTransaction()
					  .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
				        .replace(R.id.fragment1, ptrip1 )
				        .addToBackStack("tag2")

				        .commit();
				}
			});
	        
	        contactAdapter = new ArrayAdapter<SocialClass>(getActivity(), 0) {
	            @Override
	            public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null)
	                    convertView = getActivity().getLayoutInflater().inflate(R.layout.nearcontactitem, null);

	                   final   SocialClass cont = this.getItem(position);
	                      try{
	             
	                RoundedImageView imageView = (RoundedImageView)convertView.findViewById(R.id.nearby_cont_img);
	              final ImageView imgtick=(ImageView)convertView.findViewById(R.id.nearvisibility);
	                

	                  TextView name = (TextView)convertView.findViewById(R.id.nearby_cont_name);
	                  name.setText(cont.name);
	                  
	                
	                
	                if(cont.photo!=null){
	                	imageView.setImageBitmap(cont.photo);
	                	Log.e("imagesetted", "hi");
	                }
	                else
	                {
	                	imageView.setImageResource(R.drawable.def_contact);
	                }
	                
	                if(cont.isvisibleNearBy){
	                	imgtick.setImageResource(R.drawable.near_visible);
	                }
	                else
	                {
	                	imgtick.setImageResource(R.drawable.near_notvisible);
	                }
	                
	                RelativeLayout layout=(RelativeLayout)convertView.findViewById(R.id.layoutcontact);
	                  
	                  layout.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								if(!cont.isvisibleNearBy){
									cont.isvisibleNearBy=true;
									imgtick.setImageResource(R.drawable.near_visible);
								}
								else{
									cont.isvisibleNearBy=false;
									imgtick.setImageResource(R.drawable.near_notvisible);
								}
													
							}
						});
	                
//	                imgtick.setOnClickListener(new OnClickListener() {
//						
//						@Override
//						public void onClick(View v) {
//							// TODO Auto-generated method stub
//						
//						}
//					});
//	                
	                
	                
	                
	                      }
	                      catch (Exception e){}
	                      
	            //    text.setText(tweet.get("mdesc").getAsString());
	                return convertView;
	            }
	        };
	     
	     
	        //setContentView(R.layout.nearcontactslist);
	        list=(ListView)getActivity().findViewById(R.id.participants_listview);
	        list.setAdapter(contactAdapter);
	        
	        try {
				ParseObject.unpinAll("allnearby");
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	        ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

	   	 query.fromLocalDatastore();
	   	 query.whereEqualTo("isRideLogik", true);
	   	 query.findInBackground(new FindCallback<ParseObject>() {
	   		

	   		@Override
	   		public void done(List<ParseObject> obj, ParseException e) {
	   			// TODO Auto-generated method stub
	   			if(e==null){
	   				if(obj.size()>0){
	   					Log.e("loading from parse", "=================");
	   					
	   		for(int i=0;i<obj.size();i++){
	   			
	   			contclass=new SocialClass();
	   			contclass.name=obj.get(i).getString("name");
	   			contclass.phonenumber=obj.get(i).getString("phone");
	   			contclass.isRideLogik=obj.get(i).getBoolean("isRideLogik");
	   			contclass.obid=obj.get(i).getString("obid");
	   			contclass.isvisibleNearBy=false;
	   			Log.d("CUSTOM NEW",contclass.name+",  "+contclass.phonenumber+" , "+contclass.isRideLogik);
	   			
	   			if(obj.get(i).has("profile_pic")){
	   				ParseFile pf=obj.get(i).getParseFile("profile_pic");
	   			if(pf!=null){
	   			byte[] bitmapdata;
	   			try {
	   				bitmapdata = pf.getData();
	   				Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
	   				contclass.photo=bitmap;
	   			} catch (ParseException e1) {
	   				// TODO Auto-generated catch block
	   				e1.printStackTrace();
	   			}
	   			}
	   			}
	   			contactAdapter.add(contclass);
	   			
	   				}
	   				
	   				}
	   				
	   				
	   				
	   			}
	   			
	   			
	   			}
	   	 });
	   	 	
	    Button done_nearby=(Button)getActivity().findViewById(R.id.Next2);
	    done_nearby.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<String> list_array=new ArrayList<String>();
				
				for(int i=0;i<contactAdapter.getCount();i++){
					SocialClass cont=contactAdapter.getItem(i);
					if(cont.isvisibleNearBy){
						
						list_array.add(cont.obid);
						
						
					}
				}
				
				
				Bundle bundle2=new Bundle();
				
				 String tripname=getArguments().getString("name");
				 String tripdate=getArguments().getString("date");
				 bundle2.putString("name", tripname);
				 bundle2.putString("date", tripdate);
				 bundle2.putStringArrayList("participantsid", list_array);
				 
				PlanATrip3 ptrip3=new PlanATrip3();
				ptrip3.setArguments(bundle2);
				 getFragmentManager().beginTransaction()
				  .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
			        .replace(R.id.fragment1, ptrip3 )
			        .addToBackStack("tag2")

			        .commit();
				
			}
		});
	     
	 }
	        
	   
}
