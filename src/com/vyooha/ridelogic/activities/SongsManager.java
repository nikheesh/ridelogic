package com.vyooha.ridelogic.activities;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

public class SongsManager {
    
	// SDCard Path
	
    final String MEDIA_PATH=new String("/sdcard/");
    
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
 
    // Constructor
    public SongsManager(){
    	
    }
 
    /**
     * Function to read all mp3 files from sdcard
     * and store the details in ArrayList
     * */
    
    
    public ArrayList<HashMap<String, String>> getPlayList(Context context){
    
    	File home = new File(MEDIA_PATH);
    	
    	///vajid code changes - music player list - code section 001 begin
    	
    	
        Cursor c = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Media._ID,
                               MediaStore.Audio.Media.DATA,
                               MediaStore.Audio.Media.ARTIST,
                               MediaStore.Audio.Media.ALBUM,
                               MediaStore.Audio.Media.DISPLAY_NAME}, "1=1", null, null);
        
        
        Log.d("MUSIC >> MEDIASTORE",Integer.toString(c.getCount()));
        
        c.moveToFirst();
        
        do{
            
        	HashMap<String, String> song = new HashMap<String, String>();
        	song.put("songPath",c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)));
        	song.put("songTitle",c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)));
        	songsList.add(song);        	
        }
        
        while(c.moveToNext());
        
        c.close();
        
        /// - code section 001 ends
 
//        if (home.listFiles(new FileExtensionFilter()).length > 0) {
//            for (File file : home.listFiles(new FileExtensionFilter())) {
//                HashMap<String, String> song = new HashMap<String, String>();
//                song.put("songTitle", file.getName().substring(0, (file.getName().length() - 4)));
//                song.put("songPath", file.getPath());
// 
//                // Adding each song to SongList
//                songsList.add(song);
//            }
//        }
        // return songs list array
        
        return songsList;
    }
 
    
    /**
     * Class to filter files which are having .mp3 extension
     * */
    class FileExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".mp3") || name.endsWith(".MP3"));
        }
    }
}
