package com.vyooha.ridelogic.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class PlanATrip3 extends Fragment{
	
	 SupportMapFragment fragment;
	
	// private ArrayList<HashMap<Double, Double>> geopints = new ArrayList<HashMap<Double, Double>>();
	 
	AutoCompleteTextView atvPlaces;
	 
    DownloadTask placesDownloadTask;
    DownloadTask placeDetailsDownloadTask;
    ParserTask placesParserTask;
    ParserTask placeDetailsParserTask;
    boolean isDrawroute=false;
 
    GoogleMap googleMap;
 
    final int PLACES=0;
    final int PLACES_DETAILS=1;
    
    String name,date;
	ArrayList<String> list_array;
	ArrayList<ParseGeoPoint> listwaypoints;
	ArrayList<JSONObject> listwaypointsg;
    Marker marker;
	
    LatLng point;
    LatLng source,destination;
    JSONObject pointg;
   JSONObject sourceg,destinationg;
   Geocoder gcd;
   List<Address> addresses;
   
   Marker sourcemarker,destmarker;
	
	 @Override 
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
	            Bundle savedInstanceState) {  
	       View contacts=inflater.inflate(R.layout.plan_a_trip3, container, false); 
	       return contacts;
	    }  
	       
	       
	    @SuppressLint("SimpleDateFormat")
		@Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
	        
	        
	        this.getView().setFocusableInTouchMode(true);

	        this.getView().setOnKeyListener( new OnKeyListener()
	        {
	            @Override
	            public boolean onKey( View v, int keyCode, KeyEvent event )
	            {
	                if( keyCode == KeyEvent.KEYCODE_BACK )
	                {
	                    return false;
	                }
	                return false;
	            }
	        } );
	       
	    name= getArguments().getString("name");
			date= getArguments().getString("date");
			 ImageView back=(ImageView) getActivity().findViewById(R.id.left_arrow);
		        back.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						PlanATrip2 ptrip2=new PlanATrip2();
						Bundle bundle=new Bundle();
						 bundle.putString("name", name);
						 
						 bundle.putString("date", date);
				
						 ptrip2.setArguments(bundle);
						 getFragmentManager().beginTransaction()
						 .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
					        .replace(R.id.fragment1, ptrip2 )
					         .addToBackStack("tag1")
					        .commit();
					}
				});
		        
		        
		        
		        
			
			
			
			
			 list_array=getArguments().getStringArrayList("participantsid");
			 Log.e("bundle result","name  "+name+",date"+date+",participants ids : "+list_array.toString());
			 
			 FragmentManager fm = getFragmentManager();
             fragment = (SupportMapFragment) fm.findFragmentById(R.id.from_map);
             if (fragment == null) {
                 fragment = SupportMapFragment.newInstance();
                 fm.beginTransaction().replace(R.id.from_map, fragment).commit();
             }

             // Getting GoogleMap from SupportMapFragment
             googleMap = fragment.getMap();
             CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);

             // Showing the user input location in the Google Map
            
             googleMap.animateCamera(cameraZoom);
             googleMap.setMyLocationEnabled(true);
             
			 
			 atvPlaces = (AutoCompleteTextView) getActivity().findViewById(R.id.from_place);
		        atvPlaces.setThreshold(3);
		 
		        // Adding textchange listener
		        atvPlaces.addTextChangedListener(new TextWatcher() {
		 
		            @Override
		            public void onTextChanged(CharSequence s, int start, int before, int count) {
		                // Creating a DownloadTask to download Google Places matching "s"
		                placesDownloadTask = new DownloadTask(PLACES);
		 Log.e("inside listener after download tsk", s.toString());
		                // Getting url to the Google Places Autocomplete api
		                String url = getAutoCompleteUrl(s.toString());
		                Log.e("inside listener after auto complete", s.toString());
		               // Start downloading Google Places
		                // This causes to execute doInBackground() of DownloadTask class
		                placesDownloadTask.execute(url);
		            }
		 
		            @Override
		            public void beforeTextChanged(CharSequence s, int start, int count,
		            int after) {
		                // TODO Auto-generated method stub
		            }
		 
		            @Override
		            public void afterTextChanged(Editable s) {
		                // TODO Auto-generated method stub
		            }
		        });
		 
		        // Setting an item click listener for the AutoCompleteTextView dropdown list
		        atvPlaces.setOnItemClickListener(new OnItemClickListener() {
		            @Override
		            public void onItemClick(AdapterView<?> arg0, View arg1, int index,
		            long id) {
		 
		                ListView lv = (ListView) arg0;
		                SimpleAdapter adapter = (SimpleAdapter) arg0.getAdapter();
		 
		                HashMap<String, String> hm = (HashMap<String, String>) adapter.getItem(index);
		 
		                // Creating a DownloadTask to download Places details of the selected place
		                placeDetailsDownloadTask = new DownloadTask(PLACES_DETAILS);
		 
		                // Getting url to the Google Places details api
		                String url = getPlaceDetailsUrl(hm.get("reference"));
		 
		                // Start downloading Google Place Details
		                // This causes to execute doInBackground() of DownloadTask class
		                placeDetailsDownloadTask.execute(url);
		 
		            }
		        });
		        
		        
		        RelativeLayout setsource=(RelativeLayout)getActivity().findViewById(R.id.setsource);
		        RelativeLayout setdest=(RelativeLayout)getActivity().findViewById(R.id.setdest); 
		        RelativeLayout setwaypoint=(RelativeLayout)getActivity().findViewById(R.id.setwaypoint);
		        Button completion=(Button)getActivity().findViewById(R.id.finish);
		        RelativeLayout drawroute=(RelativeLayout)getActivity().findViewById(R.id.draw_route);
		        listwaypoints=new ArrayList<ParseGeoPoint>();
		        listwaypointsg=new ArrayList<JSONObject>();
		        sourceg=new JSONObject();
		        destinationg =new JSONObject();
		        pointg=new JSONObject();
		        gcd = new Geocoder(getActivity(), Locale.getDefault());
		        setsource.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(googleMap!=null){
							if(sourcemarker!=null){
								sourcemarker.remove();
							}
							
							if(isDrawroute){
								googleMap.clear();
								listwaypoints.clear();
								listwaypointsg.clear();
								isDrawroute=false;
								source=null;
								destination=null;
							}
							//source=new ParseGeoPoint(point.latitude, point.longitude);
							 // point = new LatLng(latitude, longitude);
							point=googleMap.getCameraPosition().target;
							
				                 Log.e("lat long",point.toString());
				                 
				               // CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(point);
//				                CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);
//				 
//				                // Showing the user input location in the Google Map
//				               
//				                googleMap.animateCamera(cameraZoom);
				              //  googleMap.moveCamera(cameraPosition);
							
							
							
							source=point;
							try {
								sourceg.put("lat",point.latitude);
								sourceg.put("lng",point.longitude);
								
//								addresses = gcd.getFromLocation(point.latitude, point.longitude, 1);
//								if (addresses.size() > 0) 
								sourceg.put("place_name",getAddress(point.latitude, point.longitude));
								
								
								//sourceg.put("place_name","Perinthalmanna"+date);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
					
							 MarkerOptions options = new MarkerOptions();
				                options.position(point);
				                options.title("starting point");
				               // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
				 
				                // Adding the marker in the Google Map

							       
				                sourcemarker= googleMap.addMarker(options);
				                sourcemarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				                    // googleMap.addMarker(options);
				              
							
							//marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
							Log.e("geopoint in source",source.toString());
						}
						
					}
				});
		        
 setdest.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(googleMap!=null){
							if(destmarker!=null){
								destmarker.remove();
							}
							
							if(isDrawroute){
								googleMap.clear();
								listwaypoints.clear();
								listwaypointsg.clear();
								isDrawroute=false;
								source=null;
								destination=null;
							}
							point=googleMap.getCameraPosition().target;
			                 Log.e("lat long",point.toString());
						destination=point;
						try {
							destinationg.put("lat",point.latitude);
							destinationg.put("lng",point.longitude);
				
								destinationg.put("place_name",getAddress(point.latitude, point.longitude));
								Log.e("place",getAddress(point.latitude, point.longitude));
							
							//destinationg.put("place_name","Pandikkad"+date);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						
						
						 MarkerOptions options = new MarkerOptions();
			                options.position(point);
			                options.title("destination point");
			               // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
			 
			                // Adding the marker in the Google Map
			                destmarker=googleMap.addMarker(options);
			           destmarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
			              
			              
						
							Log.e("geopoint in dest",destination.toString());
						
						}
						
						
						
					}
				});
 setwaypoint.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			if(googleMap!=null){
				if(isDrawroute){
					googleMap.clear();
					listwaypoints.clear();
					listwaypointsg.clear();
					isDrawroute=false;
					source=null;
					destination=null;
				}
				//marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
				point=googleMap.getCameraPosition().target;
				
				try {
					JSONObject pointg=new JSONObject();
					pointg.put("lat",point.latitude);
					pointg.put("lng",point.longitude);
					pointg.put("place_name",getAddress(point.latitude, point.longitude));
					
					if(listwaypointsg.size()<=6)
					{
						
						
							
							listwaypoints.add(new ParseGeoPoint(point.latitude, point.longitude));
							
				               listwaypointsg.add(pointg);
							Log.e("+++++++++++++++++waypoints============", listwaypointsg.toString());
							 MarkerOptions options = new MarkerOptions();
				               options.position(point);
				               options.title("way point");
				             
				              googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
					}
					
					else
					{
								Toast.makeText(getActivity(),"Maximum 6 way points allowed", Toast.LENGTH_LONG).show();
					}
					//pointg.put("place_name","palakkd"+date);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
              
			}
		}
	});

		completion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isDrawroute=true;
				if((source==null )||(destination==null)||listwaypointsg.size()==0)
				{
					Toast.makeText(getActivity(), "set all points for trip", Toast.LENGTH_LONG).show();
				}
				else{
					
				
					
					Map<String, Object> input_param = new HashMap<String, Object>();
					
					Log.d("geopointissue", sourceg.toString());
				
					input_param.put("source", sourceg);
					
					input_param.put("destination", destinationg);
					input_param.put("waypoints", listwaypointsg);
					Log.e("listwaypintg=======================", listwaypointsg.toString());
					JSONObject oj = new JSONObject();
					JSONArray jsa = new JSONArray();
					
					try {
						jsa.put(0, sourceg);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					input_param.put("invited_users", list_array);
					SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
					
					 
						try {
					 
							Date date1 = formatter.parse(date);
							Log.e("inside date methode", date1.toString());
						
						input_param.put("date", date1);
						
						}
						catch(Exception e){}
					
						input_param.put("tripname", name);
					ParseCloud.callFunctionInBackground("createPTrip", input_param, new FunctionCallback<Map<String,Object>>() {
					

						@Override
						public void done(Map<String,Object> result, ParseException e) {
							// TODO Auto-generated method stub
							 if (e == null) {
							      // result is "Hello world!"
								 Toast.makeText(getActivity(), result.get("message").toString(), Toast.LENGTH_LONG).show();
								 
//								 Bundle bundle2=new Bundle();
//								 String id;
//								if(result.get("pTrip_id").toString().equals(result.get("cTrip_id").toString())){
//									
//									
//									
//							
//									 bundle2.putString("plantripid", tripname);
//							
									 
									PlanATrip4 ptrip4=new PlanATrip4();
									//ptrip3.setArguments(bundle2);
									 getFragmentManager().beginTransaction()
									  .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
								        .replace(R.id.fragment1, ptrip4 )
								        .addToBackStack(null)

								        .commit();
									
								}}});
							   
							 
						
					

					
//					Toast.makeText(getActivity(), "Successfully set the trip", Toast.LENGTH_LONG).show();
//					Intent i=new Intent(getActivity().getApplicationContext(), MainMenuActivity.class);
//					startActivity(i);
					
					
				}
				
				
				
				
				
			}
		});        
	        
		drawroute.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if((source==null )||(destination==null)||listwaypoints.size()==0)
				{
					Toast.makeText(getActivity(), "set all points for trip", Toast.LENGTH_LONG).show();
				}
				else{
					
					
					googleMap.clear();
					
					 MarkerOptions options = new MarkerOptions();
		                options.position(source);
		                options.title("starting point");
		               // options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
		 
		                // Adding the marker in the Google Map
		           
		               googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		              
					options.position(destination);
	                options.title("destination point");
	                googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
		              
	                for(int i=0;i<listwaypoints.size();i++){
	                	LatLng waypoint=new LatLng(listwaypoints.get(i).getLatitude(), listwaypoints.get(i).getLongitude());
	                options.position(waypoint);
	                options.title("way point");
	                googleMap.addMarker(options).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
	                }  
					
				 String url = getDirectionsUrl(source, destination,listwaypoints);
				 
                 DownloadTask1 downloadTask = new DownloadTask1();

                 // Start downloading json data from Google Directions API
                 downloadTask.execute(url);
				}
				
			}
		});
		
		
	    }
	    
	    private String getAddress(double latitude, double longitude) {
	        StringBuilder result = new StringBuilder();
	        try {
	            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
	            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
	            if (addresses.size() > 0) {
	                Address address = addresses.get(0);
	                result.append(address.getLocality()).append(",");
	                result.append(address.getCountryName());
	            }
	        } catch (IOException e) {
	            Log.e("tag", e.getMessage());
	        }

	        return result.toString();
	    }
	    
	    private String getAutoCompleteUrl(String place){
	    	 
	        // Obtain browser key from https://code.google.com/apis/console
	        String key = "key=AIzaSyBtBUCoarybcqvUJWYodbX62kgdLYuKQ6A";
	 
	        // place to be be searched
	        String input = "input="+place;
	 
	        // place type to be searched
	        String types = "types=geocode";
	 
	        // Sensor enabled
	        String sensor = "sensor=false";
	 
	        // Building the parameters to the web service
	        String parameters = input+"&"+types+"&"+sensor+"&"+key;
	 
	        // Output format
	        String output = "json";
	 
	        // Building the url to the web service
	        String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
	 Log.d("url retrieved ", url);
	        return url;
	    }
	 
	    private String getPlaceDetailsUrl(String ref){
	 
	        // Obtain browser key from https://code.google.com/apis/console
	        String key = "key=AIzaSyBtBUCoarybcqvUJWYodbX62kgdLYuKQ6A";
	 
	        // reference of place
	        String reference = "reference="+ref;
	 
	        // Sensor enabled
	        String sensor = "sensor=false";
	 
	        // Building the parameters to the web service
	        String parameters = reference+"&"+sensor+"&"+key;
	 
	        // Output format
	        String output = "json";
	 
	        // Building the url to the web service
	        String url = "https://maps.googleapis.com/maps/api/place/details/"+output+"?"+parameters;
	 
	        return url;
	    }
	 
	    /** A method to download json data from url */
	    private String downloadUrl(String strUrl) throws IOException{
	        String data = "";
	        InputStream iStream = null;
	        HttpURLConnection urlConnection = null;
	        try{
	            URL url = new URL(strUrl);
	 
	            // Creating an http connection to communicate with url
	            urlConnection = (HttpURLConnection) url.openConnection();
	 
	            // Connecting to url
	            urlConnection.connect();
	 
	            // Reading data from url
	            iStream = urlConnection.getInputStream();
	 
	            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
	 
	            StringBuffer sb  = new StringBuffer();
	 
	            String line = "";
	            while( ( line = br.readLine())  != null){
	                sb.append(line);
	            }
	 
	            data = sb.toString();
	 
	            br.close();
	 
	        }catch(Exception e){
	            Log.d("Exception while downloading url", e.toString());
	        }finally{
	            iStream.close();
	            urlConnection.disconnect();
	        }
	        return data;
	    }
	 
	    // Fetches data from url passed
	    private class DownloadTask extends AsyncTask<String, Void, String>{
	 
	        private int downloadType=0;
	 
	        // Constructor
	        public DownloadTask(int type){
	            this.downloadType = type;
	        }
	 
	        @Override
	        protected String doInBackground(String... url) {
	 Log.e("inside do in", "Backgroyund");
	            // For storing data from web service
	            String data = "";
	 
	            try{
	                // Fetching the data from web service
	                data = downloadUrl(url[0]);
	                Log.d("data retrieved", data.toString());
	            }catch(Exception e){
	                Log.d("Background Task",e.toString());
	            }
	            return data;
	        }
	 
	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	 
	            switch(downloadType){
	            case PLACES:
	                // Creating ParserTask for parsing Google Places
	                placesParserTask = new ParserTask(PLACES);
	 
	                // Start parsing google places json data
	                // This causes to execute doInBackground() of ParserTask class
	                placesParserTask.execute(result);
	 
	                break;
	 
	            case PLACES_DETAILS :
	                // Creating ParserTask for parsing Google Places
	                placeDetailsParserTask = new ParserTask(PLACES_DETAILS);
	 
	                // Starting Parsing the JSON string
	                // This causes to execute doInBackground() of ParserTask class
	                placeDetailsParserTask.execute(result);
	            }
	        }
	    }
	 
	    /** A class to parse the Google Places in JSON format */
	    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{
	 
	        int parserType = 0;
	 
	        public ParserTask(int type){
	            this.parserType = type;
	        }
	 
	        @Override
	        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
	 
	            JSONObject jObject;
	            List<HashMap<String, String>> list = null;
	 
	            try{
	                jObject = new JSONObject(jsonData[0]);
	 
	                switch(parserType){
	                case PLACES :
	                    PlaceJSONParser placeJsonParser = new PlaceJSONParser();
	                    // Getting the parsed data as a List construct
	                    list = placeJsonParser.parse(jObject);
	                    break;
	                case PLACES_DETAILS :
	                    PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
	                    // Getting the parsed data as a List construct
	                    list = placeDetailsJsonParser.parse(jObject);
	                }
	 
	            }catch(Exception e){
	                Log.d("Exception",e.toString());
	            }
	            return list;
	        }
	 
	        @Override
	        protected void onPostExecute(List<HashMap<String, String>> result) {
	 
	            switch(parserType){
	            case PLACES :
	                String[] from = new String[] { "description"};
	                int[] to = new int[] { android.R.id.text1 };
	 
	                // Creating a SimpleAdapter for the AutoCompleteTextView
	                SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);
	 
	                // Setting the adapter
	                atvPlaces.setAdapter(adapter);
	                break;
	            case PLACES_DETAILS :
	                HashMap<String, String> hm = result.get(0);
	 
	                // Getting latitude from the parsed data
	                double latitude = Double.parseDouble(hm.get("lat"));
	 
	                // Getting longitude from the parsed data
	                double longitude = Double.parseDouble(hm.get("lng"));
	 
	                // Getting reference to the SupportMapFragment of the activity_main.xml
	               
	                
	                 point = new LatLng(latitude, longitude);
	                 Log.e("lat long",point.toString());
//	                 
	                CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(point);
//	                CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);
//	 
//	                // Showing the user input location in the Google Map
//	               
//	                googleMap.animateCamera(cameraZoom);
	                googleMap.moveCamera(cameraPosition);
//	 
//	                MarkerOptions options = new MarkerOptions();
//	                options.position(point);
//	                options.title("Position");
//	                options.snippet("Latitude:"+latitude+",Longitude:"+longitude);
//	 
//	                // Adding the marker in the Google Map
//	           
//	                marker= googleMap.addMarker(options);
//	              
//	 
	                
	                break;
	            }
	        }
	    }
	    
	    private String getDirectionsUrl(LatLng origin,LatLng dest,ArrayList<ParseGeoPoint> listwaypoint){
	    	 
	        // Origin of route
	        String str_origin = "origin="+origin.latitude+","+origin.longitude;
	 
	        // Destination of route
	        String str_dest = "destination="+dest.latitude+","+dest.longitude;
	        String waypoint="waypoints=";
	        int i;
	        for( i=0;i<listwaypoint.size()-1;i++){
	        	waypoint=waypoint+listwaypoint.get(i).getLatitude()+","+listwaypoint.get(i).getLongitude()+"|";
	        }
	        if(i==listwaypoint.size()-1){
	        	waypoint=waypoint+listwaypoint.get(i).getLatitude()+","+listwaypoint.get(i).getLongitude();
	 	       
	        }
	 
	        // Sensor enabled
	        String sensor = "sensor=false";
	 
	        // Building the parameters to the web service
	        String parameters = str_origin+"&"+str_dest+"&"+waypoint+"&"+sensor;
	 
	        // Output format
	        String output = "json";
	 
	        // Building the url to the web service
	        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
	 
	        return url;
	    }
	    
	    private class DownloadTask1 extends AsyncTask<String, Void, String>{
	    	 
	        // Downloading data in non-ui thread
	        @Override
	        protected String doInBackground(String... url) {
	 
	            // For storing data from web service
	            String data = "";
	 
	            try{
	                // Fetching the data from web service
	                data = downloadUrl(url[0]);
	            }catch(Exception e){
	                Log.d("Background Task",e.toString());
	            }
	            return data;
	        }
	 
	        // Executes in UI thread, after the execution of
	        // doInBackground()
	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	 
	            ParserTask1 parserTask = new ParserTask1();
	 
	            // Invokes the thread for parsing the JSON data
	            parserTask.execute(result);
	        }
	    }
	    
	    private class ParserTask1 extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
	    	 
	        // Parsing the data in non-ui thread
	        @Override
	        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
	 
	            JSONObject jObject;
	            List<List<HashMap<String, String>>> routes = null;
	 
	            try{
	                jObject = new JSONObject(jsonData[0]);
	                DirectionsJSONParser parser = new DirectionsJSONParser();
	 
	                // Starts parsing data
	                routes = parser.parse(jObject);
	            }catch(Exception e){
	                e.printStackTrace();
	            }
	            return routes;
	        }
	 
	        // Executes in UI thread, after the parsing process
	        @Override
	        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
	            ArrayList<LatLng> points = null;
	            PolylineOptions lineOptions = null;
	            MarkerOptions markerOptions = new MarkerOptions();
	 
	            // Traversing through all the routes
	            for(int i=0;i<result.size();i++){
	                points = new ArrayList<LatLng>();
	                lineOptions = new PolylineOptions();
	 
	                // Fetching i-th route
	                List<HashMap<String, String>> path = result.get(i);
	 
	                // Fetching all the points in i-th route
	                for(int j=0;j<path.size();j++){
	                    HashMap<String,String> point = path.get(j);
	 
	                    double lat = Double.parseDouble(point.get("lat"));
	                    double lng = Double.parseDouble(point.get("lng"));
	                    LatLng position = new LatLng(lat, lng);
	 
	                    points.add(position);
	                }
	 
	                // Adding all the points in the route to LineOptions
	                lineOptions.addAll(points);
	                lineOptions.width(2);
	                lineOptions.color(Color.RED);
	            }
	 
	            // Drawing polyline in the Google Map for the i-th route
	            try{
	            googleMap.addPolyline(lineOptions);
	            }
	            catch(Exception e)
	            {
	            	Toast.makeText(getActivity(), "add route other than sea points", Toast.LENGTH_LONG).show();
	            }
	        }
	    }

	    @Override
	    public void onDestroyView() 
	    {
	       super.onDestroyView(); 
	        
	       FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
	       ft.remove(fragment);
	       ft.commit();
	   }
	   
	}
	    


