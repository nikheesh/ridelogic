package com.vyooha.ridelogic.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class MainMenuActivity extends Activity
{
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				        WindowManager.LayoutParams.FLAG_FULLSCREEN);
				     setContentView(R.layout.mainmenu);
				     
				     RelativeLayout ride=(RelativeLayout)findViewById(R.id.menu_ride);
				    
				     
				     ride.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),CompassActivity.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
				     
				     RelativeLayout settings=(RelativeLayout)findViewById(R.id.menu_settings);
				    
				     
				     settings.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),SettingsActivity.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
				     
 RelativeLayout social=(RelativeLayout)findViewById(R.id.menu_social);
				    
				     
				     social.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),Social_Media.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
 RelativeLayout status=(RelativeLayout)findViewById(R.id.menu_stats);
				    
				     
				     status.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),NearByMapActivity.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
				     
 RelativeLayout plantrip=(RelativeLayout)findViewById(R.id.menu_plan_a_trip);
				    
				     
				     plantrip.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),PlanATrip.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
 RelativeLayout about=(RelativeLayout)findViewById(R.id.menu_about);
				    
				     
				     about.setOnClickListener(new OnClickListener() {
				           

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								//startLoading();
								Intent i=new Intent(getApplicationContext(),MainTabTrip.class);
				  					//i.putExtra("isoffer", listcompany.isoffer);
				  				startActivity(i);
				  				//stopLoading();
								
							}
				        });
				     
				     
	 }
	 
	  private void SyncNow(){
			//startLoading();
		final ArrayList<String> NumberArray=new ArrayList<String>();
		//ArrayList<String> NameArray=new ArrayList<String>();
//			ArrayList<Boolean> IsRideLogic=new ArrayList<Boolean>();
			final ArrayList<ContactClassTemp> cont_temp=new ArrayList<ContactClassTemp>();
			Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
			String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
			                ContactsContract.CommonDataKinds.Phone.NUMBER};

			Cursor people = getContentResolver().query(uri, projection, null, null, null);

			int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
			int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
		String tempNumber;int len;
			people.moveToFirst();
			do {
				ContactClassTemp cont=new ContactClassTemp();
				cont.name=people.getString(indexName);
				cont.phonenumber=people.getString(indexNumber);
//			     NameArray.add( people.getString(indexName));
				
				tempNumber=people.getString(indexNumber);
				tempNumber=tempNumber.replaceAll("\\s","");
				len=tempNumber.length();
				if(len>10)
				{
					tempNumber=tempNumber.substring(len-10, len);
				}
				Log.e("temp number+"+cont.name, tempNumber);
			    NumberArray.add(tempNumber);
			    cont_temp.add(cont);
//			    IsRideLogic.add(false);
//			    contclass.name=name;
//			    contclass.phonenumber=number;
//			    socialAdapter.add(contclass);
			    // Do work...
			} while (people.moveToNext());
			
			final ArrayList<ParseObject> list_cont=new ArrayList<ParseObject>();
			
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereContainedIn("phone", NumberArray);
			query.findInBackground(new FindCallback<ParseUser>() {
				@Override
			  public void done(List<ParseUser> objects, ParseException e) {
			    if (e == null) {
			        // The query was successful.
			    	
			    	for(int k=0;k<NumberArray.size();k++){
			    		final ParseObject contactobject=new ParseObject("Contacts");
			    		
			    		contactobject.put("name", cont_temp.get(k).name);
			    		contactobject.put("phone", cont_temp.get(k).phonenumber);
			    		contactobject.put("isRideLogik", false);
			    		
			    	for(int i=0;i<objects.size();i++){
			    		if(objects.get(i).getString("phone").equals(cont_temp.get(k).phonenumber)){
			    			  
			    			Log.d("CUSTOM", "ONE NUMBER EQUALLLED "+objects.get(i).getString("phone"));
			    			
			    			final ParseFile  pf=objects.get(i).getParseFile("profile_pic");
			    			
							contactobject.put("profile_pic", pf);				    
								contactobject.put("isRideLogik", true);
								contactobject.put("obid", objects.get(i).getObjectId());

			    			 objects.remove(i);
			    			 Log.d("CUSTOM", "sub loop finished");
			    			break;
			    		}
			    	
			    	}
			    	list_cont.add(contactobject);
			    	
					
			    	
			    	}
			    	
			    	
			    	Log.d("CUSTOM", "top loop finished");
			    	
			    	try {
			        	Log.d("CUSTOM", "before pinall");
			        	
			        	ParseObject.unpinAll("mycon");
						ParseObject.pinAll("mycon",list_cont);
						
						
					  	Log.d("CUSTOM", "after pinall");
					  
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					  	Log.d("CUSTOM", e1.toString());
					}
			    } else {
			        // Something went wrong.
			    }
			  }
			});
			
			//stopLoading();
			
		}

	 
	 @Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	 	// Inflate the menu; this adds items to the action bar if it is present.
	 	getMenuInflater().inflate(R.menu.profile, menu);
	 	return true;
	 }
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item){
	   switch(item.getItemId()) {
	   case R.id.action_logout:	          
	 	  ParseUser.logOut();				
	 	  startActivity(new Intent(this, FirstpageActivity.class));
	       break;
	  
	   }
	   return true;
	 }
	 
//	 @Override
//	 public void onBackPressed() {
//		 AlertDialog.Builder adb = new AlertDialog.Builder(this);
//
//
//		   // adb.setView( );
//
//
//		    adb.setTitle("Do you want to Exit "+R.string.app_name+" ?");
//		   
//
//
//		    adb.setIcon(R.drawable.ic_launcher);
//
//
//		    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//		        public void onClick(DialogInterface dialog, int which) {
//
//		        	finish();
//		            System.exit(1);
//		           
//		      } });
//
//
//		    adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//		        public void onClick(DialogInterface dialog, int which) {
//
//		            finish();
//		      } });
//		    adb.show();
//
//	    
//	 }
//	 
	 @Override
	 public void onBackPressed() {
	 	super.onBackPressed();
	 	finish();
//	 	android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
	 	//System.exit(0);
	 } 
	 
}
