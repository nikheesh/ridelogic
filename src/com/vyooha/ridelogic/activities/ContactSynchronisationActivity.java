package com.vyooha.ridelogic.activities;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vyooha.ridelogic.views.RoundedImageView;



public class ContactSynchronisationActivity  extends Fragment {
    // adapter that holds tweets, obviously :)
    ArrayAdapter<ContactClass> contactAdapter;
//    private JSONObject json;
//    private JSONArray jsonarray;
    private ConnectivityManager conmgr;
    
    boolean flag;
    ListView list;
    EditText search;
    ContactClass contclass;
    Button sync_btn;
    ParseUser user;
    int k;
    
    SyncContacts synccontact;
    protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(getActivity().getApplicationContext());
	    proDialog.setMessage("Synchronising.... Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
    
    
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
    	
       View contacts=inflater.inflate(R.layout.contact_list, container, false); 
       return contacts;
       
    }  
    private class ViewHolder {
       public TextView name;
     public   TextView number;
     public   RoundedImageView imageView;
     public ImageView img;
        }
       
    @Override 
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);  
    
        user=ParseUser.getCurrentUser();
        conmgr = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
    	
         contactAdapter = new ArrayAdapter<ContactClass>(getActivity(), 0) {
        	 
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            	 final ContactClass cont;
                if (convertView == null){
                	vh=new ViewHolder();
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.contact_item, null);
                    vh.imageView=(RoundedImageView)convertView.findViewById(R.id.cont_img);
                    vh.img=(ImageView)convertView.findViewById(R.id.ridelogic_logo_cnt);
                    vh.name=(TextView)convertView.findViewById(R.id.cont_name);
                    vh.number=(TextView)convertView.findViewById(R.id.cont_number);
                    convertView.setTag(vh);
                }
                
                else{
                	vh = (ViewHolder) convertView.getTag();
                	 convertView.setTag(vh);
                }
                      cont = this.getItem(position);
                     Log.d("last", cont.name+", "+cont.phonenumber+", "+cont.isRideLogik);
                     
                  
                 try{             
                  
                 //  RoundedImageView imageView = (RoundedImageView)convertView.findViewById(R.id.cont_img);
              
                

                //  TextView name = (TextView)convertView.findViewById(R.id.cont_name);
                 vh.name.setText(cont.name);
                 // TextView number=(TextView)convertView.findViewById(R.id.cont_number);
                  vh.number.setText(cont.phonenumber);
                 // ImageView img=(ImageView)convertView.findViewById(R.id.ridelogic_logo_cnt);
                  if(cont.isRideLogik){                	 
                  
                      vh.img.setImageResource(R.drawable.ic_launcher);
                      
                      
                  }
                  else{
                	  vh.img.setImageBitmap(null);
                  }
                  
                  RelativeLayout layout=(RelativeLayout)convertView.findViewById(R.id.layoutcontact);
                  
                  layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
						
												if(cont.phonenumber!=""){
													Log.i("Make call", "");

												      Intent phoneIntent = new Intent(Intent.ACTION_CALL);
												      phoneIntent.setData(Uri.parse("tel:"+cont.phonenumber));

												      try {
												         startActivity(phoneIntent);
												         getActivity().finish();
												         Log.i("Finished making a call...", "");
												      } catch (android.content.ActivityNotFoundException ex) {
												         Toast.makeText(getActivity().getApplicationContext(), 
												         "Call faild, please try again later.", Toast.LENGTH_SHORT).show();
												      }
													
												}
												
						}
					});

//                TextView phone = (TextView)convertView.findViewById(R.id.cont_number);
//                phone.setText(cont.phone);
                
                if(cont.photo!=null){
                vh.imageView.setImageBitmap(cont.photo);
                	Log.e("imagesetted", "hi");
                }
                else{
                	vh.imageView.setImageResource(R.drawable.def_contact);
                	
                }
                
                      }
                      catch (Exception e){}
                      
            //    text.setText(tweet.get("mdesc").getAsString());
                return convertView;
            }
        };
    //	requestWindowFeature(Window.FEATURE_NO_TITLE);
//setContentView(R.layout.contact_list);

ImageView close1=(ImageView)getActivity().findViewById(R.id.close_img);
close1.setOnClickListener(new OnClickListener() {
    

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		getActivity().finish();
		
	}
});

search=(EditText)getActivity().findViewById(R.id.searchedit);



 list=(ListView)getActivity().findViewById(R.id.listofcontacts);
list.setAdapter(contactAdapter);
//fetchContacts();

contactsFromParse();
//synchronisationBefaoreOrAfter();
sync_btn=(Button)getActivity().findViewById(R.id.syncnow);
sync_btn.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		//synchronousAndDisplay();
		//SyncNow();
		if(synccontact==null){
			synccontact=new SyncContacts(); 
			if(checkInternet())
				synccontact.execute();
				else
					Toast.makeText(getActivity(), "Please check your internet", Toast.LENGTH_LONG).show();
			
		}
		else{
			if(synccontact.getStatus()==AsyncTask.Status.PENDING){
				if(checkInternet())
				synccontact.execute();
				else
					Toast.makeText(getActivity(), "Please check your internet", Toast.LENGTH_LONG).show();
			}
		}
		
	}
});


search.addTextChangedListener(new TextWatcher() {

    public void afterTextChanged(Editable s) {

      // you can call or do what you want with your EditText here
    

    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    	
    	//fetchContacts(search.getText().toString());
    	contactAdapter.clear();
    	contactsFromParse(search.getText().toString());
    }
 });

    }
    
    
    public void fetchContacts() {
    	contactAdapter.clear();
		String phoneNumber = null;


		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		


		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null,null, null, null);	

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {
				 contclass=new ContactClass();
				String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
				String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
				//Log.e("name=====", name);
			//	String phone="";
				InputStream image;
				contclass.name=name;
				contclass.contact_id=contact_id;
//				contclass.photo=photo_image;
				contactAdapter.add(contclass);
			
			}
			
			


		}
	}

    public void fetchContacts(String namelike) {
    	
		String phoneNumber = null;
		contactAdapter.clear();

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
		
		final String sa1 = namelike+"%";
     	 final String SELECTION =
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
	            Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?" :
	            Contacts.DISPLAY_NAME + " LIKE ?";


		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null,SELECTION, new String[] { sa1 }, null);	

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {
				 contclass=new ContactClass();
				String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
				String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
				Log.e("name=====", name);
			//	String phone="";
				InputStream image;
				contclass.name=name;
				contclass.contact_id=contact_id;
//				contclass.photo=photo_image;
				contactAdapter.add(contclass);
			
			}
			
			


		}
	}

private void addNameAndNumber()
{
	 
	Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
	                ContactsContract.CommonDataKinds.Phone.NUMBER};

	Cursor people = getActivity().getContentResolver().query(uri, projection, null, null, null);

	int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
	int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

	people.moveToFirst();
	do {contclass=new ContactClass();
	    String name   = people.getString(indexName);
	    String number = people.getString(indexNumber);
	    contclass.name=name;
	    contclass.phonenumber=number;
	    contactAdapter.add(contclass);
	    // Do work...
	} while (people.moveToNext());
}

private void SyncNow(){
	//startLoading();
final ArrayList<String> NumberArray=new ArrayList<String>();
//ArrayList<String> NameArray=new ArrayList<String>();
//	ArrayList<Boolean> IsRideLogic=new ArrayList<Boolean>();
	final ArrayList<ContactClassTemp> cont_temp=new ArrayList<ContactClassTemp>();
	Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
	                ContactsContract.CommonDataKinds.Phone.NUMBER};

	Cursor people = getActivity().getContentResolver().query(uri, projection, null, null, null);

	int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
	int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
String tempNumber;int len;
	people.moveToFirst();
	do {
		ContactClassTemp cont=new ContactClassTemp();
		cont.name=people.getString(indexName);
		cont.phonenumber=people.getString(indexNumber);
//	     NameArray.add( people.getString(indexName));
		
		tempNumber=people.getString(indexNumber);
		tempNumber=tempNumber.replaceAll("\\s","");
		len=tempNumber.length();
		if(len>10)
		{
			tempNumber=tempNumber.substring(len-10, len);
		}
		Log.e("temp number+"+cont.name, tempNumber);
	    NumberArray.add(tempNumber);
	    cont_temp.add(cont);
//	    IsRideLogic.add(false);
//	    contclass.name=name;
//	    contclass.phonenumber=number;
//	    socialAdapter.add(contclass);
	    // Do work...
	} while (people.moveToNext());
	
	final ArrayList<ParseObject> list_cont=new ArrayList<ParseObject>();
	
	ParseQuery<ParseUser> query = ParseUser.getQuery();
	query.whereContainedIn("phone", NumberArray);
	query.findInBackground(new FindCallback<ParseUser>() {
		@Override
	  public void done(List<ParseUser> objects, ParseException e) {
	    if (e == null) {
	        // The query was successful.
	    	
	    	for(int k=0;k<NumberArray.size();k++){
	    		
//	    	ContactClass contacts=new ContactClass();
//	    	contacts.name=cont_temp.get(k).name;
//	    	contacts.phonenumber=cont_temp.get(k).phonenumber;
	    	 flag=true;
	    	 
	    		final ParseObject contactobject=new ParseObject("Contacts");
	    		
	    		contactobject.put("name", cont_temp.get(k).name);
	    		contactobject.put("phone", cont_temp.get(k).phonenumber);
	    		contactobject.put("isRideLogik", false);
	    		
	    	for(int i=0;i<objects.size();i++){
	    		if(objects.get(i).getString("phone").equals(cont_temp.get(k).phonenumber)){
	    			  
	    			Log.d("CUSTOM", "ONE NUMBER EQUALLLED "+objects.get(i).getString("phone"));
	    			
	    			final ParseFile  pf=objects.get(i).getParseFile("profile_pic");
	    			//	pf.save();
					contactobject.put("profile_pic", pf);				    
						contactobject.put("isRideLogik", true);
						contactobject.put("obid", objects.get(i).getObjectId());
//	    			 pf.saveInBackground(new SaveCallback(){
//
//				 			@Override
//				 			public void done(ParseException e) {
//				 				if(e == null){
//				 					contactobject.put("profile_pic", pf);
//	    
//				 					contactobject.put("isRideLogik", true);
//				 					
//				 					
//				 						
//				 					
//				 					}
//				 				}
//				 			});
	    			 objects.remove(i);
	    			 Log.d("CUSTOM", "sub loop finished");
	    			break;
	    		}
	    	
	    	}
	    	list_cont.add(contactobject);
	    	
			//contactobject.pin();
	    	
	    	}
	    	
	    	
	    	Log.d("CUSTOM", "top loop finished");
	    	
	    	try {
	        	Log.d("CUSTOM", "before pinall");
	        	
	        	ParseObject.unpinAll("mycon");
				ParseObject.pinAll("mycon",list_cont);
				
				
			  	Log.d("CUSTOM", "after pinall");
			  
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			  	Log.d("CUSTOM", e1.toString());
			}
	    } else {
	        // Something went wrong.
	    }
	  }
	});
	
	//stopLoading();
	
}

private void contactsFromParse()
{
	 ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

	 query.fromLocalDatastore();
	 query.findInBackground(new FindCallback<ParseObject>() {
		

		@Override
		public void done(List<ParseObject> obj, ParseException e) {
			// TODO Auto-generated method stub
			if(e==null){
				if(obj.size()>0){
					Log.e("loading from parse", "=================");
					
		for(int i=0;i<obj.size();i++){
			
			contclass=new ContactClass();
			contclass.name=obj.get(i).getString("name");
			contclass.phonenumber=obj.get(i).getString("phone");
			contclass.isRideLogik=obj.get(i).getBoolean("isRideLogik");
			
			Log.d("CUSTOM NEW",contclass.name+",  "+contclass.phonenumber+" , "+contclass.isRideLogik);
			
			if(obj.get(i).has("profile_pic")){
				ParseFile pf=obj.get(i).getParseFile("profile_pic");
			if(pf!=null){
			byte[] bitmapdata;
			try {
				bitmapdata = pf.getData();
				Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
				contclass.photo=bitmap;
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			}
			}
			contactAdapter.add(contclass);
			
				}
				
				}
				
				else
				{
					
					Log.e("loading from content resolver", "after success.################");
					addNameAndNumber();
				}
				
				
			}
			else{
				Log.e("loading from content resolver", "after success.################");
				addNameAndNumber();
			}
			
			}
	 });
	 	
	 
}

private void contactsFromParse(String namelike){
	
	
	 ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

	 query.fromLocalDatastore();
	 query.whereStartsWith("name", namelike);
	 query.findInBackground(new FindCallback<ParseObject>() {
		

		@Override
		public void done(List<ParseObject> obj, ParseException e) {
			// TODO Auto-generated method stub
			if(e==null){
				
					
		for(int i=0;i<obj.size();i++){
			
			contclass=new ContactClass();
			contclass.name=obj.get(i).getString("name");
			contclass.phonenumber=obj.get(i).getString("phone");
			contclass.isRideLogik=obj.get(i).getBoolean("isRideLogik");
			
			Log.d("CUSTOM NEW",contclass.name+",  "+contclass.phonenumber+" , "+contclass.isRideLogik);
			
			if(obj.get(i).has("profile_pic")){
				ParseFile pf=obj.get(i).getParseFile("profile_pic");
			if(pf!=null){
				byte[] bitmapdata;
					try {
						bitmapdata = pf.getData();
						Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
						contclass.photo=bitmap;
						} 
					catch (ParseException e1) 
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			
			contactAdapter.add(contclass);
			
				}
				
				}
				
				
			}
	 });
	 	
	
}
private void synchronousAndDisplay(){
	ArrayList<String> NumberArray=new ArrayList<String>();
	//ArrayList<String> NameArray=new ArrayList<String>();
//		ArrayList<Boolean> IsRideLogic=new ArrayList<Boolean>();
		final ArrayList<ContactClassTemp> cont_temp=new ArrayList<ContactClassTemp>();
		Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
		                ContactsContract.CommonDataKinds.Phone.NUMBER};

		Cursor people = getActivity().getContentResolver().query(uri, projection, null, null, null);

		int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
		ContactClassTemp cont;
		people.moveToFirst();
		do {
			cont=new ContactClassTemp();
			cont.name=people.getString(indexName);
			cont.phonenumber=people.getString(indexNumber);
//		     NameArray.add( people.getString(indexName));
		    NumberArray.add(people.getString(indexNumber));
		    cont_temp.add(cont);
//		    IsRideLogic.add(false);
//		    contclass.name=name;
//		    contclass.phonenumber=number;
//		    socialAdapter.add(contclass);
		    // Do work...
		} while (people.moveToNext());
		final ArrayList<ParseObject> list_cont=new ArrayList<ParseObject>();
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereContainedIn("phone", NumberArray);
		query.findInBackground(new FindCallback<ParseUser>() {
			@Override
		  public void done(List<ParseUser> objects, ParseException e) {
		    if (e == null) {
		        // The query was successful.
		    	
		    	for( k=0;k<cont_temp.size()-1;k++){
		    		
		    	for(int i=0;i<objects.size();i++){
		    		if(objects.get(i).getString("phone").equals(cont_temp.get(k).phonenumber)){
		    			
		    			final ParseFile  pf=objects.get(i).getParseFile("profile_pic");
		    			 pf.saveInBackground(new SaveCallback(){

					 			@Override
					 			public void done(ParseException e) {
					 				if(e == null){
					 					 ParseObject contactobject=new ParseObject("Contacts");
							    		contactobject.put("name", cont_temp.get(k).name);
							    		contactobject.put("phone", cont_temp.get(k).phonenumber);
							    		contactobject.put("isRideLogik", false);
					 					contactobject.put("profile_pic", pf);
		    
					 					contactobject.put("isRideLogik", true);
					 					
					 					list_cont.add(contactobject);
					 						
					 					
					 					}
					 				}
					 			});
		    			 objects.remove(i);
		    			break;
		    		}
		    	
		    	}
		    	
		    	
				//contactobject.pin();
		    	
		    	}
		    	
		    	try {
					ParseObject.pinAll(list_cont);
					contactAdapter.clear();
					synchronisationBefaoreOrAfter();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    } else {
		        // Something went wrong.
		    }
		  }
		});
		
		//stopLoading();
		

}
private void synchronisationBefaoreOrAfter(){
	
	
	
	
	Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
	                ContactsContract.CommonDataKinds.Phone.NUMBER};

	final Cursor people = getActivity().getContentResolver().query(uri, projection, null, null, null);

	final int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
	final int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

	people.moveToFirst();
	
	
	 ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

	 query.fromLocalDatastore();
	 query.findInBackground(new FindCallback<ParseObject>() {
		

		@Override
		public void done(List<ParseObject> obj, ParseException e) {
			// TODO Auto-generated method stub
			if(e==null){
				

				do {contclass=new ContactClass();
				    String name   = people.getString(indexName);
				    String number = people.getString(indexNumber);
				    contclass.name=name;
				    contclass.phonenumber=number;
				    contclass.isRideLogik=false;
				    // Do work...
			
				
				
				
				if(obj.size()>0){
					Log.e("loading from parse", "=================");
					
				for(int i=0;i<obj.size();i++){
					if(obj.get(i).getString("phone").equals(contclass.phonenumber)){
						contclass.isRideLogik=true;
						if(obj.get(i).has("profile_pic")){
							ParseFile pf=obj.get(i).getParseFile("profile_pic");
							if(pf!=null){
								byte[] bitmapdata;
					try {
						bitmapdata = pf.getData();
						Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
						contclass.photo=bitmap;
				
				
						} catch (ParseException e1) {
				// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				}
			}
						obj.remove(i);
						break;
					}
				}
				
				}
				
				contactAdapter.add(contclass);
				
				
				} while (people.moveToNext());
				
			}
			else{
				Log.e("loading from content resolver", "after success.################");
				addNameAndNumber();
			}
			
			}
	 });
	
}

class SyncContacts extends AsyncTask<Void, String, Void> {
    @Override
    protected Void doInBackground(Void... unused) {
SyncNow();
       
        return (null);
    }

   
    @Override
    protected void onPostExecute(Void unused) {
        //Toast.makeText(GameScreen_bugfix.this, "music loaded!", Toast.LENGTH_SHORT).show();
    	contactAdapter.clear();
	  	contactsFromParse();
		
    }
}

public boolean checkInternet(){
	NetworkInfo ni = conmgr.getActiveNetworkInfo();
	if (ni == null) {
	    // There are no active networks.
	    return false;
	}else{
		boolean isConnected = ni.isConnected();
		return isConnected;
	}
}

}
