package com.vyooha.ridelogic.activities;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

public class SocialMediaComment  extends Activity{
	EditText post_text;
	
	Button upload_comment;
	
	
	protected ProgressDialog proDialog;
	protected void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("uploading comment. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}

	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.socialmedia_comment);
	        
	        
	        post_text=(EditText)findViewById(R.id.post_comment_edit);
	       
	        upload_comment=(Button)findViewById(R.id.upload_comment);
	       
	      
	        upload_comment.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startLoading();
					 String postid=getIntent().getStringExtra("postid");
				        String ownerid=getIntent().getStringExtra("ownerid");
				        
				       
						final Map<String, Object> input_param = new HashMap<String, Object>();
						input_param.put("post_owner_userId", ownerid);
						
						input_param.put("original_postId", postid);
						
						input_param.put("comment_text", post_text.getText().toString());
						
					 					ParseCloud.callFunctionInBackground("commentOnPost", input_param, new FunctionCallback<String>() {
											

											

											@Override
											public void done(
													 String result,
													ParseException e) {
												// TODO Auto-generated method stub
												if(e==null){
													stopLoading();
													Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
												}
											}
											});
					 					
					 					
					 					
					 			
					
					
					
					
					
					
				}
			});
	    
	 }
	
	 

}
