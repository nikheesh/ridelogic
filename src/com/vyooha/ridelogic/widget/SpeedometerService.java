
package com.vyooha.ridelogic.widget;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.Random;

import com.vyooha.ridelogic.activities.R;
import com.vyooha.ridelogic.views.ArcProgress;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

public class SpeedometerService extends Service {
	public static final String UPDATEMOOD = "UpdateMood";
	public static final String CURRENTMOOD = "CurrentMood";
	
	private String currentMood;
	private LinkedList<String> moods;
	
	public SpeedometerService(){
		this.moods = new LinkedList<String>();
		fillMoodsList();
	}

    private void fillMoodsList() {
    	this.moods.add(":)");
    	this.moods.add(":(");
    	this.moods.add(":D");
    	this.moods.add(":X");
    	this.moods.add(":S");
    	this.moods.add(";)");
    	
    	this.currentMood = ";)";
    }


	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
        Log.i(SpeedometerWidgetProvider.WIDGETTAG, "onStartCommand");

        
      
        
        
        
        
        updateSpeed(intent);
       
			
		stopSelf(startId);
		
		return START_STICKY;
	}

//	private String getRandomMood() {
//		Random r = new Random(Calendar.getInstance().getTimeInMillis());
//		int pos = r.nextInt(moods.size());
//		return moods.get(pos);
//	}

	private void updateSpeed(Intent intent) {
        Log.i(SpeedometerWidgetProvider.WIDGETTAG, "This is the intent " + intent);
        if (intent != null){
    		String requestedAction = intent.getAction();
            Log.i(SpeedometerWidgetProvider.WIDGETTAG, "This is the action " + requestedAction);
    		if (requestedAction != null){// && requestedAction.equals(UPDATEMOOD)){
	          //  this.currentMood = getRandomMood();
	            	            	 
	            int widgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, 0);
	            int pvalue=intent.getIntExtra("pvalue", 0);

	          //  Log.i(SpeedometerWidgetProvider.WIDGETTAG, "This is the currentMood " + currentMood + " to widget " + widgetId);

	            AppWidgetManager appWidgetMan = AppWidgetManager.getInstance(this);
	            RemoteViews views = new RemoteViews(this.getPackageName(),R.layout.widgetlayout);
	          //  views.setTextViewText(R.id.widgetMood, currentMood);
	         //   views.setBitmap(R.id.speedwidget, , value)
	            ArcProgress myView = new ArcProgress(getApplicationContext());
	            myView.measure(150,150);
	            myView.layout(0,0,150,150);
	           
	            myView.setProgress(pvalue);
	            myView.setDrawingCacheEnabled(true);
	            Bitmap bitmap=myView.getDrawingCache();
	            views.setImageViewBitmap(R.id.imagespeedwidget, bitmap);
	            
	            
	            
	            
	            
	         //   views.setInt(R.id.speedwidget,"setProgress" , pvalue);
	            
	            appWidgetMan.updateAppWidget(widgetId, views);
	            
	            Log.i(SpeedometerWidgetProvider.WIDGETTAG, "CurrentMood updated!");
    		}
        }
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
