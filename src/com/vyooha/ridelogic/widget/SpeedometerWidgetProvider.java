package com.vyooha.ridelogic.widget;


import com.vyooha.ridelogic.activities.R;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.RemoteViews;

public class SpeedometerWidgetProvider extends AppWidgetProvider {
	public static final String WIDGETTAG = "WidgetMood";
	
	@Override
	public void onUpdate(final Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
	 //   Log.i(WIDGETTAG, "onUpdate");
		
		final int N = appWidgetIds.length;
		
		// Perform this loop procedure for each App Widget that belongs to this provider
		for (int i=0; i<N; i++) {
		  final  int appWidgetId = appWidgetIds[i];
		    		    
		  //  Log.i(WIDGETTAG, "updating widget[id] " + appWidgetId);

		    RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.widgetlayout);
		    
		    LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

			// Define a listener that responds to location updates
			LocationListener locationListener = new LocationListener() {
			    public void onLocationChanged(Location location) {
			      // Called when a new location is found by the network location provider.
			    	int pvalue=(int)(location.getSpeed()*18/5); 
			    	 
				    Intent intent = new Intent(context, SpeedometerService.class);
				    intent.setAction(SpeedometerService.UPDATEMOOD);
				    intent.putExtra("pvalue", pvalue);
				    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
				   // PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
			context.startService(intent);
		
			    }

			    public void onStatusChanged(String provider, int status, Bundle extras) {}

			    public void onProviderEnabled(String provider) {}

			    public void onProviderDisabled(String provider) {}
			  };

			// Register the listener with the Location Manager to receive location updates
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 0, locationListener);
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
			
		    
		    
		   
		  //  views.setOnClickPendingIntent(R.id.widgetBtn, pendingIntent);
		    Log.i(WIDGETTAG, "pending intent set");
		    
		    // Tell the AppWidgetManager to perform an update on the current App Widget
		    appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}	
}
